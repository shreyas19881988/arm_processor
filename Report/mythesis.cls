\NeedsTeXFormat{LaTeX2e}[2014/05/17]
\ProvidesClass{mythesis}[1994/03/26 My custom CV class]
\LoadClass{report}
\setlength{\paperwidth}{210mm}
\setlength{\paperheight}{297mm}
\setlength{\hoffset}{-1in}
\setlength{\voffset}{-1in}

\setlength{\textheight}{245mm}
\setlength{\textwidth}{160mm}

\setlength{\headheight}{3mm}
\setlength{\headsep}{12mm}
\setlength{\topmargin}{15mm}
\setlength{\footskip}{10mm}

\setlength{\parindent}{12mm}

\setlength{\oddsidemargin}{30mm}
\setlength{\evensidemargin}{20mm}
