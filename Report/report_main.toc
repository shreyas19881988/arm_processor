\contentsline {chapter}{List of Figures}{vi}
\contentsline {chapter}{List of Tables}{viii}
\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Scope of the project}{2}
\contentsline {section}{\numberline {1.2}Dissertation outline}{2}
\contentsline {chapter}{\numberline {2}Thermal Aware Techniques}{3}
\contentsline {section}{\numberline {2.1}Complete utilisation of Register File}{3}
\contentsline {section}{\numberline {2.2}Minimizing Bit switching between subsequent Register accesses}{4}
\contentsline {chapter}{\numberline {3}Superscalar ARM Processor Architecture}{6}
\contentsline {section}{\numberline {3.1}Instruction Memory}{7}
\contentsline {section}{\numberline {3.2}Fetch Unit}{8}
\contentsline {section}{\numberline {3.3}Decoder}{9}
\contentsline {subsection}{\numberline {3.3.1}Data processing instructions}{9}
\contentsline {subsection}{\numberline {3.3.2}Shifter Operand Encoding for Data Processing Instructions}{10}
\contentsline {subsection}{\numberline {3.3.3}Load Store instructions}{11}
\contentsline {subsection}{\numberline {3.3.4}Offset encoding for Load Store Instructions}{11}
\contentsline {subsection}{\numberline {3.3.5}Load Store multiple instructions}{12}
\contentsline {subsection}{\numberline {3.3.6}Branch instructions}{13}
\contentsline {section}{\numberline {3.4}Tag Register file}{14}
\contentsline {section}{\numberline {3.5}Reservation Station}{15}
\contentsline {section}{\numberline {3.6}Execution Unit}{19}
\contentsline {subsection}{\numberline {3.6.1}ALU Pipe}{20}
\contentsline {subsection}{\numberline {3.6.2}Load Store Pipe}{26}
\contentsline {subsection}{\numberline {3.6.3}Branch Pipe}{31}
\contentsline {section}{\numberline {3.7}Reorder Buffer}{34}
\contentsline {section}{\numberline {3.8}Register Address Table}{37}
\contentsline {section}{\numberline {3.9}Design}{38}
\contentsline {chapter}{\numberline {4}Results}{39}
\contentsline {chapter}{\numberline {5}Conclusion and Future Work}{44}
\contentsline {section}{\numberline {5.1}Conclusion}{44}
\contentsline {section}{\numberline {5.2}Future Work}{44}
