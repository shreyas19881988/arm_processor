\documentclass{iitbthesis}
\usepackage[pdftex]{graphicx}



\usepackage{titlesec}
\titleformat{\chapter}[display]{\Large\centering\bfseries}{Chapter \thechapter}{2.5mm}{}{}
\titleformat{\section}[hang]{\large\bfseries\raggedright}{\thesection }{2mm}{}{}
\titleformat{\subsection}[hang]{\normalsize\bfseries\raggedright}{\thesubsection }{2mm}{}{}

\usepackage{subfig}
\usepackage{tikz}
\usetikzlibrary{shapes.geometric, arrows}
\usetikzlibrary{automata,positioning}
\tikzstyle{process} = [rectangle, minimum width=3cm, minimum height=1cm, text centered, draw=black]
\tikzstyle{decision} = [diamond, text width=4cm,minimum width=3cm, minimum height=1cm, text centered, draw=black]
\tikzstyle{arrow} = [thick,->,>=stealth]

%\usepackage{geometry}
\usepackage{fontspec}
\setmainfont{Times New Roman}
\usepackage{mathtools}
\usepackage{float}
\usepackage[colorinlistoftodos,textwidth=4cm,shadow]{todonotes}
\graphicspath{{./images/}}
\DeclareGraphicsExtensions{.jpg,.jpeg,.png}


%\setlength{\parindent}{12mm}
\begin{document}
\pagenumbering{roman}
\setguide{Prof. Siddhartha Duttagupta}
\iitbdegree{Master of Technology}
\department{Department of Electrical Engineering}
\dissertation
\rollnum{123070067}
\title{SPV Array Power Optimization using Low Power Computing}
\author{Shaunak Gupte}
\maketitle

\begin{dedication}
 \bf \it This work is dedicated to my family. I am deeply thankful for their continued motivation and support.
\end{dedication}
\pagenumbering{gobble}
\include{declaration}
\pagenumbering{roman}
\setcounter{page}{3}
\include{abstract}


\tableofcontents
\listoffigures
\listoftables
\cleardoublepage\pagenumbering{arabic}

\include{chap_intro}
\include{chap_system}
\include{chap_computing}
\include{chap_simulation}
\include{chap_results}
\include{chap_conclusion}
\include{bib}


\include{acknowledgement}
\end{document}