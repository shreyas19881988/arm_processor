`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module instr_issue_pipe_reg(
										input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_pipe_rn_in,
										input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_pipe_rm_in,
										input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_pipe_rs_in,
										input [`TAG_SIZE-1:0] tag_to_issue_pipe_cpsr_in,
										input [`RB_SIZE-1:0] valid_instructions_RB_in,
										input [`RB_SIZE-1:0] load_multiple_instr_RB_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_15_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_14_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_13_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_12_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_11_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_10_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_9_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_8_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_7_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_6_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_5_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_4_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_3_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_2_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_1_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] ldm_data_reg_0_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_23_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_22_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_21_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_20_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_19_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_18_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_17_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_16_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_15_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_14_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_13_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_12_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_11_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_10_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_9_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_8_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_7_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_6_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_5_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_4_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_3_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_2_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_1_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd_0_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_23_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_22_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_21_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_20_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_19_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_18_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_17_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_16_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_15_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_14_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_13_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_12_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_11_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_10_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_9_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_8_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_7_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_6_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_5_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_4_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_3_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_2_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_1_in,
										input [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn_0_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_23_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_22_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_21_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_20_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_19_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_18_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_17_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_16_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_15_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_14_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_13_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_12_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_11_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_10_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_9_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_8_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_7_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_6_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_5_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_4_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_3_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_2_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_1_in,
										input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_0_in,
										input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_pipe_rn_in,
										input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_pipe_rm_in,
										input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_pipe_rs_in,
										input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_pipe_cpsr_in,
										output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_pipe_rn_out,
										output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_pipe_rm_out,
										output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_pipe_rs_out,
										output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_pipe_cpsr_out
										
    );


wire [`RB_SIZE-1:0] decoded_addr_for_instr_issue_pipe_rn;
wire [`RB_SIZE-1:0] decoded_addr_for_instr_issue_pipe_rm;
wire [`RB_SIZE-1:0] decoded_addr_for_instr_issue_pipe_rs;
wire instr_issue_pipe_rn_ldm;
wire instr_issue_pipe_rm_ldm;
wire instr_issue_pipe_rs_ldm;
wire [`NO_OF_REG-1:0] instr_issue_pipe_rn_ldm_address_compare; 
wire [`NO_OF_REG-1:0] instr_issue_pipe_rm_ldm_address_compare; 
wire [`NO_OF_REG-1:0] instr_issue_pipe_rs_ldm_address_compare; 
wire [`LOG_ADDRESS_SIZE-1:0] address_of_reg_in_ldm_pipe_rn;
wire [`LOG_ADDRESS_SIZE-1:0] address_of_reg_in_ldm_pipe_rm;
wire [`LOG_ADDRESS_SIZE-1:0] address_of_reg_in_ldm_pipe_rs;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_to_issue_frm_ldm_pipe_rn;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_to_issue_frm_ldm_pipe_rm;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_to_issue_frm_ldm_pipe_rs;
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rd_for_instr_issue_pipe_rn;
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rd_for_instr_issue_pipe_rm;
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rd_for_instr_issue_pipe_rs;
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rn_for_instr_issue_pipe_rn;
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rn_for_instr_issue_pipe_rm;
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rn_for_instr_issue_pipe_rs;
wire data_in_rn_instr_issue_pipe_rn;
wire data_in_rn_instr_issue_pipe_rm;
wire data_in_rn_instr_issue_pipe_rs;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rd_or_rn_for_instr_issue_pipe_rn;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rd_or_rn_for_instr_issue_pipe_rm;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rd_or_rn_for_instr_issue_pipe_rs;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_RB_for_instr_issue_pipe_rn;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_RB_for_instr_issue_pipe_rm;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_RB_for_instr_issue_pipe_rs;
wire [`RB_SIZE-1:0] instr_to_issue_pipe_rn_valid;
wire [`RB_SIZE-1:0] instr_to_issue_pipe_rm_valid;
wire [`RB_SIZE-1:0] instr_to_issue_pipe_rs_valid;
wire [`RB_SIZE-1:0] decoded_addr_for_instr_issue_pipe_cpsr;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_cpsr_reg_for_instr_issue_pipe_cpsr;
wire [`RB_SIZE-1:0] instr_to_issue_pipe_cspr_valid;

/******RN_REG_STARTS******/
decoder_24 decoded_address_tag_to_issue_alu_pipe1_rn_addr (
				.tag_in(tag_to_issue_pipe_rn_in[`TAG_OF_INSTR_TO_ISSUE_START:
				`TAG_OF_INSTR_TO_ISSUE_END]),
				.decoded_addr_out(decoded_addr_for_instr_issue_pipe_rn)
				);


assign instr_to_issue_pipe_rn_valid = decoded_addr_for_instr_issue_pipe_rn & 

valid_instructions_RB_in;


mux_24_1 #1 ldm_instr_issue_pipe_rn (
    .y_out(instr_issue_pipe_rn_ldm), 
    .i0_in(load_multiple_instr_RB_in[23]), 
    .i1_in(load_multiple_instr_RB_in[22]), 
    .i2_in(load_multiple_instr_RB_in[21]), 
    .i3_in(load_multiple_instr_RB_in[20]), 
    .i4_in(load_multiple_instr_RB_in[19]), 
    .i5_in(load_multiple_instr_RB_in[18]), 
    .i6_in(load_multiple_instr_RB_in[17]), 
    .i7_in(load_multiple_instr_RB_in[16]), 
    .i8_in(load_multiple_instr_RB_in[15]), 
    .i9_in(load_multiple_instr_RB_in[14]), 
    .i10_in(load_multiple_instr_RB_in[13]), 
    .i11_in(load_multiple_instr_RB_in[12]), 
    .i12_in(load_multiple_instr_RB_in[11]), 
    .i13_in(load_multiple_instr_RB_in[10]), 
    .i14_in(load_multiple_instr_RB_in[9]), 
    .i15_in(load_multiple_instr_RB_in[8]), 
    .i16_in(load_multiple_instr_RB_in[7]), 
    .i17_in(load_multiple_instr_RB_in[6]), 
    .i18_in(load_multiple_instr_RB_in[5]), 
    .i19_in(load_multiple_instr_RB_in[4]), 
    .i20_in(load_multiple_instr_RB_in[3]), 
    .i21_in(load_multiple_instr_RB_in[2]), 
    .i22_in(load_multiple_instr_RB_in[1]), 
    .i23_in(load_multiple_instr_RB_in[0]), 
    .sel_in(tag_to_issue_pipe_rn_in[`TAG_OF_INSTR_TO_ISSUE_START:
				`TAG_OF_INSTR_TO_ISSUE_END])
    );


assign instr_issue_pipe_rn_ldm_address_compare[15] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_15_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[14] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_14_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[13] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_13_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[12] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_12_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[11] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_11_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[10] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_10_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[9] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_9_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[8] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_8_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[7] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_7_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[6] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_6_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[5] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_5_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[4] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_4_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[3] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_3_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[2] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_2_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[1] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_1_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rn_ldm_address_compare[0] = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_0_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


encoder_16 encoded_tag_compare_pipe_rn (
    .decoded_input_in(instr_issue_pipe_rn_ldm_address_compare), 
    .encoded_result_out(address_of_reg_in_ldm_pipe_rn)
    );


mux_16_1 #`MOD_REG_ADDR_SIZE reg_value_frm_ldm_pipe_rn (
    .y_out(reg_to_issue_frm_ldm_pipe_rn), 
    .i0_in(ldm_data_reg_15_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i1_in(ldm_data_reg_14_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i2_in(ldm_data_reg_13_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i3_in(ldm_data_reg_12_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i4_in(ldm_data_reg_11_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i5_in(ldm_data_reg_10_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i6_in(ldm_data_reg_9_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i7_in(ldm_data_reg_8_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i8_in(ldm_data_reg_7_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i9_in(ldm_data_reg_6_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i10_in(ldm_data_reg_5_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i11_in(ldm_data_reg_4_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i12_in(ldm_data_reg_3_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i13_in(ldm_data_reg_2_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i14_in(ldm_data_reg_1_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i15_in(ldm_data_reg_0_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .sel_in(address_of_reg_in_ldm_pipe_rn)
    );


mux_24_1 #`LOG_MOD_REG_ADDR_SIZE mux_for_selecting_rd_frm_instr_issue_pipe_rn (
    .y_out(reg_data_frm_rd_for_instr_issue_pipe_rn), 
    .i0_in(mod_reg_addr_rd_23_in), 
    .i1_in(mod_reg_addr_rd_22_in), 
    .i2_in(mod_reg_addr_rd_21_in), 
    .i3_in(mod_reg_addr_rd_20_in), 
    .i4_in(mod_reg_addr_rd_19_in), 
    .i5_in(mod_reg_addr_rd_18_in), 
    .i6_in(mod_reg_addr_rd_17_in), 
    .i7_in(mod_reg_addr_rd_16_in), 
    .i8_in(mod_reg_addr_rd_15_in), 
    .i9_in(mod_reg_addr_rd_14_in), 
    .i10_in(mod_reg_addr_rd_13_in), 
    .i11_in(mod_reg_addr_rd_12_in), 
    .i12_in(mod_reg_addr_rd_11_in), 
    .i13_in(mod_reg_addr_rd_10_in), 
    .i14_in(mod_reg_addr_rd_9_in), 
    .i15_in(mod_reg_addr_rd_8_in), 
    .i16_in(mod_reg_addr_rd_7_in), 
    .i17_in(mod_reg_addr_rd_6_in), 
    .i18_in(mod_reg_addr_rd_5_in), 
    .i19_in(mod_reg_addr_rd_4_in), 
    .i20_in(mod_reg_addr_rd_3_in), 
    .i21_in(mod_reg_addr_rd_2_in), 
    .i22_in(mod_reg_addr_rd_1_in), 
    .i23_in(mod_reg_addr_rd_0_in), 
    .sel_in(tag_to_issue_pipe_rn_in[`TAG_OF_INSTR_TO_ISSUE_START:`TAG_OF_INSTR_TO_ISSUE_END])
    );


mux_24_1 #`LOG_MOD_REG_ADDR_SIZE mux_for_selecting_rn_frm_instr_issue_pipe_rn (
    .y_out(reg_data_frm_rn_for_instr_issue_pipe_rn), 
    .i0_in(mod_reg_addr_rn_23_in), 
    .i1_in(mod_reg_addr_rn_22_in), 
    .i2_in(mod_reg_addr_rn_21_in), 
    .i3_in(mod_reg_addr_rn_20_in), 
    .i4_in(mod_reg_addr_rn_19_in), 
    .i5_in(mod_reg_addr_rn_18_in), 
    .i6_in(mod_reg_addr_rn_17_in), 
    .i7_in(mod_reg_addr_rn_16_in), 
    .i8_in(mod_reg_addr_rn_15_in), 
    .i9_in(mod_reg_addr_rn_14_in), 
    .i10_in(mod_reg_addr_rn_13_in), 
    .i11_in(mod_reg_addr_rn_12_in), 
    .i12_in(mod_reg_addr_rn_11_in), 
    .i13_in(mod_reg_addr_rn_10_in), 
    .i14_in(mod_reg_addr_rn_9_in), 
    .i15_in(mod_reg_addr_rn_8_in), 
    .i16_in(mod_reg_addr_rn_7_in), 
    .i17_in(mod_reg_addr_rn_6_in), 
    .i18_in(mod_reg_addr_rn_5_in), 
    .i19_in(mod_reg_addr_rn_4_in), 
    .i20_in(mod_reg_addr_rn_3_in), 
    .i21_in(mod_reg_addr_rn_2_in), 
    .i22_in(mod_reg_addr_rn_1_in), 
    .i23_in(mod_reg_addr_rn_0_in), 
    .sel_in(tag_to_issue_pipe_rn_in[`TAG_OF_INSTR_TO_ISSUE_START:
						 `TAG_OF_INSTR_TO_ISSUE_END])
    );


assign data_in_rn_instr_issue_pipe_rn = 

~(|(tag_to_issue_pipe_rn_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 

reg_data_frm_rn_for_instr_issue_pipe_rn[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign reg_data_frm_rd_or_rn_for_instr_issue_pipe_rn = data_in_rn_instr_issue_pipe_rn ? 

reg_data_frm_rn_for_instr_issue_pipe_rn[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END] : 

reg_data_frm_rd_for_instr_issue_pipe_rn[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END];


assign reg_data_frm_RB_for_instr_issue_pipe_rn = instr_issue_pipe_rn_ldm ? 

reg_to_issue_frm_ldm_pipe_rn : reg_data_frm_rd_or_rn_for_instr_issue_pipe_rn;


assign reg_data_to_issue_pipe_rn_out = |(instr_to_issue_pipe_rn_valid) ? 

reg_data_frm_RB_for_instr_issue_pipe_rn : data_frm_RAT_pipe_rn_in;
/******RN_REG_ENDS******/


/******RM_REG_STARTS******/
decoder_24 decoded_address_tag_to_issue_alu_pipe1_rm_addr (
				.tag_in(tag_to_issue_pipe_rm_in[`TAG_OF_INSTR_TO_ISSUE_START:
				`TAG_OF_INSTR_TO_ISSUE_END]),
				.decoded_addr_out(decoded_addr_for_instr_issue_pipe_rm)
				);


assign instr_to_issue_pipe_rm_valid = decoded_addr_for_instr_issue_pipe_rm & 

valid_instructions_RB_in;


mux_24_1 #1 ldm_instr_issue_pipe_rm (
    .y_out(instr_issue_pipe_rm_ldm), 
    .i0_in(load_multiple_instr_RB_in[23]), 
    .i1_in(load_multiple_instr_RB_in[22]), 
    .i2_in(load_multiple_instr_RB_in[21]), 
    .i3_in(load_multiple_instr_RB_in[20]), 
    .i4_in(load_multiple_instr_RB_in[19]), 
    .i5_in(load_multiple_instr_RB_in[18]), 
    .i6_in(load_multiple_instr_RB_in[17]), 
    .i7_in(load_multiple_instr_RB_in[16]), 
    .i8_in(load_multiple_instr_RB_in[15]), 
    .i9_in(load_multiple_instr_RB_in[14]), 
    .i10_in(load_multiple_instr_RB_in[13]), 
    .i11_in(load_multiple_instr_RB_in[12]), 
    .i12_in(load_multiple_instr_RB_in[11]), 
    .i13_in(load_multiple_instr_RB_in[10]), 
    .i14_in(load_multiple_instr_RB_in[9]), 
    .i15_in(load_multiple_instr_RB_in[8]), 
    .i16_in(load_multiple_instr_RB_in[7]), 
    .i17_in(load_multiple_instr_RB_in[6]), 
    .i18_in(load_multiple_instr_RB_in[5]), 
    .i19_in(load_multiple_instr_RB_in[4]), 
    .i20_in(load_multiple_instr_RB_in[3]), 
    .i21_in(load_multiple_instr_RB_in[2]), 
    .i22_in(load_multiple_instr_RB_in[1]), 
    .i23_in(load_multiple_instr_RB_in[0]), 
    .sel_in(tag_to_issue_pipe_rm_in[`TAG_OF_INSTR_TO_ISSUE_START:
				`TAG_OF_INSTR_TO_ISSUE_END])
    );


assign instr_issue_pipe_rm_ldm_address_compare[15] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_15_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[14] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_14_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[13] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_13_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[12] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_12_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[11] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_11_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[10] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_10_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[9] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_9_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[8] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_8_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[7] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_7_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[6] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_6_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[5] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_5_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[4] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_4_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[3] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_3_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[2] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_2_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[1] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_1_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rm_ldm_address_compare[0] = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_0_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


encoder_16 encoded_tag_compare_pipe_rm (
    .decoded_input_in(instr_issue_pipe_rm_ldm_address_compare), 
    .encoded_result_out(address_of_reg_in_ldm_pipe_rm)
    );


mux_16_1 #`MOD_REG_ADDR_SIZE reg_value_frm_ldm_pipe_rm (
    .y_out(reg_to_issue_frm_ldm_pipe_rm), 
    .i0_in(ldm_data_reg_15_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i1_in(ldm_data_reg_14_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i2_in(ldm_data_reg_13_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i3_in(ldm_data_reg_12_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i4_in(ldm_data_reg_11_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i5_in(ldm_data_reg_10_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i6_in(ldm_data_reg_9_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i7_in(ldm_data_reg_8_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i8_in(ldm_data_reg_7_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i9_in(ldm_data_reg_6_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i10_in(ldm_data_reg_5_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i11_in(ldm_data_reg_4_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i12_in(ldm_data_reg_3_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i13_in(ldm_data_reg_2_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i14_in(ldm_data_reg_1_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i15_in(ldm_data_reg_0_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .sel_in(address_of_reg_in_ldm_pipe_rm)
    );


mux_24_1 #`LOG_MOD_REG_ADDR_SIZE mux_for_selecting_rd_frm_instr_issue_pipe_rm (
    .y_out(reg_data_frm_rd_for_instr_issue_pipe_rm), 
    .i0_in(mod_reg_addr_rd_23_in), 
    .i1_in(mod_reg_addr_rd_22_in), 
    .i2_in(mod_reg_addr_rd_21_in), 
    .i3_in(mod_reg_addr_rd_20_in), 
    .i4_in(mod_reg_addr_rd_19_in), 
    .i5_in(mod_reg_addr_rd_18_in), 
    .i6_in(mod_reg_addr_rd_17_in), 
    .i7_in(mod_reg_addr_rd_16_in), 
    .i8_in(mod_reg_addr_rd_15_in), 
    .i9_in(mod_reg_addr_rd_14_in), 
    .i10_in(mod_reg_addr_rd_13_in), 
    .i11_in(mod_reg_addr_rd_12_in), 
    .i12_in(mod_reg_addr_rd_11_in), 
    .i13_in(mod_reg_addr_rd_10_in), 
    .i14_in(mod_reg_addr_rd_9_in), 
    .i15_in(mod_reg_addr_rd_8_in), 
    .i16_in(mod_reg_addr_rd_7_in), 
    .i17_in(mod_reg_addr_rd_6_in), 
    .i18_in(mod_reg_addr_rd_5_in), 
    .i19_in(mod_reg_addr_rd_4_in), 
    .i20_in(mod_reg_addr_rd_3_in), 
    .i21_in(mod_reg_addr_rd_2_in), 
    .i22_in(mod_reg_addr_rd_1_in), 
    .i23_in(mod_reg_addr_rd_0_in), 
    .sel_in(tag_to_issue_pipe_rm_in[`TAG_OF_INSTR_TO_ISSUE_START:`TAG_OF_INSTR_TO_ISSUE_END])
    );


mux_24_1 #`LOG_MOD_REG_ADDR_SIZE mux_for_selecting_rn_frm_instr_issue_pipe_rm (
    .y_out(reg_data_frm_rn_for_instr_issue_pipe_rm), 
    .i0_in(mod_reg_addr_rn_23_in), 
    .i1_in(mod_reg_addr_rn_22_in), 
    .i2_in(mod_reg_addr_rn_21_in), 
    .i3_in(mod_reg_addr_rn_20_in), 
    .i4_in(mod_reg_addr_rn_19_in), 
    .i5_in(mod_reg_addr_rn_18_in), 
    .i6_in(mod_reg_addr_rn_17_in), 
    .i7_in(mod_reg_addr_rn_16_in), 
    .i8_in(mod_reg_addr_rn_15_in), 
    .i9_in(mod_reg_addr_rn_14_in), 
    .i10_in(mod_reg_addr_rn_13_in), 
    .i11_in(mod_reg_addr_rn_12_in), 
    .i12_in(mod_reg_addr_rn_11_in), 
    .i13_in(mod_reg_addr_rn_10_in), 
    .i14_in(mod_reg_addr_rn_9_in), 
    .i15_in(mod_reg_addr_rn_8_in), 
    .i16_in(mod_reg_addr_rn_7_in), 
    .i17_in(mod_reg_addr_rn_6_in), 
    .i18_in(mod_reg_addr_rn_5_in), 
    .i19_in(mod_reg_addr_rn_4_in), 
    .i20_in(mod_reg_addr_rn_3_in), 
    .i21_in(mod_reg_addr_rn_2_in), 
    .i22_in(mod_reg_addr_rn_1_in), 
    .i23_in(mod_reg_addr_rn_0_in), 
    .sel_in(tag_to_issue_pipe_rm_in[`TAG_OF_INSTR_TO_ISSUE_START:
						 `TAG_OF_INSTR_TO_ISSUE_END])
    );


assign data_in_rn_instr_issue_pipe_rm = 

~(|(tag_to_issue_pipe_rm_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 

reg_data_frm_rn_for_instr_issue_pipe_rm[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign reg_data_frm_rd_or_rn_for_instr_issue_pipe_rm = data_in_rn_instr_issue_pipe_rm ? 

reg_data_frm_rn_for_instr_issue_pipe_rm[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END] : 

reg_data_frm_rd_for_instr_issue_pipe_rm[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END];


assign reg_data_frm_RB_for_instr_issue_pipe_rm = instr_issue_pipe_rm_ldm ? 

reg_to_issue_frm_ldm_pipe_rm : reg_data_frm_rd_or_rn_for_instr_issue_pipe_rm;


assign reg_data_to_issue_pipe_rm_out = |(instr_to_issue_pipe_rm_valid) ? 

reg_data_frm_RB_for_instr_issue_pipe_rm : data_frm_RAT_pipe_rm_in;
/******RM_REG_ENDS******/


/******RS_REG_STARTS******/
decoder_24 decoded_address_tag_to_issue_alu_pipe1_rs_addr (
				.tag_in(tag_to_issue_pipe_rs_in[`TAG_OF_INSTR_TO_ISSUE_START:
				`TAG_OF_INSTR_TO_ISSUE_END]),
				.decoded_addr_out(decoded_addr_for_instr_issue_pipe_rs)
				);


assign instr_to_issue_pipe_rs_valid = decoded_addr_for_instr_issue_pipe_rs & 

valid_instructions_RB_in;


mux_24_1 #1 ldm_instr_issue_pipe_rs (
    .y_out(instr_issue_pipe_rs_ldm), 
    .i0_in(load_multiple_instr_RB_in[23]), 
    .i1_in(load_multiple_instr_RB_in[22]), 
    .i2_in(load_multiple_instr_RB_in[21]), 
    .i3_in(load_multiple_instr_RB_in[20]), 
    .i4_in(load_multiple_instr_RB_in[19]), 
    .i5_in(load_multiple_instr_RB_in[18]), 
    .i6_in(load_multiple_instr_RB_in[17]), 
    .i7_in(load_multiple_instr_RB_in[16]), 
    .i8_in(load_multiple_instr_RB_in[15]), 
    .i9_in(load_multiple_instr_RB_in[14]), 
    .i10_in(load_multiple_instr_RB_in[13]), 
    .i11_in(load_multiple_instr_RB_in[12]), 
    .i12_in(load_multiple_instr_RB_in[11]), 
    .i13_in(load_multiple_instr_RB_in[10]), 
    .i14_in(load_multiple_instr_RB_in[9]), 
    .i15_in(load_multiple_instr_RB_in[8]), 
    .i16_in(load_multiple_instr_RB_in[7]), 
    .i17_in(load_multiple_instr_RB_in[6]), 
    .i18_in(load_multiple_instr_RB_in[5]), 
    .i19_in(load_multiple_instr_RB_in[4]), 
    .i20_in(load_multiple_instr_RB_in[3]), 
    .i21_in(load_multiple_instr_RB_in[2]), 
    .i22_in(load_multiple_instr_RB_in[1]), 
    .i23_in(load_multiple_instr_RB_in[0]), 
    .sel_in(tag_to_issue_pipe_rs_in[`TAG_OF_INSTR_TO_ISSUE_START:
				`TAG_OF_INSTR_TO_ISSUE_END])
    );


assign instr_issue_pipe_rs_ldm_address_compare[15] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_15_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[14] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_14_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[13] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_13_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[12] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_12_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[11] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_11_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[10] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_10_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[9] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_9_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[8] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_8_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[7] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_7_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[6] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_6_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[5] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_5_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[4] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_4_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[3] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_3_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[2] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_2_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[1] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_1_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign instr_issue_pipe_rs_ldm_address_compare[0] = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 
	
ldm_data_reg_0_in[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


encoder_16 encoded_tag_compare_pipe_rs (
    .decoded_input_in(instr_issue_pipe_rs_ldm_address_compare), 
    .encoded_result_out(address_of_reg_in_ldm_pipe_rs)
    );


mux_16_1 #`MOD_REG_ADDR_SIZE reg_value_frm_ldm_pipe_rs (
    .y_out(reg_to_issue_frm_ldm_pipe_rs), 
    .i0_in(ldm_data_reg_15_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i1_in(ldm_data_reg_14_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i2_in(ldm_data_reg_13_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i3_in(ldm_data_reg_12_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i4_in(ldm_data_reg_11_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i5_in(ldm_data_reg_10_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i6_in(ldm_data_reg_9_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i7_in(ldm_data_reg_8_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i8_in(ldm_data_reg_7_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i9_in(ldm_data_reg_6_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i10_in(ldm_data_reg_5_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i11_in(ldm_data_reg_4_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i12_in(ldm_data_reg_3_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i13_in(ldm_data_reg_2_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i14_in(ldm_data_reg_1_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .i15_in(ldm_data_reg_0_in[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END]), 
    .sel_in(address_of_reg_in_ldm_pipe_rs)
    );


mux_24_1 #`LOG_MOD_REG_ADDR_SIZE mux_for_selecting_rd_frm_instr_issue_pipe_rs (
    .y_out(reg_data_frm_rd_for_instr_issue_pipe_rs), 
    .i0_in(mod_reg_addr_rd_23_in), 
    .i1_in(mod_reg_addr_rd_22_in), 
    .i2_in(mod_reg_addr_rd_21_in), 
    .i3_in(mod_reg_addr_rd_20_in), 
    .i4_in(mod_reg_addr_rd_19_in), 
    .i5_in(mod_reg_addr_rd_18_in), 
    .i6_in(mod_reg_addr_rd_17_in), 
    .i7_in(mod_reg_addr_rd_16_in), 
    .i8_in(mod_reg_addr_rd_15_in), 
    .i9_in(mod_reg_addr_rd_14_in), 
    .i10_in(mod_reg_addr_rd_13_in), 
    .i11_in(mod_reg_addr_rd_12_in), 
    .i12_in(mod_reg_addr_rd_11_in), 
    .i13_in(mod_reg_addr_rd_10_in), 
    .i14_in(mod_reg_addr_rd_9_in), 
    .i15_in(mod_reg_addr_rd_8_in), 
    .i16_in(mod_reg_addr_rd_7_in), 
    .i17_in(mod_reg_addr_rd_6_in), 
    .i18_in(mod_reg_addr_rd_5_in), 
    .i19_in(mod_reg_addr_rd_4_in), 
    .i20_in(mod_reg_addr_rd_3_in), 
    .i21_in(mod_reg_addr_rd_2_in), 
    .i22_in(mod_reg_addr_rd_1_in), 
    .i23_in(mod_reg_addr_rd_0_in), 
    .sel_in(tag_to_issue_pipe_rs_in[`TAG_OF_INSTR_TO_ISSUE_START:`TAG_OF_INSTR_TO_ISSUE_END])
    );


mux_24_1 #`LOG_MOD_REG_ADDR_SIZE mux_for_selecting_rn_frm_instr_issue_pipe_rs (
    .y_out(reg_data_frm_rn_for_instr_issue_pipe_rs), 
    .i0_in(mod_reg_addr_rn_23_in), 
    .i1_in(mod_reg_addr_rn_22_in), 
    .i2_in(mod_reg_addr_rn_21_in), 
    .i3_in(mod_reg_addr_rn_20_in), 
    .i4_in(mod_reg_addr_rn_19_in), 
    .i5_in(mod_reg_addr_rn_18_in), 
    .i6_in(mod_reg_addr_rn_17_in), 
    .i7_in(mod_reg_addr_rn_16_in), 
    .i8_in(mod_reg_addr_rn_15_in), 
    .i9_in(mod_reg_addr_rn_14_in), 
    .i10_in(mod_reg_addr_rn_13_in), 
    .i11_in(mod_reg_addr_rn_12_in), 
    .i12_in(mod_reg_addr_rn_11_in), 
    .i13_in(mod_reg_addr_rn_10_in), 
    .i14_in(mod_reg_addr_rn_9_in), 
    .i15_in(mod_reg_addr_rn_8_in), 
    .i16_in(mod_reg_addr_rn_7_in), 
    .i17_in(mod_reg_addr_rn_6_in), 
    .i18_in(mod_reg_addr_rn_5_in), 
    .i19_in(mod_reg_addr_rn_4_in), 
    .i20_in(mod_reg_addr_rn_3_in), 
    .i21_in(mod_reg_addr_rn_2_in), 
    .i22_in(mod_reg_addr_rn_1_in), 
    .i23_in(mod_reg_addr_rn_0_in), 
    .sel_in(tag_to_issue_pipe_rs_in[`TAG_OF_INSTR_TO_ISSUE_START:
						 `TAG_OF_INSTR_TO_ISSUE_END])
    );


assign data_in_rn_instr_issue_pipe_rs = 

~(|(tag_to_issue_pipe_rs_in[`LOG_ADDR_OF_REG_TO_ISSUE_START:`LOG_ADDR_OF_REG_TO_ISSUE_END] ^ 

reg_data_frm_rn_for_instr_issue_pipe_rs[`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]));


assign reg_data_frm_rd_or_rn_for_instr_issue_pipe_rs = data_in_rn_instr_issue_pipe_rs ? 

reg_data_frm_rn_for_instr_issue_pipe_rs[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END] : 

reg_data_frm_rd_for_instr_issue_pipe_rs[`MOD_REG_ADDR_START:`MOD_REG_ADDR_END];


assign reg_data_frm_RB_for_instr_issue_pipe_rs = instr_issue_pipe_rs_ldm ? 

reg_to_issue_frm_ldm_pipe_rs : reg_data_frm_rd_or_rn_for_instr_issue_pipe_rs;


assign reg_data_to_issue_pipe_rs_out = |(instr_to_issue_pipe_rs_valid) ? 

reg_data_frm_RB_for_instr_issue_pipe_rs : data_frm_RAT_pipe_rs_in;
/******RS_REG_ENDS******/


/******CPSR_STARTS******/
decoder_24 decoded_address_tag_to_issue_alu_pipe1_cpsr_addr (
				.tag_in(tag_to_issue_pipe_cpsr_in),
				.decoded_addr_out(decoded_addr_for_instr_issue_pipe_cpsr)
				);


assign instr_to_issue_pipe_cspr_valid = decoded_addr_for_instr_issue_pipe_cpsr & 

valid_instructions_RB_in;


mux_24_1 #`MOD_REG_ADDR_SIZE mux_for_selecting_cpsr_data (
    .y_out(reg_data_frm_cpsr_reg_for_instr_issue_pipe_cpsr), 
    .i0_in(mod_reg_addr_cpsr_23_in), 
    .i1_in(mod_reg_addr_cpsr_22_in), 
    .i2_in(mod_reg_addr_cpsr_21_in), 
    .i3_in(mod_reg_addr_cpsr_20_in), 
    .i4_in(mod_reg_addr_cpsr_19_in), 
    .i5_in(mod_reg_addr_cpsr_18_in), 
    .i6_in(mod_reg_addr_cpsr_17_in), 
    .i7_in(mod_reg_addr_cpsr_16_in), 
    .i8_in(mod_reg_addr_cpsr_15_in), 
    .i9_in(mod_reg_addr_cpsr_14_in), 
    .i10_in(mod_reg_addr_cpsr_13_in), 
    .i11_in(mod_reg_addr_cpsr_12_in), 
    .i12_in(mod_reg_addr_cpsr_11_in), 
    .i13_in(mod_reg_addr_cpsr_10_in), 
    .i14_in(mod_reg_addr_cpsr_9_in), 
    .i15_in(mod_reg_addr_cpsr_8_in), 
    .i16_in(mod_reg_addr_cpsr_7_in), 
    .i17_in(mod_reg_addr_cpsr_6_in), 
    .i18_in(mod_reg_addr_cpsr_5_in), 
    .i19_in(mod_reg_addr_cpsr_4_in), 
    .i20_in(mod_reg_addr_cpsr_3_in), 
    .i21_in(mod_reg_addr_cpsr_2_in), 
    .i22_in(mod_reg_addr_cpsr_1_in), 
    .i23_in(mod_reg_addr_cpsr_0_in), 
    .sel_in(tag_to_issue_pipe_cpsr_in)
    );


assign reg_data_to_issue_pipe_cpsr_out = |(instr_to_issue_pipe_cspr_valid) ? 

reg_data_frm_cpsr_reg_for_instr_issue_pipe_cpsr : data_frm_RAT_pipe_cpsr_in;
/******CPSR_ENDS******/

endmodule
