`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"



module tb_valid_instr_in_RB;

	// Inputs
	reg [`RB_SIZE-1:0] head_pointer1_in;
	reg [`RB_SIZE-1:0] tail_pointer1_in;
	reg [`RB_SIZE-1:0] tag_position_in_RB_in;
	wire [`RB_SIZE-1:0] valid_instr_out;
	wire [`RB_SIZE-1:0] valid_instr_above_tag_out;
	
	// Instantiate the Unit Under Test (UUT)
	valid_instr_in_RB uut (
		.head_pointer1_in(head_pointer1_in), 
		.tail_pointer1_in(tail_pointer1_in),
		.tag_position_in_RB_in(tag_position_in_RB_in),
		.valid_instr_out(valid_instr_out),
		.valid_instr_above_tag_out(valid_instr_above_tag_out)
		);

	initial begin
		// Initialize Inputs
		head_pointer1_in = 0;
		tail_pointer1_in = 0;
		tag_position_in_RB_in = 0;

		// Wait 100 ns for global reset to finish
		#100;
		head_pointer1_in <= `RB_SIZE'b0001_0000_0000_0000_0000_0000;
      tail_pointer1_in <= `RB_SIZE'b0100_0000_0000_0000_0000_0000;
		tag_position_in_RB_in <= `RB_SIZE'b0000_0001_0000_0000_0000_0000;
		#10;
		head_pointer1_in <= `RB_SIZE'b0000_0000_0000_0010_0000_0000;
      tail_pointer1_in <= `RB_SIZE'b0000_0000_0000_0000_0000_1000;
		tag_position_in_RB_in <= `RB_SIZE'b0000_0000_0000_0001_0000_0000;
		#10;
		head_pointer1_in <= `RB_SIZE'b0000_0000_0001_0000_0000_0000;
      tail_pointer1_in <= `RB_SIZE'b0000_0001_0000_0000_0000_0000;
		tag_position_in_RB_in <= `RB_SIZE'b0000_1000_0000_0000_0000_0000;
		// Add stimulus here

	end
      
endmodule

