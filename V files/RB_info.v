`define RB_SIZE 24

/******RD_RN_CPSR_EN******/
`define RD_RN_CPSR_EN 3

`define RD_EN 2

`define RN_EN 1

`define CPSR_EN 0
/******RD_RN_CPSR_EN******/

/******TAG_RD_RN_CPSR******/
`define TAG_TO_CHANGE_RD_RN_CPSR 15

`define TAG_TO_CHANGE_RD_STARTS 14

`define TAG_TO_CHANGE_RD_ENDS 10

`define TAG_TO_CHANGE_RN_STARTS 9

`define TAG_TO_CHANGE_RN_ENDS 5

`define TAG_TO_CHANGE_CPSR_STARTS 4

`define TAG_TO_CHANGE_CPSR_ENDS 0
/******TAG_RD_RN_CPSR******/


/******TAG_OF_INSTR_IN_PIPES*****/
`define PIPES_INSTR_TAG_WORD 20

`define ALU_PIPE1_INSTR_TAG_START 19

`define ALU_PIPE1_INSTR_TAG_END 15 

`define ALU_PIPE2_INSTR_TAG_START 14

`define ALU_PIPE2_INSTR_TAG_END 10

`define LS_PIPE3_INSTR_TAG_START 9

`define LS_PIPE3_INSTR_TAG_END 5

`define BRANCH_PIPE4_INSTR_TAG_START 4

`define BRANCH_PIPE4_INSTR_TAG_END 0
/******TAG_OF_INSTR_IN_PIPES*****/

`define MOD_REG_ADDR_SIZE 7

/******Modified register data******/
`define LDM_DATA_EN 11

`define LOG_MOD_REG_ADDR_SIZE 11

`define LOG_REG_ADDR_START 10

`define LOG_REG_ADDR_END 7

`define MOD_REG_ADDR_START 6

`define MOD_REG_ADDR_END 0
/******Modified register data******/

/******MODIFIED_RD_REGISTER_ADDRESS_WORD******/
`define LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE 44

`define LOG_REG_ADDR_ALU_PIPE1_START 43

`define LOG_REG_ADDR_ALU_PIPE1_END 40

`define MOD_REG_ADDR_ALU_PIPE1_START 39

`define MOD_REG_ADDR_ALU_PIPE1_END 33

`define LOG_REG_ADDR_ALU_PIPE2_START 32

`define LOG_REG_ADDR_ALU_PIPE2_END 29

`define MOD_REG_ADDR_ALU_PIPE2_START 28

`define MOD_REG_ADDR_ALU_PIPE2_END 22

`define LOG_REG_ADDR_LS_PIPE3_START 21

`define LOG_REG_ADDR_LS_PIPE3_END 18

`define MOD_REG_ADDR_LS_PIPE3_START 17

`define MOD_REG_ADDR_LS_PIPE3_END 11

`define LOG_REG_ADDR_BRANCH_PIPE4_START 10

`define LOG_REG_ADDR_BRANCH_PIPE4_END 7

`define MOD_REG_ADDR_BRANCH_PIPE4_START 6

`define MOD_REG_ADDR_BRANCH_PIPE4_END 0
/******MODIFIED_RD_REGISTER_ADDRESS_WORD******/

/******MODIFIED_CPSR_DATA_ADDR_WORD******/
`define MOD_CPSR_ADDR_4_PIPES_COMBINED_SIZE 28

`define MOD_CPSR_ADDR_ALU_PIPE1_START 27

`define MOD_CPSR_ADDR_ALU_PIPE1_END 21

`define MOD_CPSR_ADDR_ALU_PIPE2_START 20

`define MOD_CPSR_ADDR_ALU_PIPE2_END 14

`define MOD_CPSR_ADDR_LS_PIPE3_START 13

`define MOD_CPSR_ADDR_LS_PIPE3_END 7

`define MOD_CPSR_ADDR_BRANCH_PIPE4_START 6

`define MOD_CPSR_ADDR_BRANCH_PIPE4_END 0
/******MODIFIED_CPSR_DATA_ADDR_WORD******/

/******REORDER_BUFFER_ENTRY_WORD******/
`define RB_ENTRY_WORD 31

`define RB_TAG_START 30

`define RB_TAG_END 26

`define RB_SPECULATIVE_INSTR 25

`define RB_CPSR_OVERFLOW_CONTROL 24

`define RB_RD_UPDATE 23

`define RB_RN_UPDATE 22

`define RB_CPSR_UPDATE 21

`define RB_ALU_TYPE_INSTR 20

`define RB_LOAD_STORE_TYPE_INSTR 19

`define RB_LOAD_MULTIPLE_TYPE_INSTR 18

`define RB_STORE_MULTIPLE_TYPE_INSTR 17

`define RB_BRANCH_TYPE_INSTR 16

`define RB_LDM_REG_R15 15

`define RB_LDM_REG_R14 14

`define RB_LDM_REG_R13 13

`define RB_LDM_REG_R12 12

`define RB_LDM_REG_R11 11

`define RB_LDM_REG_R10 10

`define RB_LDM_REG_R9 9

`define RB_LDM_REG_R8 8

`define RB_LDM_REG_R7 7

`define RB_LDM_REG_R6 6

`define RB_LDM_REG_R5 5

`define RB_LDM_REG_R4 4

`define RB_LDM_REG_R3 3

`define RB_LDM_REG_R2 2

`define RB_LDM_REG_R1 1

`define RB_LDM_REG_R0 0
/******REORDER_BUFFER_ENTRY_WORD******/


/******TAG_TO_ISSUE_PLUS_LOG_REG******/
`define TAG_TO_ISSUE_PLUS_LOG_REG 9

`define TAG_OF_INSTR_TO_ISSUE_START 8

`define TAG_OF_INSTR_TO_ISSUE_END 4

`define LOG_ADDR_OF_REG_TO_ISSUE_START 3

`define LOG_ADDR_OF_REG_TO_ISSUE_END 0
/******TAG_TO_ISSUE_PLUS_LOG_REG******/

