`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module decoder_64(
						input [`decoder_input_64-1:0] addr_in,
						output reg [`decoder_output_64-1:0] decoded_addr_out
    );

always@(*)
begin
	case(addr_in)
		`decoder_input_64'b000000 : 
		begin
			decoded_addr_out = `decoder_output_64'b10000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b000001 : 
		begin
			decoded_addr_out = `decoder_output_64'b01000000_00000000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b000010 : 
		begin
			decoded_addr_out = `decoder_output_64'b00100000_00000000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b000011 : 
		begin
			decoded_addr_out = `decoder_output_64'b00010000_00000000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b000100 : 
		begin
			decoded_addr_out = `decoder_output_64'b00001000_00000000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b000101 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000100_00000000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b000110 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000010_00000000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b000111 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000001_00000000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b001000 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_10000000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b001001 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_01000000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b001010 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00100000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b001011 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00010000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b001100 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00001000_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b001101 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000100_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b001110 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000010_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b001111 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000001_00000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b010000 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_10000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b010001 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_01000000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b010010 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00100000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b010011 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00010000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b010100 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00001000_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b010101 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000100_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b010110 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000010_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b010111 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000001_00000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b011000 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_10000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b011001 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_01000000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b011010 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00100000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b011011 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00010000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b011100 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00001000_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b001101 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000100_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b011110 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000010_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b011111 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000001_00000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b100000 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_10000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b100001 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_01000000_00000000_00000000_00000000;
		end
		`decoder_input_64'b100010 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00100000_00000000_00000000_00000000;
		end
		`decoder_input_64'b100011 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00010000_00000000_00000000_00000000;
		end
		`decoder_input_64'b100100 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00001000_00000000_00000000_00000000;
		end
		`decoder_input_64'b100101 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000100_00000000_00000000_00000000;
		end
		`decoder_input_64'b100110 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000010_00000000_00000000_00000000;
		end
		`decoder_input_64'b100111 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000001_00000000_00000000_00000000;
		end
		`decoder_input_64'b101000 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_10000000_00000000_00000000;
		end
		`decoder_input_64'b101001 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_01000000_00000000_00000000;
		end
		`decoder_input_64'b101010 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00100000_00000000_00000000;
		end
		`decoder_input_64'b101011 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00010000_00000000_00000000;
		end
		`decoder_input_64'b101100 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00001000_00000000_00000000;
		end
		`decoder_input_64'b101101 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000100_00000000_00000000;
		end
		`decoder_input_64'b101110 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000010_00000000_00000000;
		end
		`decoder_input_64'b101111 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000001_00000000_00000000;
		end
		`decoder_input_64'b110000 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_10000000_00000000;
		end
		`decoder_input_64'b110001 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_01000000_00000000;
		end
		`decoder_input_64'b110010 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00100000_00000000;
		end
		`decoder_input_64'b110011 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00010000_00000000;
		end
		`decoder_input_64'b110100 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00001000_00000000;
		end
		`decoder_input_64'b110101 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000100_00000000;
		end
		`decoder_input_64'b110110 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000010_00000000;
		end
		`decoder_input_64'b110111 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000001_00000000;
		end
		`decoder_input_64'b111000 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000000_10000000;
		end
		`decoder_input_64'b111001 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000000_01000000;
		end
		`decoder_input_64'b111010 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00100000;
		end
		`decoder_input_64'b111011 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00010000;
		end
		`decoder_input_64'b111100 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00001000;
		end
		`decoder_input_64'b111101 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000100;
		end
		`decoder_input_64'b111110 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000010;
		end
		`decoder_input_64'b111111 : 
		begin
			decoded_addr_out = `decoder_output_64'b00000000_00000000_00000000_00000000_00000000_00000000_00000000_00000001;
		end
		default : 
		begin
			decoded_addr_out = 0;
		end
	endcase
end
endmodule
