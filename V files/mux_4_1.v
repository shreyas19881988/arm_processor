`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module mux_4_1(y_out,i0_in,i1_in,i2_in,i3_in,sel_in);

parameter BUS_WIDTH = 5;

output [BUS_WIDTH-1 : 0] y_out;
input [BUS_WIDTH-1 : 0] i0_in;
input [BUS_WIDTH-1 : 0] i1_in;
input [BUS_WIDTH-1 : 0] i2_in;
input [BUS_WIDTH-1 : 0] i3_in;
	
input [`ENCODED_NO_OF_PIPES_MUX_SEL-1:0] sel_in;

reg [BUS_WIDTH-1 : 0] y_out;
wire [BUS_WIDTH-1 : 0] i0_in;
wire [BUS_WIDTH-1 : 0] i1_in;
wire [BUS_WIDTH-1 : 0] i2_in;
wire [BUS_WIDTH-1 : 0] i3_in;
wire [`ENCODED_NO_OF_PIPES_MUX_SEL-1:0] sel_in;

always @(*)
begin
	case (sel_in)
		`ENCODED_NO_OF_PIPES_MUX_SEL'b00 : y_out <= i0_in;
		`ENCODED_NO_OF_PIPES_MUX_SEL'b01 : y_out <= i1_in;
		`ENCODED_NO_OF_PIPES_MUX_SEL'b10 : y_out <= i2_in;
		`ENCODED_NO_OF_PIPES_MUX_SEL'b11 : y_out <= i3_in;
	endcase
end

endmodule
