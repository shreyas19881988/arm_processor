`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module generating_tag_retire(
				input [`LOG_ADDRESS_SIZE-1:0] log_reg_addr_rd_in,
				input [`LOG_ADDRESS_SIZE-1:0] log_reg_addr_rn_in,
				input [`RB_SIZE-1:0] valid_instr_rd_retire_in,
				input [`RB_SIZE-1:0] valid_instr_rn_retire_in,
				input [`RB_SIZE-1:0] valid_instr_cpsr_retire_in,
				input [`TAG_SIZE-1:0] tag_instr_in,
				output [`NO_OF_REG_AND_CPSR-1:0] tag_retire_en_out,
				output [`TAG_ALL_REG_AND_CPSR-1:0] tag_to_retire_out
    );

wire [`NO_OF_REG-1:0] decoded_rd_addr; 
wire [`NO_OF_REG-1:0] decoded_rn_addr;
wire [`NO_OF_REG-1:0] decoded_cpsr_addr;
wire or_valid_instr_rd_retire_in,or_valid_instr_rn_retire_in,or_valid_instr_cpsr_retire_in;
wire [`NO_OF_REG_AND_CPSR-1:0] decoded_valid_rd_and_cpsr;
wire [`NO_OF_REG_AND_CPSR-1:0] decoded_valid_rn_and_cpsr;
wire [`NO_OF_REG_AND_CPSR-1:0] decoded_valid_cpsr;
wire [`TAG_ALL_REG_AND_CPSR-1:0] tag_to_retire_rd;
wire [`TAG_ALL_REG_AND_CPSR-1:0] tag_to_retire_rn;
wire [`TAG_ALL_REG_AND_CPSR-1:0] tag_to_retire_cpsr;


/******RD STARTS******/
decoder_16 decoded_reg_addr(
				.addr_in(log_reg_addr_rd_in),
				.decoded_addr_out(decoded_rd_addr)
    );


assign or_valid_instr_rd_retire_in = |(valid_instr_rd_retire_in);


assign decoded_valid_rd_and_cpsr = {`NO_OF_REG_AND_CPSR{or_valid_instr_rd_retire_in}} & 

{decoded_rd_addr,1'b0};


assign tag_to_retire_rd[`TAG_FOR_R15_START:`TAG_FOR_R15_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R15]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_R14_START:`TAG_FOR_R14_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R14]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_R13_START:`TAG_FOR_R13_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R13]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_R12_START:`TAG_FOR_R12_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R12]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_R11_START:`TAG_FOR_R11_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R11]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_R10_START:`TAG_FOR_R10_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R10]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_R9_START:`TAG_FOR_R9_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R9]}} & tag_instr_in;	


assign tag_to_retire_rd[`TAG_FOR_R8_START:`TAG_FOR_R8_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R8]}} & tag_instr_in;	


assign tag_to_retire_rd[`TAG_FOR_R7_START:`TAG_FOR_R7_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R7]}} & tag_instr_in;	
	 

assign tag_to_retire_rd[`TAG_FOR_R6_START:`TAG_FOR_R6_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R6]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_R5_START:`TAG_FOR_R5_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R5]}} & tag_instr_in;
	

assign tag_to_retire_rd[`TAG_FOR_R4_START:`TAG_FOR_R4_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R4]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_R3_START:`TAG_FOR_R3_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R3]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_R2_START:`TAG_FOR_R2_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R2]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_R1_START:`TAG_FOR_R1_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R1]}} & tag_instr_in;	


assign tag_to_retire_rd[`TAG_FOR_R0_START:`TAG_FOR_R0_END] = 

{`TAG_SIZE{decoded_valid_rd_and_cpsr[`INCLUDING_CPSR_R0]}} & tag_instr_in;


assign tag_to_retire_rd[`TAG_FOR_CPSR_START:`TAG_FOR_CPSR_END] = 0;
/******RD ENDS******/


/******RN STARTS******/
decoder_16 decoded_reg_addr(
				.addr_in(log_reg_addr_rn_in),
				.decoded_addr_out(decoded_rn_addr)
    );


assign or_valid_instr_rn_retire_in = |(valid_instr_rn_retire_in);


assign decoded_valid_rn_and_cpsr = {`NO_OF_REG_AND_CPSR{or_valid_instr_rn_retire_in}} & 

{decoded_rn_addr,1'b0};


assign tag_to_retire_rn[`TAG_FOR_R15_START:`TAG_FOR_R15_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R15]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_R14_START:`TAG_FOR_R14_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R14]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_R13_START:`TAG_FOR_R13_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R13]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_R12_START:`TAG_FOR_R12_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R12]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_R11_START:`TAG_FOR_R11_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R11]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_R10_START:`TAG_FOR_R10_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R10]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_R9_START:`TAG_FOR_R9_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R9]}} & tag_instr_in;	


assign tag_to_retire_rn[`TAG_FOR_R8_START:`TAG_FOR_R8_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R8]}} & tag_instr_in;	


assign tag_to_retire_rn[`TAG_FOR_R7_START:`TAG_FOR_R7_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R7]}} & tag_instr_in;	
	 

assign tag_to_retire_rn[`TAG_FOR_R6_START:`TAG_FOR_R6_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R6]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_R5_START:`TAG_FOR_R5_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R5]}} & tag_instr_in;
	

assign tag_to_retire_rn[`TAG_FOR_R4_START:`TAG_FOR_R4_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R4]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_R3_START:`TAG_FOR_R3_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R3]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_R2_START:`TAG_FOR_R2_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R2]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_R1_START:`TAG_FOR_R1_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R1]}} & tag_instr_in;	


assign tag_to_retire_rn[`TAG_FOR_R0_START:`TAG_FOR_R0_END] = 

{`TAG_SIZE{decoded_valid_rn_and_cpsr[`R0]}} & tag_instr_in;


assign tag_to_retire_rn[`TAG_FOR_CPSR_START:`TAG_FOR_CPSR_END] = 0;
/******RN ENDS******/

/******CPSR STARTS******/
assign or_valid_instr_cpsr_retire_in = |(valid_instr_cpsr_retire_in);


assign decoded_valid_cpsr = {16'b0,or_valid_instr_cpsr_retire_in};


assign tag_to_retire_cpsr[`TAG_FOR_R15_START:`TAG_FOR_R15_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R14_START:`TAG_FOR_R14_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R13_START:`TAG_FOR_R13_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R12_START:`TAG_FOR_R12_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R11_START:`TAG_FOR_R11_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R10_START:`TAG_FOR_R10_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R9_START:`TAG_FOR_R9_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R8_START:`TAG_FOR_R8_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R7_START:`TAG_FOR_R7_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R6_START:`TAG_FOR_R6_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R5_START:`TAG_FOR_R5_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R4_START:`TAG_FOR_R4_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R3_START:`TAG_FOR_R3_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R2_START:`TAG_FOR_R2_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R1_START:`TAG_FOR_R1_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_R0_START:`TAG_FOR_R0_END] = 0;


assign tag_to_retire_cpsr[`TAG_FOR_CPSR_START:`TAG_FOR_CPSR_END] = 

{`TAG_SIZE{decoded_valid_cpsr[`CPSR]}} & tag_instr_in;
/******CPSR ENDS******/


assign tag_retire_en_out = decoded_valid_rd_and_cpsr | decoded_valid_rn_and_cpsr | decoded_valid_cpsr;


assign tag_to_retire_out = tag_to_retire_rd | tag_to_retire_rn | tag_to_retire_cpsr;

endmodule
