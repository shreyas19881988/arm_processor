`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module tb_free_list_controller_alu_pipes;

	// Inputs
	reg clk_in;
	reg reset_in;
	reg reg_issue_alu_pipe1_rd_in;
	reg reg_issue_alu_pipe1_rn_in;
	reg reg_issue_alu_pipe2_rd_in;
	reg reg_issue_alu_pipe2_rn_in;
	reg [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] retired_instr1_rd_addr_in;
	reg retire_en_instr1_rd_in;
	reg [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] retired_instr1_rn_addr_in;
	reg retire_en_instr1_rn_in;
	reg [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] retired_instr2_rd_addr_in;
	reg retire_en_instr2_rd_in;
	reg [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] retired_instr2_rn_addr_in;
	reg retire_en_instr2_rn_in;

	// Outputs
	wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] reg_addr_issued_to_alu_pipe1_rd_out;
	wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] reg_addr_issued_to_alu_pipe1_rn_out;
	wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] reg_addr_issued_to_alu_pipe2_rd_out;
	wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] reg_addr_issued_to_alu_pipe2_rn_out;

	// Instantiate the Unit Under Test (UUT)
	free_list_controller_alu_pipes uut (
		.clk_in(clk_in), 
		.reset_in(reset_in), 
		.reg_issue_alu_pipe1_rd_in(reg_issue_alu_pipe1_rd_in), 
		.reg_issue_alu_pipe1_rn_in(reg_issue_alu_pipe1_rn_in), 
		.reg_issue_alu_pipe2_rd_in(reg_issue_alu_pipe2_rd_in), 
		.reg_issue_alu_pipe2_rn_in(reg_issue_alu_pipe2_rn_in), 
		.retired_instr1_rd_addr_in(retired_instr1_rd_addr_in), 
		.retire_en_instr1_rd_in(retire_en_instr1_rd_in), 
		.retired_instr1_rn_addr_in(retired_instr1_rn_addr_in), 
		.retire_en_instr1_rn_in(retire_en_instr1_rn_in), 
		.retired_instr2_rd_addr_in(retired_instr2_rd_addr_in), 
		.retire_en_instr2_rd_in(retire_en_instr2_rd_in), 
		.retired_instr2_rn_addr_in(retired_instr2_rn_addr_in), 
		.retire_en_instr2_rn_in(retire_en_instr2_rn_in), 
		.reg_addr_issued_to_alu_pipe1_rd_out(reg_addr_issued_to_alu_pipe1_rd_out), 
		.reg_addr_issued_to_alu_pipe1_rn_out(reg_addr_issued_to_alu_pipe1_rn_out), 
		.reg_addr_issued_to_alu_pipe2_rd_out(reg_addr_issued_to_alu_pipe2_rd_out), 
		.reg_addr_issued_to_alu_pipe2_rn_out(reg_addr_issued_to_alu_pipe2_rn_out)
	);

	initial begin
		// Initialize Inputs
		clk_in = 0;
		reset_in = 1;
		reg_issue_alu_pipe1_rd_in = 0;
		reg_issue_alu_pipe1_rn_in = 0;
		reg_issue_alu_pipe2_rd_in = 0;
		reg_issue_alu_pipe2_rn_in = 0;
		retired_instr1_rd_addr_in = 0;
		retire_en_instr1_rd_in = 0;
		retired_instr1_rn_addr_in = 0;
		retire_en_instr1_rn_in = 0;
		retired_instr2_rd_addr_in = 0;
		retire_en_instr2_rd_in = 0;
		retired_instr2_rn_addr_in = 0;
		retire_en_instr2_rn_in = 0;

		// Wait 100 ns for global reset to finish
		#105;
		reset_in <= 0;
		
		#10
		reg_issue_alu_pipe1_rd_in <= 1;
		reg_issue_alu_pipe1_rn_in <= 0;
		reg_issue_alu_pipe2_rd_in <= 1;
		reg_issue_alu_pipe2_rn_in <= 0;
		retired_instr1_rd_addr_in <= 0;
		retire_en_instr1_rd_in <= 0;
		retired_instr1_rn_addr_in <= 0;
		retire_en_instr1_rn_in <= 0;
		retired_instr2_rd_addr_in <= 0;
		retire_en_instr2_rd_in <= 0;
		retired_instr2_rn_addr_in <= 0;
		retire_en_instr2_rn_in <= 0;
		
		#10
		reg_issue_alu_pipe1_rd_in <= 1;
		reg_issue_alu_pipe1_rn_in <= 0;
		reg_issue_alu_pipe2_rd_in <= 0;
		reg_issue_alu_pipe2_rn_in <= 0;
		retired_instr1_rd_addr_in <= 0;
		retire_en_instr1_rd_in <= 0;
		retired_instr1_rn_addr_in <= 0;
		retire_en_instr1_rn_in <= 0;
		retired_instr2_rd_addr_in <= 0;
		retire_en_instr2_rd_in <= 0;
		retired_instr2_rn_addr_in <= 0;
		retire_en_instr2_rn_in <= 0;
		
		#10
		reg_issue_alu_pipe1_rd_in <= 1;
		reg_issue_alu_pipe1_rn_in <= 0;
		reg_issue_alu_pipe2_rd_in <= 0;
		reg_issue_alu_pipe2_rn_in <= 1;
		retired_instr1_rd_addr_in <= 0;
		retire_en_instr1_rd_in <= 0;
		retired_instr1_rn_addr_in <= 0;
		retire_en_instr1_rn_in <= 0;
		retired_instr2_rd_addr_in <= 0;
		retire_en_instr2_rd_in <= 0;
		retired_instr2_rn_addr_in <= 0;
		retire_en_instr2_rn_in <= 0;
      
		#10
		reg_issue_alu_pipe1_rd_in <= 1;
		reg_issue_alu_pipe1_rn_in <= 0;
		reg_issue_alu_pipe2_rd_in <= 0;
		reg_issue_alu_pipe2_rn_in <= 0;
		retired_instr1_rd_addr_in <= 0;
		retire_en_instr1_rd_in <= 0;
		retired_instr1_rn_addr_in <= 0;
		retire_en_instr1_rn_in <= 0;
		retired_instr2_rd_addr_in <= 0;
		retire_en_instr2_rd_in <= 0;
		retired_instr2_rn_addr_in <= 0;
		retire_en_instr2_rn_in <= 0;
		
		#10
		reg_issue_alu_pipe1_rd_in <= 0;
		reg_issue_alu_pipe1_rn_in <= 0;
		reg_issue_alu_pipe2_rd_in <= 0;
		reg_issue_alu_pipe2_rn_in <= 0;
		retired_instr1_rd_addr_in <= 0;
		retire_en_instr1_rd_in <= 0;
		retired_instr1_rn_addr_in <= 0;
		retire_en_instr1_rn_in <= 0;
		retired_instr2_rd_addr_in <= 0;
		retire_en_instr2_rd_in <= 0;
		retired_instr2_rn_addr_in <= 0;
		retire_en_instr2_rn_in <= 0;
		// Add stimulus here

	end
    

	always #5 clk_in = ~clk_in;	
		
endmodule

