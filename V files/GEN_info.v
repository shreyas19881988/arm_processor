`define TAG_SIZE 5

/******Pipes arrangement******/
`define NO_OF_PIPES 4

`define ALU_PIPE1 3

`define ALU_PIPE2 2

`define LS_PIPE3 1

`define BRANCH_PIPE4 0
/******Pipes arrangement******/


/******No of registers and CPSR******/
`define NO_OF_REG_AND_CPSR 17

`define INCLUDING_CPSR_R15 16

`define INCLUDING_CPSR_R14 15

`define INCLUDING_CPSR_R13 14

`define INCLUDING_CPSR_R12 13

`define INCLUDING_CPSR_R11 12

`define INCLUDING_CPSR_R10 11

`define INCLUDING_CPSR_R9 10

`define INCLUDING_CPSR_R8 9

`define INCLUDING_CPSR_R7 8

`define INCLUDING_CPSR_R6 7

`define INCLUDING_CPSR_R5 6

`define INCLUDING_CPSR_R4 5

`define INCLUDING_CPSR_R3 4

`define INCLUDING_CPSR_R2 3

`define INCLUDING_CPSR_R1 2

`define INCLUDING_CPSR_R0 1

`define INCLUDING_CPSR_CPSR 0
/******No of registers and CPSR******/


/******No of registers******/
`define NO_OF_REG 16

`define R15 15

`define R14 14

`define R13 13

`define R12 12

`define R11 11

`define R10 10

`define R9 9

`define R8 8

`define R7 7

`define R6 6

`define R5 5

`define R4 4

`define R3 3

`define R2 2

`define R1 1

`define R0 0
/******No of registers******/

`define ENCODED_NO_OF_PIPES 3

`define ENCODED_NO_OF_PIPES_MUX_SEL 2

/******TAG FOR ALL REG******/
`define TAG_ALL_REG 80

`define TAG_FOR_R15_START 79

`define TAG_FOR_R15_END 75

`define TAG_FOR_R14_START 74

`define TAG_FOR_R14_END 70

`define TAG_FOR_R13_START 69

`define TAG_FOR_R13_END 65

`define TAG_FOR_R12_START 64

`define TAG_FOR_R12_END 60

`define TAG_FOR_R11_START 59

`define TAG_FOR_R11_END 55

`define TAG_FOR_R10_START 54

`define TAG_FOR_R10_END 50

`define TAG_FOR_R9_START 49

`define TAG_FOR_R9_END 45

`define TAG_FOR_R8_START 44

`define TAG_FOR_R8_END 40

`define TAG_FOR_R7_START 39

`define TAG_FOR_R7_END 35

`define TAG_FOR_R6_START 34

`define TAG_FOR_R6_END 30

`define TAG_FOR_R5_START 29

`define TAG_FOR_R5_END 25

`define TAG_FOR_R4_START 24

`define TAG_FOR_R4_END 20

`define TAG_FOR_R3_START 19

`define TAG_FOR_R3_END 15

`define TAG_FOR_R2_START 14

`define TAG_FOR_R2_END 10

`define TAG_FOR_R1_START 9

`define TAG_FOR_R1_END 5

`define TAG_FOR_R0_START 4

`define TAG_FOR_R0_END 0
/******TAG FOR ALL REG******/

`define LOG_ADDRESS_SIZE 4

`define ENCODED_RB_ADDR 5


/******3 TAG WORD******/
`define THREE_INSTR_TAG_WORD 15

`define TAG_PIPE1_STARTS 14

`define TAG_PIPE1_ENDS 10

`define TAG_PIPE2_STARTS 9

`define TAG_PIPE2_ENDS 5

`define TAG_PIPE3_STARTS 4

`define TAG_PIPE3_ENDS 0
/******3 TAG WORD******/

/******Load store multiple word******/
`define LDM_WORD 16

`define LDM_REG_R15 15

`define LDM_REG_R14 14

`define LDM_REG_R13 13

`define LDM_REG_R12 12

`define LDM_REG_R11 11

`define LDM_REG_R10 10

`define LDM_REG_R9 9

`define LDM_REG_R8 8

`define LDM_REG_R7 7

`define LDM_REG_R6 6

`define LDM_REG_R5 5

`define LDM_REG_R4 4

`define LDM_REG_R3 3

`define LDM_REG_R2 2

`define LDM_REG_R1 1

`define LDM_REG_R0 0
/******Load store multiple word******/


`define decoded_output_16_1 16

`define decoder_input_64 6

`define decoder_output_64 64

`define encoder_input_64 64

`define encoder_output_64 6



/******REGISTER_FILE******/
`define ALU_PIPES_REG_FILE_SIZE 64

`define DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE 6

`define LS_PIPE_REG_FILE_SIZE 32

`define BRANCH_PIPE_FILE_SIZE 32
/******REGISTER_FILE******/