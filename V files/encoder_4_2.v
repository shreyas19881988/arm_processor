`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module encoder_4_2(
							input [`NO_OF_PIPES-1:0] decoded_addr_in,
							output reg [`ENCODED_NO_OF_PIPES-1:0] addr_out
    );

always@(*)
begin
	case(decoded_addr_in)
		`NO_OF_PIPES'b1000 : 
		begin
			addr_out = `ENCODED_NO_OF_PIPES'b000;
		end
		`NO_OF_PIPES'b0100 : 
		begin
			addr_out = `ENCODED_NO_OF_PIPES'b001;
		end
		`NO_OF_PIPES'b0010 : 
		begin
			addr_out = `ENCODED_NO_OF_PIPES'b010;
		end
		`NO_OF_PIPES'b0001 : 
		begin
			addr_out = `ENCODED_NO_OF_PIPES'b011;
		end
		default : 
		begin
			addr_out = `ENCODED_NO_OF_PIPES'b000;
		end
	endcase
end
endmodule
