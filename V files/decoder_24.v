`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module decoder_24(
						input [`TAG_SIZE-1:0] tag_in,
						output reg [`RB_SIZE-1:0] decoded_addr_out
    );

always@(*)
begin
	case(tag_in)
		`TAG_SIZE'b00000 : 
		begin
			decoded_addr_out = 8388608;
		end
		`TAG_SIZE'b00001 : 
		begin
			decoded_addr_out = 4194304;
		end
		`TAG_SIZE'b00010 : 
		begin
			decoded_addr_out = 2097152;
		end
		`TAG_SIZE'b00011 : 
		begin
			decoded_addr_out = 1048576;
		end
		`TAG_SIZE'b00100 : 
		begin
			decoded_addr_out = 524288;
		end
		`TAG_SIZE'b00101 : 
		begin
			decoded_addr_out = 262144;
		end
		`TAG_SIZE'b00110 : 
		begin
			decoded_addr_out = 131072;
		end
		`TAG_SIZE'b00111 : 
		begin
			decoded_addr_out = 65536;
		end
		`TAG_SIZE'b01000 : 
		begin
			decoded_addr_out = 32768;
		end
		`TAG_SIZE'b01001 : 
		begin
			decoded_addr_out = 16384;
		end
		`TAG_SIZE'b01010 : 
		begin
			decoded_addr_out = 8192;
		end
		`TAG_SIZE'b01011 : 
		begin
			decoded_addr_out = 4096;
		end
		`TAG_SIZE'b01100 : 
		begin
			decoded_addr_out = 2048;
		end
		`TAG_SIZE'b01101 : 
		begin
			decoded_addr_out = 1024;
		end
		`TAG_SIZE'b01110 : 
		begin
			decoded_addr_out = 512;
		end
		`TAG_SIZE'b01111 : 
		begin
			decoded_addr_out = 256;
		end
		`TAG_SIZE'b10000 : 
		begin
			decoded_addr_out = 128;
		end
		`TAG_SIZE'b10001 : 
		begin
			decoded_addr_out = 64;
		end
		`TAG_SIZE'b10010 : 
		begin
			decoded_addr_out = 32;
		end
		`TAG_SIZE'b10011 : 
		begin
			decoded_addr_out = 16;
		end
		`TAG_SIZE'b10100 : 
		begin
			decoded_addr_out = 8;
		end
		`TAG_SIZE'b10101 : 
		begin
			decoded_addr_out = 4;
		end
		`TAG_SIZE'b10110 : 
		begin
			decoded_addr_out = 2;
		end
		`TAG_SIZE'b10111 : 
		begin
			decoded_addr_out = 1;
		end
		default : 
		begin
			decoded_addr_out = 0;
		end
	endcase
end
endmodule
