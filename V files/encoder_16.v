`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module encoder_16( 
						input [`NO_OF_REG-1:0] decoded_input_in,
						output reg [`LOG_ADDRESS_SIZE-1:0] encoded_result_out
     );

always@(*)
begin
	case(decoded_input_in)
		`NO_OF_REG'b1000_0000_0000_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b0000;
		end
		`NO_OF_REG'b0100_0000_0000_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b0001;
		end
		`NO_OF_REG'b0010_0000_0000_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b0010;
		end
		`NO_OF_REG'b0001_0000_0000_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b0011;
		end
		`NO_OF_REG'b0000_1000_0000_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b0100;
		end
		`NO_OF_REG'b0000_0100_0000_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b0101;
		end
		`NO_OF_REG'b0000_0010_0000_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b0110;
		end
		`NO_OF_REG'b0000_0001_0000_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b0111;
		end
		`NO_OF_REG'b0000_0000_1000_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b1000;;
		end
		`NO_OF_REG'b0000_0000_0100_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b1001;
		end
		`NO_OF_REG'b0000_0000_0010_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b1010;
		end
		`NO_OF_REG'b0000_0000_0001_0000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b1011;
		end
		`NO_OF_REG'b0000_0000_0000_1000 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b1100;
		end
		`NO_OF_REG'b0000_0000_0000_0100 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b1101;
		end
		`NO_OF_REG'b0000_0000_0000_0010 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b1110;
		end
		`NO_OF_REG'b0000_0000_0000_0001 :	
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b1111;
		end
		default : 
		begin
			encoded_result_out = `LOG_ADDRESS_SIZE'b0000;
		end
	endcase
end
endmodule
