`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module tb_ROB;

	// Inputs
	reg clk_in;
	reg reset_in;
	reg new_entry_enable_in;
	reg retire_1_instr_in;
	reg retire_2_instr_in;
	reg [`RB_ENTRY_WORD-1:0] RB_entry1_info_in;
	reg [`RB_ENTRY_WORD-1:0] RB_entry2_info_in;
	reg [`PIPES_INSTR_TAG_WORD-1:0] tag_instr_pipes_in;
	reg [`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE-1:0] mod_reg_addr_rd_in;
	reg [`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE-1:0] mod_reg_addr_rn_in;
	reg [`MOD_CPSR_ADDR_4_PIPES_COMBINED_SIZE-1:0] mod_reg_addr_cpsr_in;
	reg [`NO_OF_PIPES-1:0] instr_complete_in;
	reg [`NO_OF_PIPES-1:0] speculative_result_in;
	reg ldm_stm_stall_in;
	reg [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_alu_pipe1_rn_log_address_in;
	reg [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_alu_pipe1_rn_in;
	wire [`RD_RN_CPSR_EN-1:0] tag_retire_en_alu_pipe1_out;
	wire [`RD_RN_CPSR_EN-1:0] tag_retire_en_alu_pipe2_out;
	wire [`RD_RN_CPSR_EN-1:0] tag_retire_en_ls_pipe3_out;
	wire [`RD_RN_CPSR_EN-1:0] tag_retire_en_branch_pipe4_out;
	wire [`RD_RN_CPSR_EN-1:0] tag_change_en_alu_pipe1_out;
	wire [`RD_RN_CPSR_EN-1:0] tag_change_en_alu_pipe2_out;
	wire [`RD_RN_CPSR_EN-1:0] tag_change_en_ls_pipe3_out;
	wire [`RD_RN_CPSR_EN-1:0] tag_change_en_branch_pipe4_out;
	wire [`TAG_TO_CHANGE_RD_RN_CPSR-1:0] tag_to_change_alu_pipe1_out;
	wire [`TAG_TO_CHANGE_RD_RN_CPSR-1:0] tag_to_change_alu_pipe2_out;
	wire [`TAG_TO_CHANGE_RD_RN_CPSR-1:0] tag_to_change_ls_pipe3_out;
	wire [`TAG_TO_CHANGE_RD_RN_CPSR-1:0] tag_to_change_branch_pipe4_out;
	wire [`NO_OF_REG-1:0] tag_change_en_in_case_of_ldm_ls_pipe3_out;
	wire [`NO_OF_REG-1:0] tag_retire_en_in_case_of_ldm_ls_pipe3_out;
	wire [`TAG_ALL_REG-1:0] tag_to_change_in_case_of_ldm_ls_pipe3_out;
	wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_alu_pipe1_rn_out;
	

	// Instantiate the Unit Under Test (UUT)
	ROB uut (
		.clk_in(clk_in), 
		.reset_in(reset_in), 
		.new_entry_enable_in(new_entry_enable_in), 
		//.retire_1_instr_in(retire_1_instr_in), 
		//.retire_2_instr_in(retire_2_instr_in), 
		.RB_entry1_info_in(RB_entry1_info_in), 
		.RB_entry2_info_in(RB_entry2_info_in),
		.tag_instr_pipes_in(tag_instr_pipes_in),
		.mod_reg_addr_rd_in(mod_reg_addr_rd_in),
		.mod_reg_addr_rn_in(mod_reg_addr_rn_in),
		.mod_reg_addr_cpsr_in(mod_reg_addr_cpsr_in),
		.instr_complete_in(instr_complete_in),
		.speculative_result_in(speculative_result_in),
		.ldm_stm_stall_in(ldm_stm_stall_in),
		.tag_to_issue_plus_alu_pipe1_rn_log_address_in(tag_to_issue_plus_alu_pipe1_rn_log_address_in),
		.data_frm_RAT_alu_pipe1_rn_in(data_frm_RAT_alu_pipe1_rn_in),
		.tag_retire_en_alu_pipe1_out(tag_retire_en_alu_pipe1_out),
		.tag_retire_en_alu_pipe2_out(tag_retire_en_alu_pipe2_out),
		.tag_retire_en_ls_pipe3_out(tag_retire_en_ls_pipe3_out),
		.tag_retire_en_branch_pipe4_out(tag_retire_en_branch_pipe4_out),
		.tag_change_en_alu_pipe1_out(tag_change_en_alu_pipe1_out),
		.tag_change_en_alu_pipe2_out(tag_change_en_alu_pipe2_out),
		.tag_change_en_ls_pipe3_out(tag_change_en_ls_pipe3_out),
		.tag_change_en_branch_pipe4_out(tag_change_en_branch_pipe4_out),
		.tag_to_change_alu_pipe1_out(tag_to_change_alu_pipe1_out),
		.tag_to_change_alu_pipe2_out(tag_to_change_alu_pipe2_out),
		.tag_to_change_ls_pipe3_out(tag_to_change_ls_pipe3_out),
		.tag_to_change_branch_pipe4_out(tag_to_change_branch_pipe4_out),
		.tag_change_en_in_case_of_ldm_ls_pipe3_out(tag_change_en_in_case_of_ldm_ls_pipe3_out),
		.tag_to_change_in_case_of_ldm_ls_pipe3_out(tag_to_change_in_case_of_ldm_ls_pipe3_out),
		.tag_retire_en_in_case_of_ldm_ls_pipe3_out(tag_retire_en_in_case_of_ldm_ls_pipe3_out),
		.reg_data_to_issue_alu_pipe1_rn_out(reg_data_to_issue_alu_pipe1_rn_out)
		
	);

	initial begin
		// Initialize Inputs
		clk_in = 0;
		reset_in = 1;
		new_entry_enable_in = 0;
		retire_1_instr_in = 0;
		retire_2_instr_in = 0;
		RB_entry1_info_in = 0;
		RB_entry2_info_in = 0;
		tag_instr_pipes_in = 0;
		mod_reg_addr_rd_in = 0;
		mod_reg_addr_rn_in = 0;
		mod_reg_addr_cpsr_in = 0;
		instr_complete_in = 0;
		ldm_stm_stall_in = 0;
		speculative_result_in = 0;
		tag_to_issue_plus_alu_pipe1_rn_log_address_in = 0;
		data_frm_RAT_alu_pipe1_rn_in = 0;

		// Wait 100 ns for global reset to finish
		#105;
      reset_in <= 0;
		#10
		new_entry_enable_in <= 1;
		RB_entry1_info_in <= `RB_ENTRY_WORD'b00000_0_0_1_0_0_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		RB_entry2_info_in <= `RB_ENTRY_WORD'b00001_1_0_1_1_0_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		#10
		new_entry_enable_in <= 1;
		RB_entry1_info_in <= `RB_ENTRY_WORD'b00010_0_0_1_1_1_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		RB_entry2_info_in <= `RB_ENTRY_WORD'b00011_1_0_1_0_0_0_0_1_0_0_0100000011000100/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		#10
		new_entry_enable_in <= 1;
		RB_entry1_info_in <= `RB_ENTRY_WORD'b00100_0_0_1_0_0_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		RB_entry2_info_in <= `RB_ENTRY_WORD'b00101_1_0_1_1_0_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		#10
		new_entry_enable_in <= 1;
		RB_entry1_info_in <= `RB_ENTRY_WORD'b00110_0_0_1_1_1_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		RB_entry2_info_in <= `RB_ENTRY_WORD'b00111_1_0_1_0_0_0_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		#10
		new_entry_enable_in <= 1;
		RB_entry1_info_in <= `RB_ENTRY_WORD'b01000_0_0_1_0_0_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		RB_entry2_info_in <= `RB_ENTRY_WORD'b01001_1_0_1_1_0_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		#10
		new_entry_enable_in <= 1;
		RB_entry1_info_in <= `RB_ENTRY_WORD'b01010_0_0_1_1_1_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		RB_entry2_info_in <= `RB_ENTRY_WORD'b01011_1_0_1_0_0_0_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		#10
		new_entry_enable_in <= 1;
		RB_entry1_info_in <= `RB_ENTRY_WORD'b01100_0_0_1_0_0_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		RB_entry2_info_in <= `RB_ENTRY_WORD'b01101_1_0_1_1_0_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		#10
		new_entry_enable_in <= 1;
		RB_entry1_info_in <= `RB_ENTRY_WORD'b01110_0_0_1_1_1_1_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		RB_entry2_info_in <= `RB_ENTRY_WORD'b01111_1_0_1_0_0_0_0_0_0_0_0000000000000000/*Tag_Speculstive_Overflow control_
		Rd update_Rn update_CPSR Update_ALU instr_Load store instr_Load Multiple_Store Multiple_
		Branch instr*/;
		#10
		new_entry_enable_in <= 0;
		tag_instr_pipes_in <= `PIPES_INSTR_TAG_WORD'b00000_00001_00010_00100;
		mod_reg_addr_rd_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0000_1000001_0001_0000001_0010_0000010_0011_0000011;
		
		mod_reg_addr_rn_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0100_0000101_0101_0000110_0110_0000111_0111_0001000;
		
		mod_reg_addr_cpsr_in <= 
		`MOD_CPSR_ADDR_4_PIPES_COMBINED_SIZE'b0000101_0000110_0000111_0001000;
		
		instr_complete_in <= 4'b1111;
		speculative_result_in <= 4'b0100;
		#10
		new_entry_enable_in <= 0;
		tag_instr_pipes_in <= `PIPES_INSTR_TAG_WORD'b00101_00110_00011_00111;
		ldm_stm_stall_in <= 1'b1;
		mod_reg_addr_rd_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0000_1000001_0001_0000001_0010_0000010_0011_0000011;
		mod_reg_addr_rn_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0100_0000101_0101_0000110_0110_0000111_0111_0001000;
		mod_reg_addr_cpsr_in <= 
		`MOD_CPSR_ADDR_4_PIPES_COMBINED_SIZE'b0000101_0000110_0000111_0001000;
		instr_complete_in <= 4'b1101;
		speculative_result_in <= 4'b1010;
		#10
		new_entry_enable_in <= 0;
		tag_instr_pipes_in <= `PIPES_INSTR_TAG_WORD'b01000_01001_00011_01010;
		ldm_stm_stall_in <= 1'b1;
		mod_reg_addr_rd_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0001_1000011_0011_0000011_0110_0000110_0111_0000111;
		instr_complete_in <= 4'b1100;
		speculative_result_in <= 4'b0110;
		#10
		new_entry_enable_in <= 0;
		tag_instr_pipes_in <= `PIPES_INSTR_TAG_WORD'b01011_01100_00011_01101;
		ldm_stm_stall_in <= 1'b1;
		mod_reg_addr_rd_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0001_1000011_0011_0000011_0111_0000110_0111_0000111;
		instr_complete_in <= 4'b1001;
		speculative_result_in <= 4'b1011;
		#10
		new_entry_enable_in <= 0;
		tag_instr_pipes_in <= `PIPES_INSTR_TAG_WORD'b01110_01111_00011_10000;
		ldm_stm_stall_in <= 1'b1;
		mod_reg_addr_rd_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0011_1000111_0111_0000111_1110_0001110_1111_0001111;
		instr_complete_in <= 4'b1110;
		speculative_result_in <= 4'b0110;
		#10
		ldm_stm_stall_in <= 1'b0;
		instr_complete_in <= 4'b0000;
		speculative_result_in <= 4'b0000;
		tag_to_issue_plus_alu_pipe1_rn_log_address_in <= `TAG_TO_ISSUE_PLUS_LOG_REG'b00011_0110;
		#10
		data_frm_RAT_alu_pipe1_rn_in <= 12;
		tag_to_issue_plus_alu_pipe1_rn_log_address_in <= `TAG_TO_ISSUE_PLUS_LOG_REG'b00010_0010;
		instr_complete_in <= 4'b0000;
		speculative_result_in <= 4'b0000;
		#10
		tag_to_issue_plus_alu_pipe1_rn_log_address_in <= `TAG_TO_ISSUE_PLUS_LOG_REG'b00010_0110;
		#10
		tag_to_issue_plus_alu_pipe1_rn_log_address_in <= `TAG_TO_ISSUE_PLUS_LOG_REG'b11010_0110
		/*#10
		retire_2_instr_in <= 0;
		new_entry_enable_in <= 1;
		RB_entry1_info_in <= `RB_ENTRY_WORD'b00110_0_0_1_0_0_1_0_0_0_0/*Tag_Rd address_Rn address
		_Speculstive_Overflow control_Rd update_Rn update_CPSR Update_ALU instr_Load store instr_
		Branch instr*/;
		/*RB_entry2_info_in <= `RB_ENTRY_WORD'b00111_0_0_1_1_0_1_0_0_0_0/*Tag_Rd address_Rn address
		_Speculstive_Overflow control_Rd update_Rn update_CPSR Update_ALU instr_Load store instr_
		Branch instr*/;
		/*tag_instr_pipes_in <= `PIPES_INSTR_TAG_WORD'b00000_00101_00010_00011/*alu pipe1_alu pipe2
		_load store pipe3_branch pipe4*/;
		/*mod_reg_addr_rd_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0000_1000001_0001_0000001_0010_0000010_0011_0000011;
		/*alu pipe1_log_addr_alu pipe1 mod addr_alu pipe2 log addr_alu pipe2 mod addr_
		load store pipe3 log addr_load store pipe3 mod addr_branch pipe4 log addr_branch pipe4 mod addr*/
		/*instr_complete_in <= 4'b0000;
		ldm_stm_stall_in <= 1'b1;
		#10
		retire_2_instr_in <= 0;
		new_entry_enable_in <= 0;
		tag_instr_pipes_in <= `PIPES_INSTR_TAG_WORD'b00001_00101_00011_00111/*alu pipe1_alu pipe2
		_load store pipe3_branch pipe4*/;
		/*mod_reg_addr_rd_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0000_1000001_0010_0000010_0010_0000010_0011_0000011;
		/*alu pipe1_log_addr_alu pipe1 mod addr_alu pipe2 log addr_alu pipe2 mod addr_
		load store pipe3 log addr_load store pipe3 mod addr_branch pipe4 log addr_branch pipe4 mod addr*/
		/*instr_complete_in <= 4'b0000;
		ldm_stm_stall_in <= 1'b1;
		#10
		tag_instr_pipes_in <= `PIPES_INSTR_TAG_WORD'b00011_00101_00111_01111/*alu pipe1_alu pipe2
		_load store pipe3_branch pipe4*/;
		/*mod_reg_addr_rd_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0000_1000001_0011_0000011_0010_0000010_0011_0000011;
		/*alu pipe1_log_addr_alu pipe1 mod addr_alu pipe2 log addr_alu pipe2 mod addr_
		load store pipe3 log addr_load store pipe3 mod addr_branch pipe4 log addr_branch pipe4 mod addr*/
		/*instr_complete_in <= 4'b0000;
		ldm_stm_stall_in <= 1'b1;
		#10
		tag_instr_pipes_in <= `PIPES_INSTR_TAG_WORD'b00011_00101_00111_01111/*alu pipe1_alu pipe2
		_load store pipe3_branch pipe4*/;
		/*mod_reg_addr_rd_in <= 
		`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE'b0000_1000001_0011_0000011_0010_0000010_0011_0000011;
		/*alu pipe1_log_addr_alu pipe1 mod addr_alu pipe2 log addr_alu pipe2 mod addr_
		load store pipe3 log addr_load store pipe3 mod addr_branch pipe4 log addr_branch pipe4 mod addr*/
		/*instr_complete_in <= 4'b0000;
		ldm_stm_stall_in <= 1'b0;
		
		/*#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		RB_entry1_info_in <= 30;
		RB_entry2_info_in <= 300;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 1;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 40;
		RB_entry2_info_in <= 400;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		RB_entry1_info_in <= 50;
		RB_entry2_info_in <= 500;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 60;
		RB_entry2_info_in <= 600;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 70;
		RB_entry2_info_in <= 700;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 80;
		RB_entry2_info_in <= 800;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 90;
		RB_entry2_info_in <= 900;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 100;
		RB_entry2_info_in <= 1000;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 110;
		RB_entry2_info_in <= 1100;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 120;
		RB_entry2_info_in <= 1200;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 130;
		RB_entry2_info_in <= 1300;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 140;
		RB_entry2_info_in <= 1400;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		RB_entry1_info_in <= 150;
		RB_entry2_info_in <= 1500;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;*/
        
		// Add stimulus here

	end
      
	always #5 clk_in = ~clk_in;
	
endmodule

