`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module priority_encoder_64( input [`encoder_input_64-1:0] decoded_input_in,
									 output [`encoder_output_64-1:0] priority_encoded_out
    );


assign priority_encoded_out = (decoded_input_in[63] ? `encoder_input_64'b000000 : (decoded_input_in[62]

? `encoder_input_64'b000001 : (decoded_input_in[61] ? `encoder_input_64'b000010 : (decoded_input_in[60]

? `encoder_input_64'b000011 : (decoded_input_in[59] ? `encoder_input_64'b000100 : (decoded_input_in[58]

? `encoder_input_64'b000101 : (decoded_input_in[57] ? `encoder_input_64'b000110 : (decoded_input_in[56]

? `encoder_input_64'b000111 : (decoded_input_in[55] ? `encoder_input_64'b001000 : (decoded_input_in[54]

? `encoder_input_64'b001001 : (decoded_input_in[53] ? `encoder_input_64'b001010 : (decoded_input_in[52]

? `encoder_input_64'b001011 : (decoded_input_in[51] ? `encoder_input_64'b001100 : (decoded_input_in[50]

? `encoder_input_64'b001101 : (decoded_input_in[49] ? `encoder_input_64'b001110 : (decoded_input_in[48]

? `encoder_input_64'b001111 : (decoded_input_in[47] ? `encoder_input_64'b010000 : (decoded_input_in[46]

? `encoder_input_64'b010001 : (decoded_input_in[45] ? `encoder_input_64'b010010 : (decoded_input_in[44]

? `encoder_input_64'b010011 : (decoded_input_in[43] ? `encoder_input_64'b010100 : (decoded_input_in[42]

? `encoder_input_64'b010101 : (decoded_input_in[41] ? `encoder_input_64'b010110 : (decoded_input_in[40]

? `encoder_input_64'b010111 : (decoded_input_in[39] ? `encoder_input_64'b011000 : (decoded_input_in[38]

? `encoder_input_64'b011001 : (decoded_input_in[37] ? `encoder_input_64'b011010 : (decoded_input_in[36]

? `encoder_input_64'b011011 : (decoded_input_in[35] ? `encoder_input_64'b011100 : (decoded_input_in[34]

? `encoder_input_64'b011101 : (decoded_input_in[33] ? `encoder_input_64'b011110 : (decoded_input_in[32]

? `encoder_input_64'b011111 : (decoded_input_in[31] ? `encoder_input_64'b100000 : (decoded_input_in[30]

? `encoder_input_64'b100001 : (decoded_input_in[29] ? `encoder_input_64'b100010 : (decoded_input_in[28]

? `encoder_input_64'b100011 : (decoded_input_in[27] ? `encoder_input_64'b100100 : (decoded_input_in[26]

? `encoder_input_64'b100101 : (decoded_input_in[25] ? `encoder_input_64'b100110 : (decoded_input_in[24]

? `encoder_input_64'b100111 : (decoded_input_in[23] ? `encoder_input_64'b101000 : (decoded_input_in[22]

? `encoder_input_64'b101001 : (decoded_input_in[21] ? `encoder_input_64'b101010 : (decoded_input_in[20]

? `encoder_input_64'b101011 : (decoded_input_in[19] ? `encoder_input_64'b101100 : (decoded_input_in[18]

? `encoder_input_64'b101101 : (decoded_input_in[17] ? `encoder_input_64'b101110 : (decoded_input_in[16]

? `encoder_input_64'b101111 : (decoded_input_in[15] ? `encoder_input_64'b110000 : (decoded_input_in[14]

? `encoder_input_64'b110001 : (decoded_input_in[13] ? `encoder_input_64'b110010 : (decoded_input_in[12]

? `encoder_input_64'b110011 : (decoded_input_in[11] ? `encoder_input_64'b110100 : (decoded_input_in[10]

? `encoder_input_64'b110101 : (decoded_input_in[9] ? `encoder_input_64'b110110 : (decoded_input_in[8]

? `encoder_input_64'b110111 : (decoded_input_in[7] ? `encoder_input_64'b111000 : (decoded_input_in[6]

? `encoder_input_64'b111001 : (decoded_input_in[5] ? `encoder_input_64'b111010 : (decoded_input_in[4]

? `encoder_input_64'b111011 : (decoded_input_in[3] ? `encoder_input_64'b111100 : (decoded_input_in[2]

? `encoder_input_64'b111101 : (decoded_input_in[1] ? `encoder_input_64'b111110 : (decoded_input_in[0]

? `encoder_input_64'b111111 : 0))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));

endmodule
