`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"


module retire_controller_sub_module(
								input clk_in,
								input reset_in,
								input [`RB_SIZE-1:0] head_pointer_instr1_in,
								input [`RB_SIZE-1:0] head_pointer_instr2_in,
								input [`RB_SIZE-1:0] valid_retire_instr_in,
								input [`RB_SIZE-1:0] load_multiple_instr_RB_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_15_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_14_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_13_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_12_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_11_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_10_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_9_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_8_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_7_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_6_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_5_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_4_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_3_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_2_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_1_in,
								input [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg_0_in,
								output retire_one_instr_out,
								output retire_both_instr_out,
								output ldm_retire_complete_instr1_out,
								output ldm_retire_complete_instr2_out
								
    );


wire [`ENCODED_RB_ADDR-1:0] encoded_head_pointer1_position;
wire [`ENCODED_RB_ADDR-1:0] encoded_head_pointer2_position;
wire valid_retire_instr1;
wire valid_retire_instr2;
wire ldm_instr1_retire;
wire ldm_instr2_retire;
wire [`LOG_ADDRESS_SIZE-1:0] data_count_for_ldm_retire;
wire [`LOG_ADDRESS_SIZE-1:0] count_for_ldm_retire;
wire retire_begin_ldm_instr1;
wire retire_begin_ldm_instr2;
wire [`LOG_MOD_REG_ADDR_SIZE:0] value_to_retire_frm_ldm_instr;
wire ldm_retire_complete_instr1_out;
wire ldm_retire_complete_instr2_out;


encoder_24 encoding_head_pointer1 (
    .decoded_input_in(head_pointer_instr1_in), 
    .encoded_result_out(encoded_head_pointer1_position)
    );


encoder_24 encoding_head_pointer2 (
    .decoded_input_in(head_pointer_instr2_in), 
    .encoded_result_out(encoded_head_pointer2_position)
    );
	 

mux_24_1 #1 mux_for_selecting_valid_retire_for_instr1_retire (
    .y_out(valid_retire_instr1), 
    .i0_in(valid_retire_instr_in[23]), 
    .i1_in(valid_retire_instr_in[22]), 
    .i2_in(valid_retire_instr_in[21]), 
    .i3_in(valid_retire_instr_in[20]), 
    .i4_in(valid_retire_instr_in[19]), 
    .i5_in(valid_retire_instr_in[18]), 
    .i6_in(valid_retire_instr_in[17]), 
    .i7_in(valid_retire_instr_in[16]), 
    .i8_in(valid_retire_instr_in[15]), 
    .i9_in(valid_retire_instr_in[14]), 
    .i10_in(valid_retire_instr_in[13]), 
    .i11_in(valid_retire_instr_in[12]), 
    .i12_in(valid_retire_instr_in[11]), 
    .i13_in(valid_retire_instr_in[10]), 
    .i14_in(valid_retire_instr_in[9]), 
    .i15_in(valid_retire_instr_in[8]), 
    .i16_in(valid_retire_instr_in[7]), 
    .i17_in(valid_retire_instr_in[6]), 
    .i18_in(valid_retire_instr_in[5]), 
    .i19_in(valid_retire_instr_in[4]), 
    .i20_in(valid_retire_instr_in[3]), 
    .i21_in(valid_retire_instr_in[2]), 
    .i22_in(valid_retire_instr_in[1]), 
    .i23_in(valid_retire_instr_in[0]), 
    .sel_in(encoded_head_pointer1_position)
    );


mux_24_1 #1 mux_for_selecting_ldm_instr_for_instr1_retire (
    .y_out(ldm_instr1_retire), 
    .i0_in(load_multiple_instr_RB_in[23]), 
    .i1_in(load_multiple_instr_RB_in[22]), 
    .i2_in(load_multiple_instr_RB_in[21]), 
    .i3_in(load_multiple_instr_RB_in[20]), 
    .i4_in(load_multiple_instr_RB_in[19]), 
    .i5_in(load_multiple_instr_RB_in[18]), 
    .i6_in(load_multiple_instr_RB_in[17]), 
    .i7_in(load_multiple_instr_RB_in[16]), 
    .i8_in(load_multiple_instr_RB_in[15]), 
    .i9_in(load_multiple_instr_RB_in[14]), 
    .i10_in(load_multiple_instr_RB_in[13]), 
    .i11_in(load_multiple_instr_RB_in[12]), 
    .i12_in(load_multiple_instr_RB_in[11]), 
    .i13_in(load_multiple_instr_RB_in[10]), 
    .i14_in(load_multiple_instr_RB_in[9]), 
    .i15_in(load_multiple_instr_RB_in[8]), 
    .i16_in(load_multiple_instr_RB_in[7]), 
    .i17_in(load_multiple_instr_RB_in[6]), 
    .i18_in(load_multiple_instr_RB_in[5]), 
    .i19_in(load_multiple_instr_RB_in[4]), 
    .i20_in(load_multiple_instr_RB_in[3]), 
    .i21_in(load_multiple_instr_RB_in[2]), 
    .i22_in(load_multiple_instr_RB_in[1]), 
    .i23_in(load_multiple_instr_RB_in[0]), 
    .sel_in(encoded_head_pointer1_position)
    );


mux_24_1 #1 mux_for_selecting_valid_retire_for_instr2_retire (
    .y_out(valid_retire_instr2), 
    .i0_in(valid_retire_instr_in[23]), 
    .i1_in(valid_retire_instr_in[22]), 
    .i2_in(valid_retire_instr_in[21]), 
    .i3_in(valid_retire_instr_in[20]), 
    .i4_in(valid_retire_instr_in[19]), 
    .i5_in(valid_retire_instr_in[18]), 
    .i6_in(valid_retire_instr_in[17]), 
    .i7_in(valid_retire_instr_in[16]), 
    .i8_in(valid_retire_instr_in[15]), 
    .i9_in(valid_retire_instr_in[14]), 
    .i10_in(valid_retire_instr_in[13]), 
    .i11_in(valid_retire_instr_in[12]), 
    .i12_in(valid_retire_instr_in[11]), 
    .i13_in(valid_retire_instr_in[10]), 
    .i14_in(valid_retire_instr_in[9]), 
    .i15_in(valid_retire_instr_in[8]), 
    .i16_in(valid_retire_instr_in[7]), 
    .i17_in(valid_retire_instr_in[6]), 
    .i18_in(valid_retire_instr_in[5]), 
    .i19_in(valid_retire_instr_in[4]), 
    .i20_in(valid_retire_instr_in[3]), 
    .i21_in(valid_retire_instr_in[2]), 
    .i22_in(valid_retire_instr_in[1]), 
    .i23_in(valid_retire_instr_in[0]), 
    .sel_in(encoded_head_pointer2_position)
    );


mux_24_1 #1 mux_for_selecting_ldm_instr_for_instr2_retire (
    .y_out(ldm_instr2_retire), 
    .i0_in(load_multiple_instr_RB_in[23]), 
    .i1_in(load_multiple_instr_RB_in[22]), 
    .i2_in(load_multiple_instr_RB_in[21]), 
    .i3_in(load_multiple_instr_RB_in[20]), 
    .i4_in(load_multiple_instr_RB_in[19]), 
    .i5_in(load_multiple_instr_RB_in[18]), 
    .i6_in(load_multiple_instr_RB_in[17]), 
    .i7_in(load_multiple_instr_RB_in[16]), 
    .i8_in(load_multiple_instr_RB_in[15]), 
    .i9_in(load_multiple_instr_RB_in[14]), 
    .i10_in(load_multiple_instr_RB_in[13]), 
    .i11_in(load_multiple_instr_RB_in[12]), 
    .i12_in(load_multiple_instr_RB_in[11]), 
    .i13_in(load_multiple_instr_RB_in[10]), 
    .i14_in(load_multiple_instr_RB_in[9]), 
    .i15_in(load_multiple_instr_RB_in[8]), 
    .i16_in(load_multiple_instr_RB_in[7]), 
    .i17_in(load_multiple_instr_RB_in[6]), 
    .i18_in(load_multiple_instr_RB_in[5]), 
    .i19_in(load_multiple_instr_RB_in[4]), 
    .i20_in(load_multiple_instr_RB_in[3]), 
    .i21_in(load_multiple_instr_RB_in[2]), 
    .i22_in(load_multiple_instr_RB_in[1]), 
    .i23_in(load_multiple_instr_RB_in[0]), 
    .sel_in(encoded_head_pointer2_position)
    );


assign data_count_for_ldm_retire = count_for_ldm_retire + 4'b0001;


assign retire_begin_ldm_instr1 = valid_retire_instr1 & ldm_instr1_retire;


assign retire_begin_ldm_instr2 = valid_retire_instr2 & ldm_instr2_retire;


register_with_reset #`LOG_ADDRESS_SIZE counter_for_ldm_data (
		 .data_in(data_count_for_ldm_retire), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in | ldm_retire_complete_instr1_out | ldm_retire_complete_instr2_out), 
		 .en_in(retire_begin_ldm_instr1 | retire_begin_ldm_instr2), 
		 .data_out(count_for_ldm_retire)
		 );


mux_16_1 #(`LOG_MOD_REG_ADDR_SIZE+1) reg_value_frm_ldm_reg (
    .y_out(value_to_retire_frm_ldm_instr), 
    .i0_in(ldm_data_reg_15_in), 
    .i1_in(ldm_data_reg_14_in), 
    .i2_in(ldm_data_reg_13_in), 
    .i3_in(ldm_data_reg_12_in), 
    .i4_in(ldm_data_reg_11_in), 
    .i5_in(ldm_data_reg_10_in), 
    .i6_in(ldm_data_reg_9_in), 
    .i7_in(ldm_data_reg_8_in), 
    .i8_in(ldm_data_reg_7_in), 
    .i9_in(ldm_data_reg_6_in), 
    .i10_in(ldm_data_reg_5_in), 
    .i11_in(ldm_data_reg_4_in), 
    .i12_in(ldm_data_reg_3_in), 
    .i13_in(ldm_data_reg_2_in), 
    .i14_in(ldm_data_reg_1_in), 
    .i15_in(ldm_data_reg_0_in), 
    .sel_in(count_for_ldm_retire)
    );


assign ldm_retire_complete_instr1_out = retire_begin_ldm_instr1 & 

~value_to_retire_frm_ldm_instr[`LDM_DATA_EN];


assign ldm_retire_complete_instr2_out = retire_begin_ldm_instr2 & 

~value_to_retire_frm_ldm_instr[`LDM_DATA_EN];


retire_controller generating_retire_signals (
    .valid_retire_instr1_in(valid_retire_instr1), 
    .ldm_instr1_in(ldm_instr1_retire), 
    .valid_ldm_finished_instr1_in(ldm_retire_complete_instr1_out), 
    .valid_retire_instr2_in(valid_retire_instr2), 
    .ldm_instr2_in(ldm_instr2_retire), 
    .valid_ldm_finished_instr2_in(ldm_retire_complete_instr2_out), 
    .retire_one_instr_out(retire_one_instr_out), 
    .retire_both_instr_out(retire_both_instr_out)
    );


endmodule
