`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module encoder_24( 
						input [`RB_SIZE-1:0] decoded_input_in,
						output reg [`ENCODED_RB_ADDR-1:0] encoded_result_out
     );

always@(*)
begin
	case(decoded_input_in)
		`RB_SIZE'b1000_0000_0000_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b00000;
		end
		`RB_SIZE'b0100_0000_0000_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b00001;
		end
		`RB_SIZE'b0010_0000_0000_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b00010;
		end
		`RB_SIZE'b0001_0000_0000_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b00011;
		end
		`RB_SIZE'b0000_1000_0000_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b00100;
		end
		`RB_SIZE'b0000_0100_0000_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b00101;
		end
		`RB_SIZE'b0000_0010_0000_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b00110;
		end
		`RB_SIZE'b0000_0001_0000_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b00111;
		end
		`RB_SIZE'b0000_0000_1000_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b01000;
		end
		`RB_SIZE'b0000_0000_0100_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b01001;
		end
		`RB_SIZE'b0000_0000_0010_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b01010;
		end
		`RB_SIZE'b0000_0000_0001_0000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b01011;
		end
		`RB_SIZE'b0000_0000_0000_1000_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b01100;
		end
		`RB_SIZE'b0000_0000_0000_0100_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b01101;
		end
		`RB_SIZE'b0000_0000_0000_0010_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b01110;
		end
		`RB_SIZE'b0000_0000_0000_0001_0000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b01111;
		end
		`RB_SIZE'b0000_0000_0000_0000_1000_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b10000;
		end
		`RB_SIZE'b0000_0000_0000_0000_0100_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b10001;
		end
		`RB_SIZE'b0000_0000_0000_0000_0010_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b10010;
		end
		`RB_SIZE'b0000_0000_0000_0000_0001_0000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b10011;
		end
		`RB_SIZE'b0000_0000_0000_0000_0000_1000 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b10100;
		end
		`RB_SIZE'b0000_0000_0000_0000_0000_0100 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b10101;
		end
		`RB_SIZE'b0000_0000_0000_0000_0000_0010 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b10110;
		end
		`RB_SIZE'b0000_0000_0000_0000_0000_0001 :	
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b10111;
		end
		default : 
		begin
			encoded_result_out = `ENCODED_RB_ADDR'b00000;
		end
	endcase
end
endmodule
