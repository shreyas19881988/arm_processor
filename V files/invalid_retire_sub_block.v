`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module invalid_retire_sub_block(
								input [`RB_SIZE-1:0] valid_decoded_addr_in,
								input [`RB_SIZE-1:0] speculative_instr_RB_in,
								input speculative_result_pipe_in,
								input instr_complete_pipe_in,
								input [`RB_SIZE-1:0] load_multiple_instr_RB_in,
								input [`RB_SIZE-1:0] RD_update_instr_RB_in,
								input [`RB_SIZE-1:0] RN_update_instr_RB_in,
								input [`RB_SIZE-1:0] CPSR_update_instr_RB_in,
								input [`TAG_SIZE-1:0] tag_instr_in,
								input [`RB_SIZE-1:0] comparing_rd_pipe_in,
								input [`RB_SIZE-1:0] comparing_rn_pipe_in,
								input [`RB_SIZE-1:0] comparing_cpsr_pipe_in,
								input [`RB_SIZE-1:0] head_pointer1_in,
								input [`RB_SIZE-1:0] all_below_head_in,
								input [`RB_SIZE-1:0] all_above_tail_in,
								input [`RB_SIZE-1:0] valid_instr_in,
								input head_greater_than_tail_in,
								input [`THREE_INSTR_TAG_WORD-1:0] tag_in_other_pipes_in,
								input [`RB_SIZE-1:0] instr_other_pipe1_valid_retire_in,
								input [`RB_SIZE-1:0] instr_other_pipe2_valid_retire_in,
								input [`RB_SIZE-1:0] instr_other_pipe3_valid_retire_in,
								output [`RB_SIZE-1:0] invalid_instr_RB_out,
								output [`RB_SIZE-1:0] invalid_ldm_instr_RB_out,
								output rd_update_out,
								output rn_update_out,
								output cpsr_update_out,
								output [`TAG_TO_CHANGE_RD_RN_CPSR-1:0] new_tag_to_change_rd_rn_cpsr_out,
								output rd_invalid_but_retiring_in_other_pipe_out,
								output rn_invalid_but_retiring_in_other_pipe_out,
								output cpsr_invalid_but_retiring_in_other_pipe_out,
								output [`RD_RN_CPSR_EN-1:0] tag_change_en_pipe_out,
								output [`RB_SIZE-1:0] valid_instr_above_tag_out,
								output or_invalid_instr_above_tail_out,
								output or_valid_instr_above_tag_same_rd_pipe_out,
								output or_valid_instr_above_tag_same_rn_pipe_out,
								output or_valid_instr_above_tag_same_cpsr_pipe_out
    );


wire rd_update;
wire rn_update;
wire cpsr_update;
wire or_invalid_instr_RB;
wire [`RB_SIZE-1:0] comparing_rd_pipe;
wire [`RB_SIZE-1:0] comparing_rn_pipe;
wire [`RB_SIZE-1:0] comparing_cpsr_pipe;
wire [`RB_SIZE-1:0] all_above_tag_position;
wire [`RB_SIZE-1:0] head_greater_than_tag_position;
wire head_greater_than_tag_position_single;
wire [`RB_SIZE-1:0] valid_instr_above_tag_for_head_greater_than_tag;
wire [`RB_SIZE-1:0] valid_instr_above_tag_for_tag_greater_than_head;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_rd;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_rn;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_cpsr;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_rd;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_rn;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_cpsr;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_rd;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_rn;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_cpsr;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_rd;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_rn;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_cpsr;
wire [`RB_SIZE-1:0] instr_updating_rd_above_tail;
wire [`RB_SIZE-1:0] instr_updating_rn_above_tail;
wire [`RB_SIZE-1:0] instr_updating_cpsr_above_tail;
wire [`RB_SIZE-1:0] instr_updating_rd_below_head;
wire [`RB_SIZE-1:0] instr_updating_rn_below_head;
wire [`RB_SIZE-1:0] instr_updating_cpsr_below_head;
wire [`RB_SIZE-1:0] all_below_invalid_instr;
wire [`RB_SIZE-1:0] invalid_instr_above_tail;
wire [`NO_OF_REG-1:0] decoded_rd_addr;
wire [`NO_OF_REG-1:0] decoded_rn_addr;
wire [`NO_OF_REG-1:0] decoded_cpsr_addr;
wire [`TAG_SIZE-1:0] tag_rd;
wire [`TAG_SIZE-1:0] tag_rn;
wire [`TAG_SIZE-1:0] tag_cpsr;
wire [`NO_OF_REG_AND_CPSR-1:0] decoded_valid_rd_and_cpsr;
wire [`NO_OF_REG_AND_CPSR-1:0] decoded_valid_rn_and_cpsr;
wire [`NO_OF_REG_AND_CPSR-1:0] decoded_valid_cpsr;
wire [`RB_SIZE-1:0] valid_instr_above_tag_same_rd_pipe;
wire [`RB_SIZE-1:0] valid_instr_above_tag_same_rn_pipe;
wire [`RB_SIZE-1:0] valid_instr_above_tag_same_cpsr_pipe;



genvar i;
generate
for(i=`RB_SIZE-1;i>=0;i=i-1)
begin	:	grp_invalid_instr
	assign invalid_instr_RB_out[i] = valid_decoded_addr_in[i] & speculative_instr_RB_in[i] & 
	
	~speculative_result_pipe_in & instr_complete_pipe_in;
end
endgenerate


assign or_invalid_instr_RB = |(invalid_instr_RB_out);


assign invalid_ldm_instr_RB_out = invalid_instr_RB_out & load_multiple_instr_RB_in;
	
	
mux_24_1 #1 rd_update_invalid_addr (
    .y_out(rd_update), 
    .i0_in(RD_update_instr_RB_in[23]), 
    .i1_in(RD_update_instr_RB_in[22]), 
    .i2_in(RD_update_instr_RB_in[21]), 
    .i3_in(RD_update_instr_RB_in[20]), 
    .i4_in(RD_update_instr_RB_in[19]), 
    .i5_in(RD_update_instr_RB_in[18]), 
    .i6_in(RD_update_instr_RB_in[17]), 
    .i7_in(RD_update_instr_RB_in[16]), 
    .i8_in(RD_update_instr_RB_in[15]), 
    .i9_in(RD_update_instr_RB_in[14]), 
    .i10_in(RD_update_instr_RB_in[13]), 
    .i11_in(RD_update_instr_RB_in[12]), 
    .i12_in(RD_update_instr_RB_in[11]), 
    .i13_in(RD_update_instr_RB_in[10]), 
    .i14_in(RD_update_instr_RB_in[9]), 
    .i15_in(RD_update_instr_RB_in[8]), 
    .i16_in(RD_update_instr_RB_in[7]), 
    .i17_in(RD_update_instr_RB_in[6]), 
    .i18_in(RD_update_instr_RB_in[5]), 
    .i19_in(RD_update_instr_RB_in[4]), 
    .i20_in(RD_update_instr_RB_in[3]), 
    .i21_in(RD_update_instr_RB_in[2]), 
    .i22_in(RD_update_instr_RB_in[1]), 
    .i23_in(RD_update_instr_RB_in[0]), 
    .sel_in(tag_instr_in)
    );


assign rd_update_out = rd_update & or_invalid_instr_RB;


mux_24_1 #1 rn_update_invalid_addr (
    .y_out(rn_update), 
    .i0_in(RN_update_instr_RB_in[23]), 
    .i1_in(RN_update_instr_RB_in[22]), 
    .i2_in(RN_update_instr_RB_in[21]), 
    .i3_in(RN_update_instr_RB_in[20]), 
    .i4_in(RN_update_instr_RB_in[19]), 
    .i5_in(RN_update_instr_RB_in[18]), 
    .i6_in(RN_update_instr_RB_in[17]), 
    .i7_in(RN_update_instr_RB_in[16]), 
    .i8_in(RN_update_instr_RB_in[15]), 
    .i9_in(RN_update_instr_RB_in[14]), 
    .i10_in(RN_update_instr_RB_in[13]), 
    .i11_in(RN_update_instr_RB_in[12]), 
    .i12_in(RN_update_instr_RB_in[11]), 
    .i13_in(RN_update_instr_RB_in[10]), 
    .i14_in(RN_update_instr_RB_in[9]), 
    .i15_in(RN_update_instr_RB_in[8]), 
    .i16_in(RN_update_instr_RB_in[7]), 
    .i17_in(RN_update_instr_RB_in[6]), 
    .i18_in(RN_update_instr_RB_in[5]), 
    .i19_in(RN_update_instr_RB_in[4]), 
    .i20_in(RN_update_instr_RB_in[3]), 
    .i21_in(RN_update_instr_RB_in[2]), 
    .i22_in(RN_update_instr_RB_in[1]), 
    .i23_in(RN_update_instr_RB_in[0]), 
    .sel_in(tag_instr_in)
    );


assign rn_update_out = rn_update & or_invalid_instr_RB;


mux_24_1 #1 cpsr_update_invalid_addr (
    .y_out(cpsr_update), 
    .i0_in(CPSR_update_instr_RB_in[23]), 
    .i1_in(CPSR_update_instr_RB_in[22]), 
    .i2_in(CPSR_update_instr_RB_in[21]), 
    .i3_in(CPSR_update_instr_RB_in[20]), 
    .i4_in(CPSR_update_instr_RB_in[19]), 
    .i5_in(CPSR_update_instr_RB_in[18]), 
    .i6_in(CPSR_update_instr_RB_in[17]), 
    .i7_in(CPSR_update_instr_RB_in[16]), 
    .i8_in(CPSR_update_instr_RB_in[15]), 
    .i9_in(CPSR_update_instr_RB_in[14]), 
    .i10_in(CPSR_update_instr_RB_in[13]), 
    .i11_in(CPSR_update_instr_RB_in[12]), 
    .i12_in(CPSR_update_instr_RB_in[11]), 
    .i13_in(CPSR_update_instr_RB_in[10]), 
    .i14_in(CPSR_update_instr_RB_in[9]), 
    .i15_in(CPSR_update_instr_RB_in[8]), 
    .i16_in(CPSR_update_instr_RB_in[7]), 
    .i17_in(CPSR_update_instr_RB_in[6]), 
    .i18_in(CPSR_update_instr_RB_in[5]), 
    .i19_in(CPSR_update_instr_RB_in[4]), 
    .i20_in(CPSR_update_instr_RB_in[3]), 
    .i21_in(CPSR_update_instr_RB_in[2]), 
    .i22_in(CPSR_update_instr_RB_in[1]), 
    .i23_in(CPSR_update_instr_RB_in[0]), 
    .sel_in(tag_instr_in)
    );	 


assign cpsr_update_out = cpsr_update & or_invalid_instr_RB;


genvar p;
generate
for(p=`RB_SIZE-1;p>=0;p=p-1)
begin	:	grp_comparing_reg_alu_pipe1
	assign comparing_rd_pipe[p] =  rd_update_out & comparing_rd_pipe_in[p];
	
	
	assign comparing_rn_pipe[p] =  rn_update_out & comparing_rn_pipe_in[p];
	
	
	assign comparing_cpsr_pipe[p] =  cpsr_update_out & comparing_cpsr_pipe_in[p];
end
endgenerate


/******Generating all instruction above tag******/
genvar q;
generate
for(q=`RB_SIZE-1;q>0;q=q-1)
begin	:	grp_all_above_tag_position
	assign all_above_tag_position[q] = |(valid_decoded_addr_in[q-1:0]);
end
endgenerate


assign all_above_tag_position[0] = 1'b0;


assign head_greater_than_tag_position = head_pointer1_in & all_above_tag_position;


assign head_greater_than_tag_position_single = |(head_greater_than_tag_position);


assign valid_instr_above_tag_for_head_greater_than_tag = all_below_head_in & all_above_tag_position

& valid_instr_in;//last term may not be needed


assign valid_instr_above_tag_for_tag_greater_than_head = (all_above_tail_in & all_above_tag_position) 

| all_below_head_in & valid_instr_in;//last term may not be needed


assign valid_instr_above_tag_out = head_greater_than_tag_position_single ? 

valid_instr_above_tag_for_head_greater_than_tag : valid_instr_above_tag_for_tag_greater_than_head;


assign valid_instr_above_tag_same_rd_pipe = valid_instr_above_tag_out & comparing_rd_pipe;


assign valid_instr_above_tag_same_rn_pipe = valid_instr_above_tag_out & comparing_rn_pipe;


assign valid_instr_above_tag_same_cpsr_pipe = valid_instr_above_tag_out & comparing_cpsr_pipe;
/******Generating all instruction above tag******/


/******Generating new tag starts******/
genvar r;
generate
for(r=0;r<=`RB_SIZE-2;r=r+1)
begin	: grp_generate_all_below_invalid_instr
	assign all_below_invalid_instr[r] = |(invalid_instr_RB_out[`RB_SIZE-1:r+1]);
end
endgenerate

assign all_below_invalid_instr[`RB_SIZE-1] = 1'b0;


assign invalid_instr_above_tail = all_above_tail_in & all_below_invalid_instr;

/******RD STARTS******/

/******Generating New Tag Starts******/
priority_encoder_24_1 priority_encoder_for_head_greater_than_tail (
    .decoded_input_in(valid_instr_above_tag_same_rd_pipe), 
    .priority_encoded_out(new_tag_head_greater_than_tail_rd)
    );


assign instr_updating_rd_above_tail = all_above_tail_in & valid_instr_above_tag_same_rd_pipe;


priority_encoder_24_1 priority_encoder_for_tail_greater_than_head_and_instr_above_tail (
    .decoded_input_in(instr_updating_rd_above_tail), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_rd)
    );


assign instr_updating_rd_below_head = all_below_head_in & valid_instr_above_tag_same_rd_pipe;


priority_encoder_24_1 priority_encoder_for_tail_greater_than_head_and_instr_below_head (
    .decoded_input_in(instr_updating_rd_below_head), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_rd)
    );


assign or_invalid_instr_above_tail_out = |(invalid_instr_above_tail);


assign new_tag_tail_greater_than_head_final_rd = or_invalid_instr_above_tail_out ? 

new_tag_tail_greater_than_head_instr_above_tail_rd : 

new_tag_tail_greater_than_head_instr_below_head_rd;


assign new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RD_STARTS:`TAG_TO_CHANGE_RD_ENDS] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_rd : 

new_tag_tail_greater_than_head_final_rd;


assign rd_invalid_but_retiring_in_other_pipe_out = 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RD_STARTS:`TAG_TO_CHANGE_RD_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RD_STARTS:`TAG_TO_CHANGE_RD_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RD_STARTS:`TAG_TO_CHANGE_RD_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));
/******RD ENDS******/

/******RN STARTS******/

/******Generating New Tag Starts******/
priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_rn (
    .decoded_input_in(valid_instr_above_tag_same_rn_pipe), 
    .priority_encoded_out(new_tag_head_greater_than_tail_rn)
    );


assign instr_updating_rn_above_tail = all_above_tail_in & valid_instr_above_tag_same_rn_pipe;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_rn (
    .decoded_input_in(instr_updating_rn_above_tail), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_rn)
    );


assign instr_updating_rn_below_head = all_below_head_in & valid_instr_above_tag_same_rn_pipe;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_rn (
    .decoded_input_in(instr_updating_rn_below_head), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_rn)
    );


assign new_tag_tail_greater_than_head_final_rn = or_invalid_instr_above_tail_out ? 

new_tag_tail_greater_than_head_instr_above_tail_rn : 

new_tag_tail_greater_than_head_instr_below_head_rn;


assign new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RN_STARTS:`TAG_TO_CHANGE_RN_ENDS] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_rn : 

new_tag_tail_greater_than_head_final_rn;


assign rn_invalid_but_retiring_in_other_pipe_out =  

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RN_STARTS:`TAG_TO_CHANGE_RN_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RN_STARTS:`TAG_TO_CHANGE_RN_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RN_STARTS:`TAG_TO_CHANGE_RN_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));
/******RN ENDS******/

/******CPSR STARTS******/

/******New tag Starts******/
priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_cpsr (
    .decoded_input_in(valid_instr_above_tag_same_cpsr_pipe), 
    .priority_encoded_out(new_tag_head_greater_than_tail_cpsr)
    );


assign instr_updating_cpsr_above_tail = all_above_tail_in & valid_instr_above_tag_same_cpsr_pipe;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_cpsr (
    .decoded_input_in(instr_updating_cpsr_above_tail), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_cpsr)
    );


assign instr_updating_cpsr_below_head = all_below_head_in & valid_instr_above_tag_same_cpsr_pipe;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_cpsr (
    .decoded_input_in(instr_updating_cpsr_below_head), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_cpsr)
    );


assign new_tag_tail_greater_than_head_final_cpsr = or_invalid_instr_above_tail_out ? 

new_tag_tail_greater_than_head_instr_above_tail_cpsr : 

new_tag_tail_greater_than_head_instr_below_head_cpsr;


assign new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_CPSR_STARTS:`TAG_TO_CHANGE_CPSR_ENDS] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_cpsr : 

new_tag_tail_greater_than_head_final_cpsr;


assign cpsr_invalid_but_retiring_in_other_pipe_out =  

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_CPSR_STARTS:`TAG_TO_CHANGE_CPSR_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_CPSR_STARTS:`TAG_TO_CHANGE_CPSR_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_CPSR_STARTS:`TAG_TO_CHANGE_CPSR_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));
/******CPSR Ends******/


assign or_valid_instr_above_tag_same_rd_pipe_out = |(valid_instr_above_tag_same_rd_pipe);


assign or_valid_instr_above_tag_same_rn_pipe_out = |(valid_instr_above_tag_same_rn_pipe);


assign or_valid_instr_above_tag_same_cpsr_pipe_out = |(valid_instr_above_tag_same_cpsr_pipe);


assign tag_change_en_pipe_out[`RD_EN] = rd_update_out & or_valid_instr_above_tag_same_rd_pipe_out &

~rd_invalid_but_retiring_in_other_pipe_out;


assign tag_change_en_pipe_out[`RN_EN] = rn_update_out & or_valid_instr_above_tag_same_rn_pipe_out &

~rn_invalid_but_retiring_in_other_pipe_out;


assign tag_change_en_pipe_out[`CPSR_EN] = cpsr_update_out & 

or_valid_instr_above_tag_same_cpsr_pipe_out & ~cpsr_invalid_but_retiring_in_other_pipe_out;

endmodule 
