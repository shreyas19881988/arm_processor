`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module priority_encoder_24_1( input [`RB_SIZE-1:0] decoded_input_in,
										output [`TAG_SIZE-1:0] priority_encoded_out
    );


assign priority_encoded_out = (decoded_input_in[0] ? 23 : (decoded_input_in[1] ? 22 : 

(decoded_input_in[2] ? 21 : (decoded_input_in[3] ? 20 : (decoded_input_in[4] ? 19 : 

(decoded_input_in[5] ? 18 : (decoded_input_in[6] ? 17 : (decoded_input_in[7] ? 16 : 

(decoded_input_in[8] ? 15 : (decoded_input_in[9] ? 14 : (decoded_input_in[10] ? 13 : 

(decoded_input_in[11] ? 12 : (decoded_input_in[12] ? 11 : (decoded_input_in[13] ? 10 : 

(decoded_input_in[14] ? 9 : (decoded_input_in[15] ? 8 : (decoded_input_in[16] ? 7 : 

(decoded_input_in[17] ? 6 : (decoded_input_in[18] ? 5 : (decoded_input_in[19] ? 4 : 

(decoded_input_in[20] ? 3 : (decoded_input_in[21] ? 2 : (decoded_input_in[22] ? 1 : 

(decoded_input_in[23] ? 0 : 0))))))))))))))))))))))));

endmodule
