`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module valid_retire_other_than_load_store_pipe(
					input [`RB_SIZE-1:0] valid_decoded_addr_in,
					input [`RB_SIZE-1:0] speculative_instr_RB_in,
					input speculative_result_pipe_in,
					input instr_complete_pipe_in,
					input [`RB_SIZE-1:0] RD_update_instr_RB_in,
					input [`RB_SIZE-1:0] RN_update_instr_RB_in,
					input [`RB_SIZE-1:0] CPSR_update_instr_RB_in,
					output [`RB_SIZE-1:0] instr_valid_retire_out, 
					output [`RB_SIZE-1:0] instr_valid_retire_with_rd_update_out,
					output [`RB_SIZE-1:0] instr_valid_retire_with_rn_update_out,
					output [`RB_SIZE-1:0] instr_valid_retire_with_cpsr_update_out,
					output [`RB_SIZE-1:0] instr_valid_retire_irrespective_of_speculative_out
	 );

wire [`RB_SIZE-1:0] instr_partial_retire;

genvar i;
generate
for(i=`RB_SIZE-1;i>=0;i=i-1)
begin	:	grp_valid_retire_block
	assign instr_partial_retire[i] = valid_decoded_addr_in[i] & (~speculative_instr_RB_in[i] | 
	
	speculative_result_pipe_in);
	
	
	assign instr_valid_retire_out[i] = instr_partial_retire[i] & instr_complete_pipe_in;
	
	
	assign instr_valid_retire_irrespective_of_speculative_out[i] = valid_decoded_addr_in[i] & 
	
	instr_complete_pipe_in;
	
end
endgenerate
	
	
assign instr_valid_retire_with_rd_update_out = instr_valid_retire_out & RD_update_instr_RB_in;
	
	
assign instr_valid_retire_with_rn_update_out = instr_valid_retire_out & RN_update_instr_RB_in;
	
	
assign instr_valid_retire_with_cpsr_update_out = instr_valid_retire_out & CPSR_update_instr_RB_in;

endmodule
