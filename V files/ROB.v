`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module ROB( 
				input clk_in,
				input reset_in, 
				
				/******Two new instructions are to be put in the ROB starts******/
				input new_entry_enable_in,
				/******Two new instructions are to be put in the ROB ends******/
				
				/******Only one instruction can be committed starts******/
				//input retire_1_instr_in, 
				/******Only one instruction can be committed ends******/
				
				/******Only one instruction can be committed starts******/
				//input retire_2_instr_in,
				/******Only one instruction can be committed ends******/
				
				/******REORDER_BUFFER_ENTRY_WORD_STARTS******/
				input [`RB_ENTRY_WORD-1:0] RB_entry1_info_in,
				input [`RB_ENTRY_WORD-1:0] RB_entry2_info_in,
				/******REORDER_BUFFER_ENTRY_WORD_ENDS******/
				
				/******TAG_OF_INSTR_IN_PIPES_STARTS******/
				input [`PIPES_INSTR_TAG_WORD-1:0] tag_instr_pipes_in,
				/******TAG_OF_INSTR_IN_PIPES_ENDS******/
				
				/******Modified register address starts******/
				input [`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE-1:0] mod_reg_addr_rd_in,
				input [`LOG_MOD_REG_ADDR_4_PIPES_COMBINED_SIZE-1:0] mod_reg_addr_rn_in,
				input [`MOD_CPSR_ADDR_4_PIPES_COMBINED_SIZE-1:0] mod_reg_addr_cpsr_in,
				/******Modified register address ends******/
				
				/******Instruction Complete Starts******/
				input [`NO_OF_PIPES-1:0] instr_complete_in,
				/******Instruction Complete Ends******/

				
				/******Speculative Result Starts******/
				input [`NO_OF_PIPES-1:0] speculative_result_in,
				/******Speculative Result Ends******/
				
				
				/******Load store multiple instructions Starts******/
				input ldm_stm_stall_in,
				/******Load store multiple instructions Ends******/
				
				
				/******Issue to pipes Starts******/
				
				/******ALU PIPE1 STARTS******/
				
				/******RN Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_alu_pipe1_rn_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_alu_pipe1_rn_in,
				/******RN Ends******/
				
				/******Rm Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_alu_pipe1_rm_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_alu_pipe1_rm_in,
				/******Rm Ends******/
								
				/******Rs Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_alu_pipe1_rs_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_alu_pipe1_rs_in,
				/******Rs Ends******/
				
				/******CPSR Starts******/
				input [`TAG_SIZE-1:0] tag_to_issue_alu_pipe1_cpsr_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_alu_pipe1_cpsr_in,
				/******CPSR Ends******/
				
				/******ALU PIPE1 ENDS******/
				
				
				/******ALU PIPE2 STARTS******/
				
				/******RN Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_alu_pipe2_rn_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_alu_pipe2_rn_in,
				/******RN Ends******/
				
				/******Rm Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_alu_pipe2_rm_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_alu_pipe2_rm_in,
				/******Rm Ends******/
								
				/******Rs Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_alu_pipe2_rs_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_alu_pipe2_rs_in,
				/******Rs Ends******/
				
				/******CPSR Starts******/
				input [`TAG_SIZE-1:0] tag_to_issue_alu_pipe2_cpsr_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_alu_pipe2_cpsr_in,
				/******CPSR Ends******/
				
				/******ALU PIPE2 ENDS******/
				
				
				/******LS PIPE3 STARTS******/
				
				/******RN Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_ls_pipe3_rn_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_ls_pipe3_rn_in,
				/******RN Ends******/
				
				/******Rm Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_ls_pipe3_rm_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_ls_pipe3_rm_in,
				/******Rm Ends******/
								
				/******Rs Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_ls_pipe3_rs_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_ls_pipe3_rs_in,
				/******Rs Ends******/
				
				/******CPSR Starts******/
				input [`TAG_SIZE-1:0] tag_to_issue_ls_pipe3_cpsr_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_ls_pipe3_cpsr_in,
				/******CPSR Ends******/
				
				/******LS_PIPE3 ENDS******/
				
				
				/******BRANCH PIPE4 STARTS******/
				
				/******RN Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_branch_pipe4_rn_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_branch_pipe4_rn_in,
				/******RN Ends******/
				
				/******Rm Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_branch_pipe4_rm_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_branch_pipe4_rm_in,
				/******Rm Ends******/
								
				/******Rs Starts******/
				input [`TAG_TO_ISSUE_PLUS_LOG_REG-1:0] tag_to_issue_plus_branch_pipe4_rs_log_address_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_branch_pipe4_rs_in,
				/******Rs Ends******/
				
				/******CPSR Starts******/
				input [`TAG_SIZE-1:0] tag_to_issue_branch_pipe4_cpsr_in,
				input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_branch_pipe4_cpsr_in,
				/******CPSR Ends******/
				
				/******BRANCH_PIPE4 ENDS******/
				
				
				/******Tag retire******/
				output [`RD_RN_CPSR_EN-1:0] tag_retire_en_alu_pipe1_out,
				output [`RD_RN_CPSR_EN-1:0] tag_retire_en_alu_pipe2_out,
				output [`RD_RN_CPSR_EN-1:0] tag_retire_en_ls_pipe3_out,
				output [`RD_RN_CPSR_EN-1:0] tag_retire_en_branch_pipe4_out,
				
				
				/******Tag change******/
				output [`RD_RN_CPSR_EN-1:0] tag_change_en_alu_pipe1_out,
				output [`RD_RN_CPSR_EN-1:0] tag_change_en_alu_pipe2_out,
				output [`RD_RN_CPSR_EN-1:0] tag_change_en_ls_pipe3_out,
				output [`RD_RN_CPSR_EN-1:0] tag_change_en_branch_pipe4_out,
				output [`TAG_TO_CHANGE_RD_RN_CPSR-1:0] tag_to_change_alu_pipe1_out,
				output [`TAG_TO_CHANGE_RD_RN_CPSR-1:0] tag_to_change_alu_pipe2_out,
				output [`TAG_TO_CHANGE_RD_RN_CPSR-1:0] tag_to_change_ls_pipe3_out,
				output [`TAG_TO_CHANGE_RD_RN_CPSR-1:0] tag_to_change_branch_pipe4_out,
				
				/******Tag change in case of load multiple******/
				output [`NO_OF_REG-1:0] tag_change_en_in_case_of_ldm_ls_pipe3_out,
				output [`TAG_ALL_REG-1:0] tag_to_change_in_case_of_ldm_ls_pipe3_out,
				output [`NO_OF_REG-1:0] tag_retire_en_in_case_of_ldm_ls_pipe3_out,
				
				/******Issue to pipes Starts******/
				
				/******ALU_PIPE1_STARTS******/
				
				/******RN_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_alu_pipe1_rn_out,
				/******RN_ADDR_STARTS******/
				
				/******RM_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_alu_pipe1_rm_out,
				/******RM_ADDR_STARTS******/
				
				/******RS_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_alu_pipe1_rs_out,
				/******RM_ADDR_STARTS******/
				
				/******CPSR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_alu_pipe1_cpsr_out,
				/******CPSR_STARTS******/
				
				/******ALU_PIPE1_ENDS******/
				
				
				/******ALU_PIPE2_STARTS******/
				
				/******RN_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_alu_pipe2_rn_out,
				/******RN_ADDR_STARTS******/
				
				/******RM_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_alu_pipe2_rm_out,
				/******RM_ADDR_STARTS******/
				
				/******RS_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_alu_pipe2_rs_out,
				/******RM_ADDR_STARTS******/
				
				/******CPSR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_alu_pipe2_cpsr_out,
				/******CPSR_STARTS******/
				
				/******ALU_PIPE2_ENDS******/
				
				
				/******LS_PIPE3_STARTS******/
				
				/******RN_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_ls_pipe3_rn_out,
				/******RN_ADDR_STARTS******/
				
				/******RM_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_ls_pipe3_rm_out,
				/******RM_ADDR_STARTS******/
				
				/******RS_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_ls_pipe3_rs_out,
				/******RM_ADDR_STARTS******/
				
				/******CPSR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_ls_pipe3_cpsr_out,
				/******CPSR_STARTS******/
				
				/******LS_PIPE3_ENDS******/
				
				
				/******BRANCH_PIPE4_STARTS******/
				
				/******RN_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_branch_pipe4_rn_out,
				/******RN_ADDR_STARTS******/
				
				/******RM_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_branch_pipe4_rm_out,
				/******RM_ADDR_STARTS******/
				
				/******RS_ADDR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_branch_pipe4_rs_out,
				/******RM_ADDR_STARTS******/
				
				/******CPSR_STARTS******/
				output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_branch_pipe4_cpsr_out
				/******CPSR_STARTS******/
				
				/******BRANCH_PIPE4_ENDS******/
				
				
				
				
				
    );

wire [`RB_SIZE-1:0] tail_pointer;
wire [`RB_SIZE-1:0] tail_pointer_instr1;
wire [`RB_SIZE-1:0] tail_pointer_instr2;
wire [`RB_SIZE-1:0] head_pointer_instr1;
wire [`RB_SIZE-1:0] head_pointer_instr2;
wire [`RB_SIZE-1:0] head_pointer;
wire RB_full,RB_empty;
wire [`RB_ENTRY_WORD-1:0] reorder_buffer_entry [`RB_SIZE-1:0];
wire [`RB_ENTRY_WORD-1:0] data_to_reorder_buffer_reg [`RB_SIZE-1:0];
wire [`RB_SIZE-1:0] valid_decoded_addr_alu_pipe1;
wire [`RB_SIZE-1:0] valid_decoded_addr_alu_pipe2;
wire [`RB_SIZE-1:0] valid_decoded_addr_ls_pipe3;
wire [`RB_SIZE-1:0] valid_decoded_addr_branch_pipe4;
wire [`RB_SIZE-1:0] data_to_valid_retire_reg;
wire [`RB_SIZE-1:0] valid_retire_reg;
wire [`RB_SIZE-1:0] valid_retire;
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] data_to_mod_reg_addr_rd [`RB_SIZE-1:0];
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rd [`RB_SIZE-1:0];
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] data_to_mod_reg_addr_rn [`RB_SIZE-1:0];
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_rn [`RB_SIZE-1:0];
wire [`MOD_REG_ADDR_SIZE-1:0] data_to_mod_reg_addr_cpsr [`RB_SIZE-1:0];
wire [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr [`RB_SIZE-1:0];
wire [`RB_SIZE-1:0] instr_alu_pipe1_valid_retire;
wire [`RB_SIZE-1:0] valid_retire_rd;
wire [`RB_SIZE-1:0] valid_retire_rn;
wire [`RB_SIZE-1:0] valid_retire_cpsr;
wire [`RB_SIZE-1:0] valid_instr_rd_retire_alu_pipe1;
wire [`RB_SIZE-1:0] valid_instr_rd_retire_alu_pipe2;
wire [`RB_SIZE-1:0] valid_instr_rd_retire_ls_pipe3;
wire [`RB_SIZE-1:0] valid_instr_rd_retire_branch_pipe4;
wire [`RB_SIZE-1:0] valid_instr_rn_retire_alu_pipe1;
wire [`RB_SIZE-1:0] valid_instr_rn_retire_alu_pipe2;
wire [`RB_SIZE-1:0] valid_instr_rn_retire_ls_pipe3;
wire [`RB_SIZE-1:0] valid_instr_rn_retire_branch_pipe4;
wire [`RB_SIZE-1:0] valid_instr_cpsr_retire_alu_pipe1;
wire [`RB_SIZE-1:0] valid_instr_cpsr_retire_alu_pipe2;
wire [`RB_SIZE-1:0] valid_instr_cpsr_retire_ls_pipe3;
wire [`RB_SIZE-1:0] valid_instr_cpsr_retire_branch_pipe4;
wire [`RB_SIZE-1:0] instr_alu_pipe2_valid_retire;
wire [`RB_SIZE-1:0] instr_ls_pipe3_valid_retire;
wire [`RB_SIZE-1:0] instr_branch_pipe4_valid_retire;
wire [`LOG_MOD_REG_ADDR_SIZE:0] ldm_data_reg [`NO_OF_REG-1:0];
wire [`LOG_MOD_REG_ADDR_SIZE:0] data_to_ldm_data_reg [`NO_OF_REG-1:0];
wire [`RB_SIZE-1:0] decoded_ls_pipe3_ls_mult_instr;
wire or_decoded_branch_pipe4_ls_mult_instr;
wire [`LOG_MOD_REG_ADDR_SIZE-1:0] selected_mod_reg_addr;
wire [`RB_SIZE-1:0] reset_for_valid_retire_reg;
wire en_for_valid_retire_reg;
wire [`NO_OF_REG-1:0] en_for_ldm_data;
wire [`NO_OF_REG-1:0] data_for_en_for_ldm_data;
wire retire_ldm;
wire [`RB_SIZE-1:0] invalid_instruction_alu_pipe1;
wire [`RB_SIZE-1:0] invalid_instruction_alu_pipe2;
wire [`RB_SIZE-1:0] invalid_instruction_ls_pipe3;
wire [`RB_SIZE-1:0] invalid_instruction_branch_pipe4;
wire [`RB_SIZE-1:0] invalid_ldm_instruction_ls_pipe3;
wire [`LOG_ADDRESS_SIZE-1:0] invalid_rd_alu_pipe1;
wire [`LOG_ADDRESS_SIZE-1:0] invalid_rd_alu_pipe2;
wire [`LOG_ADDRESS_SIZE-1:0] invalid_rd_ls_pipe3;
wire [`LOG_ADDRESS_SIZE-1:0] invalid_rd_branch_pipe4;
wire [`LOG_ADDRESS_SIZE-1:0] invalid_rn_alu_pipe1;
wire [`LOG_ADDRESS_SIZE-1:0] invalid_rn_alu_pipe2;
wire [`LOG_ADDRESS_SIZE-1:0] invalid_rn_ls_pipe3;
wire [`LOG_ADDRESS_SIZE-1:0] invalid_rn_branch_pipe4;
wire [`RB_SIZE-1:0] comparing_rd_alu_pipe1;
wire [`RB_SIZE-1:0] comparing_rd_alu_pipe2;
wire [`RB_SIZE-1:0] comparing_rd_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_rd_branch_pipe4;
wire [`RB_SIZE-1:0] comparing_rn_alu_pipe1;
wire [`RB_SIZE-1:0] comparing_rn_alu_pipe2;
wire [`RB_SIZE-1:0] comparing_rn_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_rn_branch_pipe4;
wire [`RB_SIZE-1:0] comparing_cpsr_alu_pipe1;
wire [`RB_SIZE-1:0] comparing_cpsr_alu_pipe2;
wire [`RB_SIZE-1:0] comparing_cpsr_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_cpsr_branch_pipe4;
wire [`RB_SIZE-1:0] valid_instructions_in_RB;
wire head_greater_than_tail;
wire [`RB_SIZE-1:0] all_above_tail;
wire [`RB_SIZE-1:0] all_below_head; 
wire [`RB_SIZE-1:0] speculative_instr_RB; 
wire [`RB_SIZE-1:0] load_multiple_instr_RB; 
wire [`RB_SIZE-1:0] RD_update_instr_RB; 
wire [`RB_SIZE-1:0] RN_update_instr_RB;
wire [`RB_SIZE-1:0] CPSR_update_instr_RB;
wire alu_pipe1_invalid_but_updating_rd,alu_pipe2_invalid_but_updating_rd;
wire ls_pipe3_invalid_but_updating_rd,branch_pipe4_invalid_but_updating_rd;
wire alu_pipe1_invalid_but_updating_rn,alu_pipe2_invalid_but_updating_rn;
wire ls_pipe3_invalid_but_updating_rn,branch_pipe4_invalid_but_updating_rn;
wire alu_pipe1_invalid_but_updating_cpsr,alu_pipe2_invalid_but_updating_cpsr;
wire ls_pipe3_invalid_but_updating_cpsr,branch_pipe4_invalid_but_updating_cpsr;
wire alu_pipe1_rd_invalid_but_retiring_in_other_pipe,alu_pipe1_rn_invalid_but_retiring_in_other_pipe;
wire alu_pipe1_cpsr_invalid_but_retiring_in_other_pipe;
wire alu_pipe2_rd_invalid_but_retiring_in_other_pipe,alu_pipe2_rn_invalid_but_retiring_in_other_pipe;
wire alu_pipe2_cpsr_invalid_but_retiring_in_other_pipe;
wire ls_pipe3_rd_invalid_but_retiring_in_other_pipe,ls_pipe3_rn_invalid_but_retiring_in_other_pipe;
wire ls_pipe3_cpsr_invalid_but_retiring_in_other_pipe;
wire branch_pipe4_rd_invalid_but_retiring_in_other_pipe;
wire branch_pipe4_rn_invalid_but_retiring_in_other_pipe;
wire branch_pipe4_cpsr_invalid_but_retiring_in_other_pipe;
wire [`LDM_WORD-1:0] ldm_word_frm_RB [`RB_SIZE-1:0];
wire [`RB_SIZE-1:0] valid_instr_above_tag_alu_pipe1;
wire [`RB_SIZE-1:0] valid_instr_above_tag_alu_pipe2;
wire [`RB_SIZE-1:0] valid_instr_above_tag_ls_pipe3;
wire [`RB_SIZE-1:0] valid_instr_above_tag_branch_pipe4;
wire or_invalid_instr_above_tail_alu_pipe1;
wire or_invalid_instr_above_tail_alu_pipe2;
wire or_invalid_instr_above_tail_ls_pipe3;
wire or_invalid_instr_above_tail_branch_pipe4;
wire [`RB_SIZE-1:0] comparing_r0_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r1_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r2_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r3_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r4_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r5_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r6_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r7_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r8_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r9_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r10_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r11_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r12_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r13_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r14_ls_pipe3;
wire [`RB_SIZE-1:0] comparing_r15_ls_pipe3;
wire or_valid_instr_above_tag_same_rd_alu_pipe1;
wire or_valid_instr_above_tag_same_rd_alu_pipe2;
wire or_valid_instr_above_tag_same_rd_ls_pipe3;
wire or_valid_instr_above_tag_same_rd_branch_pipe4;
wire or_valid_instr_above_tag_same_rn_alu_pipe1;
wire or_valid_instr_above_tag_same_rn_alu_pipe2;
wire or_valid_instr_above_tag_same_rn_ls_pipe3;
wire or_valid_instr_above_tag_same_rn_branch_pipe4;
wire or_valid_instr_above_tag_same_cpsr_alu_pipe1;
wire or_valid_instr_above_tag_same_cpsr_alu_pipe2;
wire or_valid_instr_above_tag_same_cpsr_ls_pipe3;
wire or_valid_instr_above_tag_same_cpsr_branch_pipe4;
wire [`RB_SIZE-1:0] decoded_addr_for_instr_issue_alu_pipe1_rn;
wire [`RB_SIZE-1:0] instr_to_issue_alu_pipe1_rn_valid;
wire instr_issue_alu_pipe1_rn_ldm;
wire [`NO_OF_REG-1:0] instr_issue_alu_pipe1_rn_ldm_address_compare; 
wire [`LOG_ADDRESS_SIZE-1:0] address_of_reg_in_ldm_alu_pipe1_rn;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_to_issue_frm_ldm_alu_pipe1_rn;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rd_for_instr_issue_alu_pipe1_rn;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rn_for_instr_issue_alu_pipe1_rn;
wire data_in_rd_instr_issue_alu_pipe1_rn;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_rd_or_rn_for_instr_issue_alu_pipe1_rn;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_RB_for_instr_issue_alu_pipe1_rn;
wire retire_1_instr;
wire retire_2_instr;
wire ldm_retire_complete_instr1;
wire ldm_retire_complete_instr2;
wire [`RB_SIZE-1:0] instr_valid_retire_irrespective_of_speculative_alu_pipe1;
wire [`RB_SIZE-1:0] instr_valid_retire_irrespective_of_speculative_alu_pipe2;
wire [`RB_SIZE-1:0] instr_valid_retire_irrespective_of_speculative_ls_pipe3;
wire [`RB_SIZE-1:0] instr_valid_retire_irrespective_of_speculative_branch_pipe4;



/******Head Tail Pointer Controller******/
head_tail_pointer_RB head_tail_pointer_controller (
		.clk_in(clk_in), 
		.reset_in(reset_in), 
		.new_entry_enable_in(new_entry_enable_in), 
		.retire_1_instr_in(retire_1_instr), 
		.retire_2_instr_in(retire_2_instr), 
		.head_pointer_out(head_pointer), 
		.tail_pointer_out(tail_pointer), 
		.RB_full_out(RB_full),
		.RB_empty_out(RB_empty)
	);
/******Head Tail Pointer Controller******/

/******New Reorder Buffer Data Preparation Start******/

/******Using decoder start******/
/*decoder_24 addr1 (
				.tag_in(RB_entry1_info_in[`RB_TAG_START:`RB_TAG_END]),
				.decoded_addr_out(tail_pointer_instr1)
				);

decoder_24 addr2 (
				.tag_in(RB_entry2_info_in[`RB_TAG_START:`RB_TAG_END]),
				.decoded_addr_out(tail_pointer_instr2)
				);*/
/******Using Decoder end******/

/******Using Tail Pointer Start******/
separating_head_and_tail_pointers head_and_tail_pointers_separated (
    .head_pointer_in(head_pointer), 
    .tail_pointer_in(tail_pointer), 
    .head_pointer1_out(head_pointer_instr1), 
    .head_pointer2_out(head_pointer_instr2), 
    .tail_pointer1_out(tail_pointer_instr1), 
    .tail_pointer2_out(tail_pointer_instr2)
    );
/******Using Tail Pointer End******/

genvar i;
generate
for(i=`RB_SIZE-1;i>=0;i=i-1)
begin : grp_data_to_reorder_buffer_reg
	assign data_to_reorder_buffer_reg[i] = ({`RB_ENTRY_WORD{tail_pointer_instr1[i]}} & 
	
	RB_entry1_info_in) | ({`RB_ENTRY_WORD{tail_pointer_instr2[i]}} & RB_entry2_info_in);
end
endgenerate
/******New Reorder Buffer Data Preparation End******/

/******New Reorder Buffer Data Register******/
genvar j;
generate
for(j=`RB_SIZE-1;j>=0;j=j-1)
begin : grp_reorder_buffer_reg
	register_with_reset #`RB_ENTRY_WORD reg_reorder_buffer (
		 .data_in(data_to_reorder_buffer_reg[j]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(new_entry_enable_in & tail_pointer[j]), 
		 .data_out(reorder_buffer_entry[j])
		 );
end
endgenerate
/******New Reorder Buffer Data Register******/

/******Address generation for Mod Reg Addr******/
decoded_address_generation_for_4_pipes decoding_all_4_pipes_tags_valid_instruction (
    .tag_instr_pipes_in(tag_instr_pipes_in), 
    .valid_instructions_in_RB_in(valid_instructions_in_RB), 
    .decoded_valid_addr_alu_pipe1_out(valid_decoded_addr_alu_pipe1), 
    .decoded_valid_addr_alu_pipe2_out(valid_decoded_addr_alu_pipe2), 
    .decoded_valid_addr_ls_pipe3_out(valid_decoded_addr_ls_pipe3), 
    .decoded_valid_addr_branch_pipe4_out(valid_decoded_addr_branch_pipe4)
    );
/******Address generation for Mod Reg Addr******/

genvar k;
generate
for(k=`RB_SIZE-1;k>=0;k=k-1)
begin	:	extracting_data_frm_rB
	assign speculative_instr_RB[k] = reorder_buffer_entry[k][`RB_SPECULATIVE_INSTR];
	assign load_multiple_instr_RB[k] = reorder_buffer_entry[k][`RB_LOAD_MULTIPLE_TYPE_INSTR];
	assign RD_update_instr_RB[k] = reorder_buffer_entry[k][`RB_RD_UPDATE];
	assign RN_update_instr_RB[k] = reorder_buffer_entry[k][`RB_RN_UPDATE];
	assign CPSR_update_instr_RB[k] = reorder_buffer_entry[k][`RB_CPSR_UPDATE];
	assign ldm_word_frm_RB[k] = reorder_buffer_entry[k][`RB_LDM_REG_R15:`RB_LDM_REG_R0];
end
endgenerate


valid_instr_in_RB valid_instrs (
    .head_pointer1_in(head_pointer_instr1), 
    .tail_pointer1_in(tail_pointer_instr1), 
    .valid_instr_out(valid_instructions_in_RB), 
    .head_greater_than_tail_out(head_greater_than_tail),
	 .all_above_tail_out(all_above_tail),
	 .all_below_head_out(all_below_head)
    );


/******Data Preparation Starts******/
genvar l;
generate
for(l=`RB_SIZE-1;l>=0;l=l-1)
begin :	grp_data_to_mod_reg_addr
	/******Rd modified address****/
	assign data_to_mod_reg_addr_rd[l] = ({`LOG_MOD_REG_ADDR_SIZE{valid_decoded_addr_alu_pipe1[l]}} & 
	
	mod_reg_addr_rd_in[`LOG_REG_ADDR_ALU_PIPE1_START:`MOD_REG_ADDR_ALU_PIPE1_END]) | 
	
	({`LOG_MOD_REG_ADDR_SIZE{valid_decoded_addr_alu_pipe2[l]}} & 
	
	mod_reg_addr_rd_in[`LOG_REG_ADDR_ALU_PIPE2_START:`MOD_REG_ADDR_ALU_PIPE2_END]) | 	
	
	({`LOG_MOD_REG_ADDR_SIZE{valid_decoded_addr_ls_pipe3[l] }} & 
	
	mod_reg_addr_rd_in[`LOG_REG_ADDR_LS_PIPE3_START:`MOD_REG_ADDR_LS_PIPE3_END]) | 
	
	({`LOG_MOD_REG_ADDR_SIZE{valid_decoded_addr_branch_pipe4[l]}} & 
	
	mod_reg_addr_rd_in[`LOG_REG_ADDR_BRANCH_PIPE4_START:`MOD_REG_ADDR_BRANCH_PIPE4_END]);
	
	/******Rn modified address******/
	assign data_to_mod_reg_addr_rn[l] = ({`LOG_MOD_REG_ADDR_SIZE{valid_decoded_addr_alu_pipe1[l]}} & 
	
	mod_reg_addr_rn_in[`LOG_REG_ADDR_ALU_PIPE1_START:`MOD_REG_ADDR_ALU_PIPE1_END]) | 
	
	({`LOG_MOD_REG_ADDR_SIZE{valid_decoded_addr_alu_pipe2[l]}} & 
	
	mod_reg_addr_rn_in[`LOG_REG_ADDR_ALU_PIPE2_START:`MOD_REG_ADDR_ALU_PIPE2_END]) | 	
	
	({`LOG_MOD_REG_ADDR_SIZE{valid_decoded_addr_ls_pipe3[l]}} & 
	
	mod_reg_addr_rn_in[`LOG_REG_ADDR_LS_PIPE3_START:`MOD_REG_ADDR_LS_PIPE3_END]) | 
	
	({`LOG_MOD_REG_ADDR_SIZE{valid_decoded_addr_branch_pipe4[l]}} & 
	
	mod_reg_addr_rn_in[`LOG_REG_ADDR_BRANCH_PIPE4_START:`MOD_REG_ADDR_BRANCH_PIPE4_END]);
	
	/******CPSR modified address******/
	assign data_to_mod_reg_addr_cpsr[l] = ({`MOD_REG_ADDR_SIZE{valid_decoded_addr_alu_pipe1[l]}} & 
	
	mod_reg_addr_cpsr_in[`MOD_CPSR_ADDR_ALU_PIPE1_START:`MOD_CPSR_ADDR_ALU_PIPE1_END]) | 
	
	({`MOD_REG_ADDR_SIZE{valid_decoded_addr_alu_pipe2[l]}} & 
	
	mod_reg_addr_cpsr_in[`MOD_CPSR_ADDR_ALU_PIPE2_START:`MOD_CPSR_ADDR_ALU_PIPE2_END]) | 	
	
	({`MOD_REG_ADDR_SIZE{valid_decoded_addr_ls_pipe3[l]}} & 
	
	mod_reg_addr_cpsr_in[`MOD_CPSR_ADDR_LS_PIPE3_START:`MOD_CPSR_ADDR_LS_PIPE3_END]) | 
	
	({`MOD_REG_ADDR_SIZE{valid_decoded_addr_branch_pipe4[l]}} & 
	
	mod_reg_addr_cpsr_in[`MOD_CPSR_ADDR_BRANCH_PIPE4_START:`MOD_CPSR_ADDR_BRANCH_PIPE4_END]);
end
endgenerate
	
	valid_retire_other_than_load_store_pipe valid_retire_alu_pipe1 (
    .valid_decoded_addr_in(valid_decoded_addr_alu_pipe1), 
    .speculative_instr_RB_in(speculative_instr_RB), 
    .speculative_result_pipe_in(speculative_result_in[`ALU_PIPE1]), 
    .instr_complete_pipe_in(instr_complete_in[`ALU_PIPE1]), 
    .RD_update_instr_RB_in(RD_update_instr_RB), 
    .RN_update_instr_RB_in(RN_update_instr_RB), 
    .CPSR_update_instr_RB_in(CPSR_update_instr_RB), 
    .instr_valid_retire_out(instr_alu_pipe1_valid_retire), 
    .instr_valid_retire_with_rd_update_out(valid_instr_rd_retire_alu_pipe1), 
    .instr_valid_retire_with_rn_update_out(valid_instr_rn_retire_alu_pipe1), 
    .instr_valid_retire_with_cpsr_update_out(valid_instr_cpsr_retire_alu_pipe1),
	 .instr_valid_retire_irrespective_of_speculative_out(
	 instr_valid_retire_irrespective_of_speculative_alu_pipe1)
    );
	 
	 
	 valid_retire_other_than_load_store_pipe valid_retire_alu_pipe2 (
    .valid_decoded_addr_in(valid_decoded_addr_alu_pipe2), 
    .speculative_instr_RB_in(speculative_instr_RB), 
    .speculative_result_pipe_in(speculative_result_in[`ALU_PIPE2]), 
    .instr_complete_pipe_in(instr_complete_in[`ALU_PIPE2]), 
    .RD_update_instr_RB_in(RD_update_instr_RB), 
    .RN_update_instr_RB_in(RN_update_instr_RB), 
    .CPSR_update_instr_RB_in(CPSR_update_instr_RB), 
    .instr_valid_retire_out(instr_alu_pipe2_valid_retire), 
    .instr_valid_retire_with_rd_update_out(valid_instr_rd_retire_alu_pipe2), 
    .instr_valid_retire_with_rn_update_out(valid_instr_rn_retire_alu_pipe2), 
    .instr_valid_retire_with_cpsr_update_out(valid_instr_cpsr_retire_alu_pipe2),
	 .instr_valid_retire_irrespective_of_speculative_out(
	 instr_valid_retire_irrespective_of_speculative_alu_pipe2)
	 );
	 
	 
	 valid_retire_sub_block valid_retire_ls_pipe3 (
    .valid_decoded_addr_in(valid_decoded_addr_ls_pipe3), 
    .speculative_instr_RB_in(speculative_instr_RB), 
    .speculative_result_pipe_in(speculative_result_in[`LS_PIPE3]), 
    .instr_complete_pipe_in(instr_complete_in[`LS_PIPE3]), 
    .RD_update_instr_RB_in(RD_update_instr_RB), 
    .RN_update_instr_RB_in(RN_update_instr_RB), 
    .CPSR_update_instr_RB_in(CPSR_update_instr_RB), 
    .load_multiple_instr_RB_in(load_multiple_instr_RB), 
	 .ldm_stm_stall_in(ldm_stm_stall_in),
    .instr_valid_retire_out(instr_ls_pipe3_valid_retire), 
    .instr_valid_retire_with_rd_update_out(valid_instr_rd_retire_ls_pipe3), 
    .instr_valid_retire_with_rn_update_out(valid_instr_rn_retire_ls_pipe3), 
    .instr_valid_retire_with_cpsr_update_out(valid_instr_cpsr_retire_ls_pipe3),
	 .decoded_ls_mult_instr_out(decoded_ls_pipe3_ls_mult_instr),
	 .instr_valid_retire_irrespective_of_speculative_out(
	 instr_valid_retire_irrespective_of_speculative_ls_pipe3)
    );
	 
	 
	 valid_retire_other_than_load_store_pipe valid_retire_branch_pipe4 (
    .valid_decoded_addr_in(valid_decoded_addr_branch_pipe4), 
    .speculative_instr_RB_in(speculative_instr_RB), 
    .speculative_result_pipe_in(speculative_result_in[`BRANCH_PIPE4]), 
    .instr_complete_pipe_in(instr_complete_in[`BRANCH_PIPE4]), 
    .RD_update_instr_RB_in(RD_update_instr_RB), 
    .RN_update_instr_RB_in(RN_update_instr_RB), 
    .CPSR_update_instr_RB_in(CPSR_update_instr_RB), 
    .instr_valid_retire_out(instr_branch_pipe4_valid_retire), 
    .instr_valid_retire_with_rd_update_out(valid_instr_rd_retire_branch_pipe4), 
    .instr_valid_retire_with_rn_update_out(valid_instr_rn_retire_branch_pipe4), 
    .instr_valid_retire_with_cpsr_update_out(valid_instr_cpsr_retire_branch_pipe4),
	 .instr_valid_retire_irrespective_of_speculative_out(
	 instr_valid_retire_irrespective_of_speculative_branch_pipe4)
	 );

/******Data Preparation Ends******/

/******TAG RETIRE STARTS******/
assign valid_retire = instr_valid_retire_irrespective_of_speculative_alu_pipe1 | 

instr_valid_retire_irrespective_of_speculative_alu_pipe2 | 

instr_valid_retire_irrespective_of_speculative_ls_pipe3 | 

instr_valid_retire_irrespective_of_speculative_branch_pipe4;


assign valid_retire_rd = valid_instr_rd_retire_alu_pipe1 | valid_instr_rd_retire_alu_pipe2 | 

valid_instr_rd_retire_ls_pipe3 | valid_instr_rd_retire_branch_pipe4;


assign valid_retire_rn = valid_instr_rn_retire_alu_pipe1 | valid_instr_rn_retire_alu_pipe2 | 

valid_instr_rn_retire_ls_pipe3 | valid_instr_rn_retire_branch_pipe4;


assign valid_retire_cpsr = valid_instr_cpsr_retire_alu_pipe1 | valid_instr_cpsr_retire_alu_pipe2 | 

valid_instr_cpsr_retire_ls_pipe3 | valid_instr_cpsr_retire_branch_pipe4;


/******ALU PIPE1 STARTS******/
tag_retire_en_generation_other_than_load_store tag_retire_en_alu_pipe1_generation (
    .valid_instr_retire_updating_rd_pipe_in(valid_instr_rd_retire_alu_pipe1), 
    .instr_invalid_pipe_but_updating_rd_in(alu_pipe1_invalid_but_updating_rd), 
    .instr_invalid_pipe_updating_rd_but_retiring_frm_other_pipe_in(
	 alu_pipe1_rd_invalid_but_retiring_in_other_pipe), 
    .valid_instr_retire_updating_rn_pipe_in(valid_instr_rn_retire_alu_pipe1), 
    .instr_invalid_pipe_but_updating_rn_in(alu_pipe1_invalid_but_updating_rn), 
    .instr_invalid_pipe_updating_rn_but_retiring_frm_other_pipe_in(
	 alu_pipe1_rn_invalid_but_retiring_in_other_pipe), 
    .valid_instr_retire_updating_cpsr_pipe_in(valid_instr_cpsr_retire_alu_pipe1), 
    .instr_invalid_pipe_but_updating_cpsr_in(alu_pipe1_invalid_but_updating_cpsr), 
    .instr_invalid_pipe_updating_cpsr_but_retiring_frm_other_pipe_in(
	 alu_pipe1_cpsr_invalid_but_retiring_in_other_pipe), 
	 .or_valid_instr_above_tag_updating_same_rd_pipe_in(or_valid_instr_above_tag_same_rd_alu_pipe1),
	 .or_valid_instr_above_tag_updating_same_rn_pipe_in(or_valid_instr_above_tag_same_rn_alu_pipe1),
	 .or_valid_instr_above_tag_updating_cpsr_pipe_in(or_valid_instr_above_tag_same_cpsr_alu_pipe1),
	 .tag_retire_en_pipe_out(tag_retire_en_alu_pipe1_out)
    );
/******ALU PIPE1 ENDS******/


/******ALU PIPE2 STARTS******/
tag_retire_en_generation_other_than_load_store tag_retire_en_alu_pipe2_generation (
    .valid_instr_retire_updating_rd_pipe_in(valid_instr_rd_retire_alu_pipe2), 
    .instr_invalid_pipe_but_updating_rd_in(alu_pipe2_invalid_but_updating_rd), 
    .instr_invalid_pipe_updating_rd_but_retiring_frm_other_pipe_in(
	 alu_pipe2_rd_invalid_but_retiring_in_other_pipe), 
    .valid_instr_retire_updating_rn_pipe_in(valid_instr_rn_retire_alu_pipe2), 
    .instr_invalid_pipe_but_updating_rn_in(alu_pipe2_invalid_but_updating_rn), 
    .instr_invalid_pipe_updating_rn_but_retiring_frm_other_pipe_in(
	 alu_pipe2_rn_invalid_but_retiring_in_other_pipe), 
    .valid_instr_retire_updating_cpsr_pipe_in(valid_instr_cpsr_retire_alu_pipe2), 
    .instr_invalid_pipe_but_updating_cpsr_in(alu_pipe2_invalid_but_updating_cpsr), 
    .instr_invalid_pipe_updating_cpsr_but_retiring_frm_other_pipe_in(
	 alu_pipe2_cpsr_invalid_but_retiring_in_other_pipe), 
	 .or_valid_instr_above_tag_updating_same_rd_pipe_in(or_valid_instr_above_tag_same_rd_alu_pipe2),
	 .or_valid_instr_above_tag_updating_same_rn_pipe_in(or_valid_instr_above_tag_same_rn_alu_pipe2),
	 .or_valid_instr_above_tag_updating_cpsr_pipe_in(or_valid_instr_above_tag_same_cpsr_alu_pipe2),
    .tag_retire_en_pipe_out(tag_retire_en_alu_pipe2_out)
    );
/******ALU PIPE2 ENDS******/


/******LOAD STORE PIPE3 STARTS******/
tag_retire_en_generation tag_retire_en_ls_pipe3_generation (
    .pipe_ls_mult_instr_in(decoded_ls_pipe3_ls_mult_instr), 
    .valid_instr_retire_updating_rd_pipe_in(valid_instr_rd_retire_ls_pipe3), 
    .instr_invalid_pipe_but_updating_rd_in(ls_pipe3_invalid_but_updating_rd), 
    .instr_invalid_pipe_updating_rd_but_retiring_frm_other_pipe_in(
	 ls_pipe3_rd_invalid_but_retiring_in_other_pipe), 
    .valid_instr_retire_updating_rn_pipe_in(valid_instr_rn_retire_ls_pipe3), 
    .instr_invalid_pipe_but_updating_rn_in(ls_pipe3_invalid_but_updating_rn), 
    .instr_invalid_pipe_updating_rn_but_retiring_frm_other_pipe_in(
	 ls_pipe3_rn_invalid_but_retiring_in_other_pipe), 
    .valid_instr_retire_updating_cpsr_pipe_in(valid_instr_cpsr_retire_ls_pipe3), 
    .instr_invalid_pipe_but_updating_cpsr_in(ls_pipe3_invalid_but_updating_cpsr), 
    .instr_invalid_pipe_updating_cpsr_but_retiring_frm_other_pipe_in(
	 ls_pipe3_cpsr_invalid_but_retiring_in_other_pipe), 
	 .or_valid_instr_above_tag_updating_same_rd_pipe_in(or_valid_instr_above_tag_same_rd_ls_pipe3),
	 .or_valid_instr_above_tag_updating_same_rn_pipe_in(or_valid_instr_above_tag_same_rn_ls_pipe3),
	 .or_valid_instr_above_tag_updating_cpsr_pipe_in(or_valid_instr_above_tag_same_cpsr_ls_pipe3),
    .tag_retire_en_pipe_out(tag_retire_en_ls_pipe3_out)
    );
/******LOAD STORE PIPE3 STARTS******/


/******BRANCH PIPE4 STARTS******/
tag_retire_en_generation_other_than_load_store tag_retire_en_branch_pipe4_generation (
    .valid_instr_retire_updating_rd_pipe_in(valid_instr_rd_retire_branch_pipe4), 
    .instr_invalid_pipe_but_updating_rd_in(branch_pipe4_invalid_but_updating_rd), 
    .instr_invalid_pipe_updating_rd_but_retiring_frm_other_pipe_in(
	 branch_pipe4_rd_invalid_but_retiring_in_other_pipe), 
    .valid_instr_retire_updating_rn_pipe_in(valid_instr_rn_retire_branch_pipe4), 
    .instr_invalid_pipe_but_updating_rn_in(branch_pipe4_invalid_but_updating_rn), 
    .instr_invalid_pipe_updating_rn_but_retiring_frm_other_pipe_in(
	 branch_pipe4_rn_invalid_but_retiring_in_other_pipe), 
    .valid_instr_retire_updating_cpsr_pipe_in(valid_instr_cpsr_retire_branch_pipe4), 
    .instr_invalid_pipe_but_updating_cpsr_in(branch_pipe4_invalid_but_updating_cpsr), 
    .instr_invalid_pipe_updating_cpsr_but_retiring_frm_other_pipe_in(
	 branch_pipe4_cpsr_invalid_but_retiring_in_other_pipe), 
	 .or_valid_instr_above_tag_updating_same_rd_pipe_in(or_valid_instr_above_tag_same_rd_branch_pipe4),
	 .or_valid_instr_above_tag_updating_same_rn_pipe_in(or_valid_instr_above_tag_same_rn_branch_pipe4),
	 .or_valid_instr_above_tag_updating_cpsr_pipe_in(or_valid_instr_above_tag_same_cpsr_branch_pipe4),
    .tag_retire_en_pipe_out(tag_retire_en_branch_pipe4_out)
    );
/******BRANCH PIPE4 STARTS******/


genvar m;
generate
for(m=`RB_SIZE-1;m>=0;m=m-1)
begin	:	grp_mod_reg_addr	
	
	register_with_reset #`LOG_MOD_REG_ADDR_SIZE reg_modified_addr_rd (
		 .data_in(data_to_mod_reg_addr_rd[m]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(valid_retire_rd[m]), 
		 .data_out(mod_reg_addr_rd[m])
		 );
	
	register_with_reset #`LOG_MOD_REG_ADDR_SIZE reg_modified_addr_rn (
		 .data_in(data_to_mod_reg_addr_rn[m]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(valid_retire_rn[m]), 
		 .data_out(mod_reg_addr_rn[m])
		 );
	
	register_with_reset #`MOD_REG_ADDR_SIZE reg_modified_addr_cpsr (
		 .data_in(data_to_mod_reg_addr_cpsr[m]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(valid_retire_cpsr[m]), 
		 .data_out(mod_reg_addr_cpsr[m])
		 );
		 
	assign reset_for_valid_retire_reg[m] = (retire_2_instr & (head_pointer_instr1[m] | 
	
	head_pointer_instr2[m])) | (retire_1_instr & head_pointer_instr1[m]);
	
	
	assign data_to_valid_retire_reg[m] = valid_retire[m];
	
	
	//assign en_for_valid_retire_reg = |(instr_complete_in);
	assign en_for_valid_retire_reg = valid_retire[m];
	
	
	register_with_reset #1 reg_data_to_valid_retire_reg (
		 .data_in(data_to_valid_retire_reg[m]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in | reset_for_valid_retire_reg[m]), 
		 .en_in(data_to_valid_retire_reg[m]), 
		 .data_out(valid_retire_reg[m])
		 );
end
endgenerate
/******Modified Register Address Ends******/

/******Load Store Multiple instruction data******/
assign selected_mod_reg_addr = mod_reg_addr_rd_in[`LOG_REG_ADDR_LS_PIPE3_START:

`MOD_REG_ADDR_LS_PIPE3_END];


assign data_for_en_for_ldm_data[`NO_OF_REG-1] = ldm_stm_stall_in & ~(|(en_for_ldm_data));



/******Data for enable******/
genvar n;
generate
for(n=`NO_OF_REG-2;n>=0;n=n-1)
begin	:	grp_en_for_ldm_data
	assign data_for_en_for_ldm_data[n] = en_for_ldm_data[n+1];
end
endgenerate
/******Data for enable******/

/******Enable Register and Load multiple data register******/



genvar o;
generate
for(o=`NO_OF_REG-1;o>=0;o=o-1)
begin : grp_register_for_ldm_data
		
	register_with_reset #1 reg_en_for_ldm_data (
		 .data_in(data_for_en_for_ldm_data[o]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in /*| retire_ldm*/ | en_for_ldm_data[o]), 
		 .en_in(ldm_stm_stall_in), 
		 .data_out(en_for_ldm_data[o])
		 );
	
	
	assign data_to_ldm_data_reg[o] = {ldm_stm_stall_in,selected_mod_reg_addr};
end
endgenerate


register_with_reset #(`LOG_MOD_REG_ADDR_SIZE+1) reg_for_ldm_data_first (
		 .data_in(data_to_ldm_data_reg[`NO_OF_REG-1]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in | ldm_retire_complete_instr1 | ldm_retire_complete_instr2), 
		 .en_in(data_for_en_for_ldm_data[`NO_OF_REG-1]), 
		 .data_out(ldm_data_reg[`NO_OF_REG-1])
		 );


genvar t;
generate
for(t=`NO_OF_REG-2;t>=0;t=t-1)
begin : grp_ldm_reg
	register_with_reset #(`LOG_MOD_REG_ADDR_SIZE+1) reg_for_ldm_data (
		 .data_in(data_to_ldm_data_reg[t]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in | ldm_retire_complete_instr1 | ldm_retire_complete_instr2), 
		 .en_in(data_for_en_for_ldm_data[t]), 
		 .data_out(ldm_data_reg[t])
		 );
end
endgenerate

/******Enable Register and Load multiple data register******/


/******ALU PIPE1 STARTS******/
assign invalid_rd_alu_pipe1 = mod_reg_addr_rd_in[`LOG_REG_ADDR_ALU_PIPE1_START:

`LOG_REG_ADDR_ALU_PIPE1_END];


assign invalid_rn_alu_pipe1 = mod_reg_addr_rn_in[`LOG_REG_ADDR_ALU_PIPE1_START:

`LOG_REG_ADDR_ALU_PIPE1_END];


invalid_retire_subblock_other_than_load_store_pipe invalid_instr_alu_pipe1 (
    .valid_decoded_addr_in(valid_decoded_addr_alu_pipe1), 
    .speculative_instr_RB_in(speculative_instr_RB), 
    .speculative_result_pipe_in(speculative_result_in[`ALU_PIPE1]), 
    .instr_complete_pipe_in(instr_complete_in[`ALU_PIPE1]), 
    .RD_update_instr_RB_in(RD_update_instr_RB),
	 .RN_update_instr_RB_in(RN_update_instr_RB),
	 .CPSR_update_instr_RB_in(CPSR_update_instr_RB),
	 .tag_instr_in(tag_instr_pipes_in[`ALU_PIPE1_INSTR_TAG_START:`ALU_PIPE1_INSTR_TAG_END]),
	 .comparing_rd_pipe_in(comparing_rd_alu_pipe1),
	 .comparing_rn_pipe_in(comparing_rn_alu_pipe1),
	 .comparing_cpsr_pipe_in(comparing_cpsr_alu_pipe1),
	 .head_pointer1_in(head_pointer_instr1),
	 .all_below_head_in(all_below_head),
	 .all_above_tail_in(all_above_tail),
	 .valid_instr_in(valid_instructions_in_RB),
	 .head_greater_than_tail_in(head_greater_than_tail),
	 .tag_in_other_pipes_in({tag_instr_pipes_in[`ALU_PIPE2_INSTR_TAG_START:`ALU_PIPE2_INSTR_TAG_END],
									tag_instr_pipes_in[`LS_PIPE3_INSTR_TAG_START:`LS_PIPE3_INSTR_TAG_END],
									tag_instr_pipes_in[`BRANCH_PIPE4_INSTR_TAG_START:`BRANCH_PIPE4_INSTR_TAG_END]}),
	 .instr_other_pipe1_valid_retire_in(instr_alu_pipe2_valid_retire),
	 .instr_other_pipe2_valid_retire_in(instr_ls_pipe3_valid_retire),
	 .instr_other_pipe3_valid_retire_in(instr_branch_pipe4_valid_retire),
	 .invalid_instr_RB_out(invalid_instruction_alu_pipe1), 
    .rd_update_out(alu_pipe1_invalid_but_updating_rd),
	 .rn_update_out(alu_pipe1_invalid_but_updating_rn),
	 .cpsr_update_out(alu_pipe1_invalid_but_updating_cpsr),
	 .new_tag_to_change_rd_rn_cpsr_out(tag_to_change_alu_pipe1_out),
	 .rd_invalid_but_retiring_in_other_pipe_out(alu_pipe1_rd_invalid_but_retiring_in_other_pipe),
	 .rn_invalid_but_retiring_in_other_pipe_out(alu_pipe1_rn_invalid_but_retiring_in_other_pipe),
	 .cpsr_invalid_but_retiring_in_other_pipe_out(alu_pipe1_cpsr_invalid_but_retiring_in_other_pipe),
	 .tag_change_en_pipe_out(tag_change_en_alu_pipe1_out),
	 .valid_instr_above_tag_out(valid_instr_above_tag_alu_pipe1),
	 .or_invalid_instr_above_tail_out(or_invalid_instr_above_tail_alu_pipe1),
	 .or_valid_instr_above_tag_same_rd_pipe_out(or_valid_instr_above_tag_same_rd_alu_pipe1),
	 .or_valid_instr_above_tag_same_rn_pipe_out(or_valid_instr_above_tag_same_rn_alu_pipe1),
	 .or_valid_instr_above_tag_same_cpsr_pipe_out(or_valid_instr_above_tag_same_cpsr_alu_pipe1)
    );


genvar p;
generate
for(p=`RB_SIZE-1;p>=0;p=p-1)
begin	:	grp_comparing_reg_alu_pipe1
	assign comparing_rd_alu_pipe1[p] =  (~(|(invalid_rd_alu_pipe1 ^ 
	
	mod_reg_addr_rd[p][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[p]) | 
	
	(~(|(invalid_rd_alu_pipe1 ^ mod_reg_addr_rn[p][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[p]);
	
	
	assign comparing_rn_alu_pipe1[p] =  (~(|(invalid_rn_alu_pipe1 ^ 
	
	mod_reg_addr_rn[p][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RN_update_instr_RB[p]) | 
	
	(~(|(invalid_rn_alu_pipe1 ^ mod_reg_addr_rd[p][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RD_update_instr_RB[p]);
	
	
	assign comparing_cpsr_alu_pipe1[p] =  CPSR_update_instr_RB[p];
end
endgenerate
/******ALU PIPE1 ENDS******/


/******ALU PIPE2 STARTS******/
assign invalid_rd_alu_pipe2 = mod_reg_addr_rd_in[`LOG_REG_ADDR_ALU_PIPE2_START:

`LOG_REG_ADDR_ALU_PIPE2_END];


assign invalid_rn_alu_pipe2 = mod_reg_addr_rn_in[`LOG_REG_ADDR_ALU_PIPE2_START:

`LOG_REG_ADDR_ALU_PIPE2_END];


invalid_retire_subblock_other_than_load_store_pipe invalid_instr_alu_pipe2 (
    .valid_decoded_addr_in(valid_decoded_addr_alu_pipe2), 
    .speculative_instr_RB_in(speculative_instr_RB), 
    .speculative_result_pipe_in(speculative_result_in[`ALU_PIPE2]), 
    .instr_complete_pipe_in(instr_complete_in[`ALU_PIPE2]), 
    .RD_update_instr_RB_in(RD_update_instr_RB),
	 .RN_update_instr_RB_in(RN_update_instr_RB),
	 .CPSR_update_instr_RB_in(CPSR_update_instr_RB),
	 .tag_instr_in(tag_instr_pipes_in[`ALU_PIPE2_INSTR_TAG_START:`ALU_PIPE2_INSTR_TAG_END]),
	 .comparing_rd_pipe_in(comparing_rd_alu_pipe2),
	 .comparing_rn_pipe_in(comparing_rn_alu_pipe2),
	 .comparing_cpsr_pipe_in(comparing_cpsr_alu_pipe2),
	 .head_pointer1_in(head_pointer_instr1),
	 .all_below_head_in(all_below_head),
	 .all_above_tail_in(all_above_tail),
	 .valid_instr_in(valid_instructions_in_RB),
	 .head_greater_than_tail_in(head_greater_than_tail),
	 .tag_in_other_pipes_in({tag_instr_pipes_in[`ALU_PIPE1_INSTR_TAG_START:`ALU_PIPE1_INSTR_TAG_END],
									tag_instr_pipes_in[`LS_PIPE3_INSTR_TAG_START:`LS_PIPE3_INSTR_TAG_END],
									tag_instr_pipes_in[`BRANCH_PIPE4_INSTR_TAG_START:`BRANCH_PIPE4_INSTR_TAG_END]}),
	 .instr_other_pipe1_valid_retire_in(instr_alu_pipe1_valid_retire),
	 .instr_other_pipe2_valid_retire_in(instr_ls_pipe3_valid_retire),
	 .instr_other_pipe3_valid_retire_in(instr_branch_pipe4_valid_retire),
	 .invalid_instr_RB_out(invalid_instruction_alu_pipe2), 
    .rd_update_out(alu_pipe2_invalid_but_updating_rd),
	 .rn_update_out(alu_pipe2_invalid_but_updating_rn),
	 .cpsr_update_out(alu_pipe2_invalid_but_updating_cpsr),
	 .new_tag_to_change_rd_rn_cpsr_out(tag_to_change_alu_pipe2_out),
	 .rd_invalid_but_retiring_in_other_pipe_out(alu_pipe2_rd_invalid_but_retiring_in_other_pipe),
	 .rn_invalid_but_retiring_in_other_pipe_out(alu_pipe2_rn_invalid_but_retiring_in_other_pipe),
	 .cpsr_invalid_but_retiring_in_other_pipe_out(alu_pipe2_cpsr_invalid_but_retiring_in_other_pipe),
	 .tag_change_en_pipe_out(tag_change_en_alu_pipe2_out),
	 .valid_instr_above_tag_out(valid_instr_above_tag_alu_pipe2),
	 .or_invalid_instr_above_tail_out(or_invalid_instr_above_tail_alu_pipe2),
	 .or_valid_instr_above_tag_same_rd_pipe_out(or_valid_instr_above_tag_same_rd_alu_pipe2),
	 .or_valid_instr_above_tag_same_rn_pipe_out(or_valid_instr_above_tag_same_rn_alu_pipe2),
	 .or_valid_instr_above_tag_same_cpsr_pipe_out(or_valid_instr_above_tag_same_cpsr_alu_pipe2)
    );


genvar q;
generate
for(q=`RB_SIZE-1;q>=0;q=q-1)
begin	:	grp_comparing_reg_alu_pipe2
	assign comparing_rd_alu_pipe2[q] =  (~(|(invalid_rd_alu_pipe2 ^ 
	
	mod_reg_addr_rd[q][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[q]) | 
	
	(~(|(invalid_rd_alu_pipe2 ^ mod_reg_addr_rn[q][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[q]);
	
	
	assign comparing_rn_alu_pipe2[q] =  (~(|(invalid_rn_alu_pipe2 ^ 
	
	mod_reg_addr_rn[q][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RN_update_instr_RB[q]) | 
	
	(~(|(invalid_rn_alu_pipe2 ^ mod_reg_addr_rd[q][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RD_update_instr_RB[q]);
	
	
	assign comparing_cpsr_alu_pipe2[q] =  CPSR_update_instr_RB[q];
end
endgenerate
/******ALU PIPE2 ENDS******/


/******LS PIPE3 STARTS******/
assign invalid_rd_ls_pipe3 = mod_reg_addr_rd_in[`LOG_REG_ADDR_LS_PIPE3_START:

`LOG_REG_ADDR_LS_PIPE3_END];


assign invalid_rn_ls_pipe3 = mod_reg_addr_rn_in[`LOG_REG_ADDR_LS_PIPE3_START:

`LOG_REG_ADDR_LS_PIPE3_END];


invalid_retire_sub_block invalid_instr_ls_pipe3 (
    .valid_decoded_addr_in(valid_decoded_addr_ls_pipe3), 
    .speculative_instr_RB_in(speculative_instr_RB), 
    .speculative_result_pipe_in(speculative_result_in[`LS_PIPE3]), 
    .instr_complete_pipe_in(instr_complete_in[`LS_PIPE3]), 
    .load_multiple_instr_RB_in(load_multiple_instr_RB), 
	 .RD_update_instr_RB_in(RD_update_instr_RB),
	 .RN_update_instr_RB_in(RN_update_instr_RB),
	 .CPSR_update_instr_RB_in(CPSR_update_instr_RB),
	 .tag_instr_in(tag_instr_pipes_in[`LS_PIPE3_INSTR_TAG_START:`LS_PIPE3_INSTR_TAG_END]),
	 .comparing_rd_pipe_in(comparing_rd_ls_pipe3),
	 .comparing_rn_pipe_in(comparing_rn_ls_pipe3),
	 .comparing_cpsr_pipe_in(comparing_cpsr_ls_pipe3),
	 .head_pointer1_in(head_pointer_instr1),
	 .all_below_head_in(all_below_head),
	 .all_above_tail_in(all_above_tail),
	 .valid_instr_in(valid_instructions_in_RB),
	 .head_greater_than_tail_in(head_greater_than_tail),
	 .tag_in_other_pipes_in({tag_instr_pipes_in[`ALU_PIPE1_INSTR_TAG_START:`ALU_PIPE1_INSTR_TAG_END],
									tag_instr_pipes_in[`ALU_PIPE2_INSTR_TAG_START:`ALU_PIPE2_INSTR_TAG_END],
									tag_instr_pipes_in[`BRANCH_PIPE4_INSTR_TAG_START:`BRANCH_PIPE4_INSTR_TAG_END]}),
	 .instr_other_pipe1_valid_retire_in(instr_alu_pipe1_valid_retire),
	 .instr_other_pipe2_valid_retire_in(instr_alu_pipe2_valid_retire),
	 .instr_other_pipe3_valid_retire_in(instr_branch_pipe4_valid_retire),
	 .invalid_instr_RB_out(invalid_instruction_ls_pipe3), 
    .invalid_ldm_instr_RB_out(invalid_ldm_instruction_ls_pipe3),
	 .rd_update_out(ls_pipe3_invalid_but_updating_rd),
	 .rn_update_out(ls_pipe3_invalid_but_updating_rn),
	 .cpsr_update_out(ls_pipe3_invalid_but_updating_cpsr),
	 .new_tag_to_change_rd_rn_cpsr_out(tag_to_change_ls_pipe3_out),
	 .rd_invalid_but_retiring_in_other_pipe_out(ls_pipe3_rd_invalid_but_retiring_in_other_pipe),
	 .rn_invalid_but_retiring_in_other_pipe_out(ls_pipe3_rn_invalid_but_retiring_in_other_pipe),
	 .cpsr_invalid_but_retiring_in_other_pipe_out(ls_pipe3_cpsr_invalid_but_retiring_in_other_pipe),
	 .tag_change_en_pipe_out(tag_change_en_ls_pipe3_out),
	 .valid_instr_above_tag_out(valid_instr_above_tag_ls_pipe3),
	 .or_invalid_instr_above_tail_out(or_invalid_instr_above_tail_ls_pipe3),
	 .or_valid_instr_above_tag_same_rd_pipe_out(or_valid_instr_above_tag_same_rd_ls_pipe3),
	 .or_valid_instr_above_tag_same_rn_pipe_out(or_valid_instr_above_tag_same_rn_ls_pipe3),
	 .or_valid_instr_above_tag_same_cpsr_pipe_out(or_valid_instr_above_tag_same_cpsr_ls_pipe3)
    );


genvar r;
generate
for(r=`RB_SIZE-1;r>=0;r=r-1)
begin	:	grp_comparing_reg_ls_pipe3
	assign comparing_rd_ls_pipe3[r] =  (~(|(invalid_rd_ls_pipe3 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(invalid_rd_ls_pipe3 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_rn_ls_pipe3[r] =  (~(|(invalid_rn_ls_pipe3 ^ 
	
	mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RN_update_instr_RB[r]) | 
	
	(~(|(invalid_rn_ls_pipe3 ^ mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RD_update_instr_RB[r]);
	
	
	assign comparing_cpsr_ls_pipe3[r] =  CPSR_update_instr_RB[r];
	
	
	assign comparing_r0_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd0 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd0 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r1_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd1 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd1 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r2_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd2 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd2 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r3_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd3 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd3 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r4_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd4 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd4 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r5_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd5 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd5 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r6_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd6 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd6 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r7_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd7 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd7 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r8_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd8 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd8 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r9_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd9 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd9 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r10_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd10 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd10 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r11_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd11 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd11 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r12_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd12 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd12 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r13_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd13 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd13 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r14_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd14 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd14 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
	
	
	assign comparing_r15_ls_pipe3[r] =  (~(|(`LOG_ADDRESS_SIZE'd15 ^ 
	
	mod_reg_addr_rd[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RD_update_instr_RB[r]) | 
	
	(~(|(`LOG_ADDRESS_SIZE'd15 ^ mod_reg_addr_rn[r][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[r]);
end
endgenerate


/******Tag change in case of load multiple starts******/
ldm_updating_registers ldm_invalid_but_updating_registers_ls_pipe3 (
    .r0_in(ldm_data_reg[15][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r1_in(ldm_data_reg[14][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r2_in(ldm_data_reg[13][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r3_in(ldm_data_reg[12][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r4_in(ldm_data_reg[11][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r5_in(ldm_data_reg[10][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r6_in(ldm_data_reg[9][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r7_in(ldm_data_reg[8][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r8_in(ldm_data_reg[7][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r9_in(ldm_data_reg[6][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r10_in(ldm_data_reg[5][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r11_in(ldm_data_reg[4][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r12_in(ldm_data_reg[3][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r13_in(ldm_data_reg[2][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r14_in(ldm_data_reg[1][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .r15_in(ldm_data_reg[0][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END]), 
    .ldm_word_frm_RB_23_in(ldm_word_frm_RB[23]), 
    .ldm_word_frm_RB_22_in(ldm_word_frm_RB[22]), 
    .ldm_word_frm_RB_21_in(ldm_word_frm_RB[21]), 
    .ldm_word_frm_RB_20_in(ldm_word_frm_RB[20]), 
    .ldm_word_frm_RB_19_in(ldm_word_frm_RB[19]), 
    .ldm_word_frm_RB_18_in(ldm_word_frm_RB[18]), 
    .ldm_word_frm_RB_17_in(ldm_word_frm_RB[17]), 
    .ldm_word_frm_RB_16_in(ldm_word_frm_RB[16]), 
    .ldm_word_frm_RB_15_in(ldm_word_frm_RB[15]), 
    .ldm_word_frm_RB_14_in(ldm_word_frm_RB[14]), 
    .ldm_word_frm_RB_13_in(ldm_word_frm_RB[13]), 
    .ldm_word_frm_RB_12_in(ldm_word_frm_RB[12]), 
    .ldm_word_frm_RB_11_in(ldm_word_frm_RB[11]), 
    .ldm_word_frm_RB_10_in(ldm_word_frm_RB[10]), 
    .ldm_word_frm_RB_9_in(ldm_word_frm_RB[9]), 
    .ldm_word_frm_RB_8_in(ldm_word_frm_RB[8]), 
    .ldm_word_frm_RB_7_in(ldm_word_frm_RB[7]), 
    .ldm_word_frm_RB_6_in(ldm_word_frm_RB[6]), 
    .ldm_word_frm_RB_5_in(ldm_word_frm_RB[5]), 
    .ldm_word_frm_RB_4_in(ldm_word_frm_RB[4]), 
    .ldm_word_frm_RB_3_in(ldm_word_frm_RB[3]), 
    .ldm_word_frm_RB_2_in(ldm_word_frm_RB[2]), 
    .ldm_word_frm_RB_1_in(ldm_word_frm_RB[1]), 
    .ldm_word_frm_RB_0_in(ldm_word_frm_RB[0]), 
    .tag_instr_in(tag_instr_pipes_in[`LS_PIPE3_INSTR_TAG_START:`LS_PIPE3_INSTR_TAG_END]), 
    .invalid_ldm_instruction_in(invalid_ldm_instruction_ls_pipe3),
	 .valid_instr_above_tag_pipe_in(valid_instr_above_tag_ls_pipe3),
	 .or_invalid_instr_above_tail_pipe_in(or_invalid_instr_above_tail_pipe_in),
	 .instr_updating_r0_pipe_in(comparing_r0_ls_pipe3),
	 .instr_updating_r1_pipe_in(comparing_r1_ls_pipe3),
	 .instr_updating_r2_pipe_in(comparing_r2_ls_pipe3),
	 .instr_updating_r3_pipe_in(comparing_r3_ls_pipe3),
	 .instr_updating_r4_pipe_in(comparing_r4_ls_pipe3),
	 .instr_updating_r5_pipe_in(comparing_r5_ls_pipe3),
	 .instr_updating_r6_pipe_in(comparing_r6_ls_pipe3),
	 .instr_updating_r7_pipe_in(comparing_r7_ls_pipe3),
	 .instr_updating_r8_pipe_in(comparing_r8_ls_pipe3),
	 .instr_updating_r9_pipe_in(comparing_r9_ls_pipe3),
	 .instr_updating_r10_pipe_in(comparing_r10_ls_pipe3),
	 .instr_updating_r11_pipe_in(comparing_r11_ls_pipe3),
	 .instr_updating_r12_pipe_in(comparing_r12_ls_pipe3),
	 .instr_updating_r13_pipe_in(comparing_r13_ls_pipe3),
	 .instr_updating_r14_pipe_in(comparing_r14_ls_pipe3),
	 .instr_updating_r15_pipe_in(comparing_r15_ls_pipe3),
	 .all_above_tail_in(all_above_tail),
	 .all_below_head_in(all_below_head),
	 .head_greater_than_tail_in(head_greater_than_tail),
	 .tag_in_other_pipes_in({tag_instr_pipes_in[`ALU_PIPE1_INSTR_TAG_START:`ALU_PIPE1_INSTR_TAG_END],
									tag_instr_pipes_in[`ALU_PIPE2_INSTR_TAG_START:`ALU_PIPE2_INSTR_TAG_END],
									tag_instr_pipes_in[`BRANCH_PIPE4_INSTR_TAG_START:`BRANCH_PIPE4_INSTR_TAG_END]}),
	 .instr_other_pipe1_valid_retire_in(instr_alu_pipe1_valid_retire),
	 .instr_other_pipe2_valid_retire_in(instr_alu_pipe2_valid_retire),	
	 .instr_other_pipe3_valid_retire_in(instr_branch_pipe4_valid_retire),
	 .new_tag_to_change_all_reg_for_ldm_invalid_out(tag_to_change_in_case_of_ldm_ls_pipe3_out),
	 .new_tag_change_en_for_ldm_invalid_out(tag_change_en_in_case_of_ldm_ls_pipe3_out),
	 .tag_retire_en_for_ldm_invalid_out(tag_retire_en_in_case_of_ldm_ls_pipe3_out)
    );	
/******Tag change in case of load multiple starts******/
/******LS PIPE3 ENDS******/


/******BRANCH PIPE4 STARTS******/
assign invalid_rd_branch_pipe4 = mod_reg_addr_rd_in[`LOG_REG_ADDR_BRANCH_PIPE4_START:

`LOG_REG_ADDR_BRANCH_PIPE4_END];


assign invalid_rn_branch_pipe4 = mod_reg_addr_rn_in[`LOG_REG_ADDR_BRANCH_PIPE4_START:

`LOG_REG_ADDR_BRANCH_PIPE4_END];


invalid_retire_subblock_other_than_load_store_pipe invalid_instr_branch_pipe4 (
    .valid_decoded_addr_in(valid_decoded_addr_branch_pipe4), 
    .speculative_instr_RB_in(speculative_instr_RB), 
    .speculative_result_pipe_in(speculative_result_in[`BRANCH_PIPE4]), 
    .instr_complete_pipe_in(instr_complete_in[`BRANCH_PIPE4]), 
    .RD_update_instr_RB_in(RD_update_instr_RB),
	 .RN_update_instr_RB_in(RN_update_instr_RB),
	 .CPSR_update_instr_RB_in(CPSR_update_instr_RB),
	 .tag_instr_in(tag_instr_pipes_in[`BRANCH_PIPE4_INSTR_TAG_START:`BRANCH_PIPE4_INSTR_TAG_END]),
	 .comparing_rd_pipe_in(comparing_rd_branch_pipe4),
	 .comparing_rn_pipe_in(comparing_rn_branch_pipe4),
	 .comparing_cpsr_pipe_in(comparing_cpsr_branch_pipe4),
	 .head_pointer1_in(head_pointer_instr1),
	 .all_below_head_in(all_below_head),
	 .all_above_tail_in(all_above_tail),
	 .valid_instr_in(valid_instructions_in_RB),
	 .head_greater_than_tail_in(head_greater_than_tail),
	 .tag_in_other_pipes_in({tag_instr_pipes_in[`ALU_PIPE1_INSTR_TAG_START:`ALU_PIPE1_INSTR_TAG_END],
									tag_instr_pipes_in[`ALU_PIPE2_INSTR_TAG_START:`ALU_PIPE2_INSTR_TAG_END],
									tag_instr_pipes_in[`LS_PIPE3_INSTR_TAG_START:`LS_PIPE3_INSTR_TAG_END]}),
	 .instr_other_pipe1_valid_retire_in(instr_alu_pipe1_valid_retire),
	 .instr_other_pipe2_valid_retire_in(instr_alu_pipe2_valid_retire),
	 .instr_other_pipe3_valid_retire_in(instr_ls_pipe3_valid_retire),
	 .invalid_instr_RB_out(invalid_instruction_branch_pipe4), 
    .rd_update_out(branch_pipe4_invalid_but_updating_rd),
	 .rn_update_out(branch_pipe4_invalid_but_updating_rn),
	 .cpsr_update_out(branch_pipe4_invalid_but_updating_cpsr),
	 .new_tag_to_change_rd_rn_cpsr_out(tag_to_change_branch_pipe4_out),
	 .rd_invalid_but_retiring_in_other_pipe_out(branch_pipe4_rd_invalid_but_retiring_in_other_pipe),
	 .rn_invalid_but_retiring_in_other_pipe_out(branch_pipe4_rn_invalid_but_retiring_in_other_pipe),
	 .cpsr_invalid_but_retiring_in_other_pipe_out(branch_pipe4_cpsr_invalid_but_retiring_in_other_pipe),
	 .tag_change_en_pipe_out(tag_change_en_branch_pipe4_out),
	 .valid_instr_above_tag_out(valid_instr_above_tag_branch_pipe4),
	 .or_invalid_instr_above_tail_out(or_invalid_instr_above_tail_branch_pipe4),
	 .or_valid_instr_above_tag_same_rd_pipe_out(or_valid_instr_above_tag_same_rd_branch_pipe4),
	 .or_valid_instr_above_tag_same_rn_pipe_out(or_valid_instr_above_tag_same_rn_branch_pipe4),
	 .or_valid_instr_above_tag_same_cpsr_pipe_out(or_valid_instr_above_tag_same_cpsr_branch_pipe4)
    );


genvar s;
generate
for(s=`RB_SIZE-1;s>=0;s=s-1)
begin	:	grp_comparing_reg_branch_pipe4
	assign comparing_rd_branch_pipe4[s] =  (~(|(invalid_rd_branch_pipe4 ^ 
	
	mod_reg_addr_rd[s][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 	RD_update_instr_RB[s]) | 
	
	(~(|(invalid_rd_branch_pipe4 ^ mod_reg_addr_rn[s][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RN_update_instr_RB[s]);
	
	
	assign comparing_rn_branch_pipe4[s] = (~(|(invalid_rn_branch_pipe4 ^ 
	
	mod_reg_addr_rn[s][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & RN_update_instr_RB[s]) | 
	
	(~(|(invalid_rn_branch_pipe4 ^ mod_reg_addr_rd[s][`LOG_REG_ADDR_START:`LOG_REG_ADDR_END])) & 
	
	RD_update_instr_RB[s]);
	
	
	assign comparing_cpsr_branch_pipe4[s] = CPSR_update_instr_RB[s];
end
endgenerate
/******BRANCH PIPE4 ENDS******/
 


/******REG_VALUE_TO_ISSUE_TO_PIPES_BEGINS******/

/******ALU_PIPE1 STARTS******/
instr_issue_pipe_reg instr_issue_alu_pipe1_rn (
    .tag_to_issue_pipe_rn_in(tag_to_issue_plus_alu_pipe1_rn_log_address_in), 
	 .tag_to_issue_pipe_rm_in(tag_to_issue_plus_alu_pipe1_rm_log_address_in),
	 .tag_to_issue_pipe_rs_in(tag_to_issue_plus_alu_pipe1_rs_log_address_in),
	 .tag_to_issue_pipe_cpsr_in(tag_to_issue_alu_pipe1_cpsr_in),
    .valid_instructions_RB_in(valid_instructions_in_RB), 
    .load_multiple_instr_RB_in(load_multiple_instr_RB), 
    .ldm_data_reg_15_in(ldm_data_reg[15][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_14_in(ldm_data_reg[14][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_13_in(ldm_data_reg[13][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_12_in(ldm_data_reg[12][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_11_in(ldm_data_reg[11][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_10_in(ldm_data_reg[10][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_9_in(ldm_data_reg[9][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_8_in(ldm_data_reg[8][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_7_in(ldm_data_reg[7][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_6_in(ldm_data_reg[6][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_5_in(ldm_data_reg[5][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_4_in(ldm_data_reg[4][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_3_in(ldm_data_reg[3][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_2_in(ldm_data_reg[2][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_1_in(ldm_data_reg[1][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_0_in(ldm_data_reg[0][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .mod_reg_addr_rd_23_in(mod_reg_addr_rd[23]), 
    .mod_reg_addr_rd_22_in(mod_reg_addr_rd[22]), 
    .mod_reg_addr_rd_21_in(mod_reg_addr_rd[21]), 
    .mod_reg_addr_rd_20_in(mod_reg_addr_rd[20]), 
    .mod_reg_addr_rd_19_in(mod_reg_addr_rd[19]), 
    .mod_reg_addr_rd_18_in(mod_reg_addr_rd[18]), 
    .mod_reg_addr_rd_17_in(mod_reg_addr_rd[17]), 
    .mod_reg_addr_rd_16_in(mod_reg_addr_rd[16]), 
    .mod_reg_addr_rd_15_in(mod_reg_addr_rd[15]), 
    .mod_reg_addr_rd_14_in(mod_reg_addr_rd[14]), 
    .mod_reg_addr_rd_13_in(mod_reg_addr_rd[13]), 
    .mod_reg_addr_rd_12_in(mod_reg_addr_rd[12]), 
    .mod_reg_addr_rd_11_in(mod_reg_addr_rd[11]), 
    .mod_reg_addr_rd_10_in(mod_reg_addr_rd[10]), 
    .mod_reg_addr_rd_9_in(mod_reg_addr_rd[9]), 
    .mod_reg_addr_rd_8_in(mod_reg_addr_rd[8]), 
    .mod_reg_addr_rd_7_in(mod_reg_addr_rd[7]), 
    .mod_reg_addr_rd_6_in(mod_reg_addr_rd[6]), 
    .mod_reg_addr_rd_5_in(mod_reg_addr_rd[5]), 
    .mod_reg_addr_rd_4_in(mod_reg_addr_rd[4]), 
    .mod_reg_addr_rd_3_in(mod_reg_addr_rd[3]), 
    .mod_reg_addr_rd_2_in(mod_reg_addr_rd[2]), 
    .mod_reg_addr_rd_1_in(mod_reg_addr_rd[1]), 
    .mod_reg_addr_rd_0_in(mod_reg_addr_rd[0]), 
    .mod_reg_addr_rn_23_in(mod_reg_addr_rn[23]), 
    .mod_reg_addr_rn_22_in(mod_reg_addr_rn[22]), 
    .mod_reg_addr_rn_21_in(mod_reg_addr_rn[21]), 
    .mod_reg_addr_rn_20_in(mod_reg_addr_rn[20]), 
    .mod_reg_addr_rn_19_in(mod_reg_addr_rn[19]), 
    .mod_reg_addr_rn_18_in(mod_reg_addr_rn[18]), 
    .mod_reg_addr_rn_17_in(mod_reg_addr_rn[17]), 
    .mod_reg_addr_rn_16_in(mod_reg_addr_rn[16]), 
    .mod_reg_addr_rn_15_in(mod_reg_addr_rn[15]), 
    .mod_reg_addr_rn_14_in(mod_reg_addr_rn[14]), 
    .mod_reg_addr_rn_13_in(mod_reg_addr_rn[13]), 
    .mod_reg_addr_rn_12_in(mod_reg_addr_rn[12]), 
    .mod_reg_addr_rn_11_in(mod_reg_addr_rn[11]), 
    .mod_reg_addr_rn_10_in(mod_reg_addr_rn[10]), 
    .mod_reg_addr_rn_9_in(mod_reg_addr_rn[9]), 
    .mod_reg_addr_rn_8_in(mod_reg_addr_rn[8]), 
    .mod_reg_addr_rn_7_in(mod_reg_addr_rn[7]), 
    .mod_reg_addr_rn_6_in(mod_reg_addr_rn[6]), 
    .mod_reg_addr_rn_5_in(mod_reg_addr_rn[5]), 
    .mod_reg_addr_rn_4_in(mod_reg_addr_rn[4]), 
    .mod_reg_addr_rn_3_in(mod_reg_addr_rn[3]), 
    .mod_reg_addr_rn_2_in(mod_reg_addr_rn[2]), 
    .mod_reg_addr_rn_1_in(mod_reg_addr_rn[1]), 
    .mod_reg_addr_rn_0_in(mod_reg_addr_rn[0]), 
	 .mod_reg_addr_cpsr_23_in(mod_reg_addr_cpsr[23]), 
    .mod_reg_addr_cpsr_22_in(mod_reg_addr_cpsr[22]), 
    .mod_reg_addr_cpsr_21_in(mod_reg_addr_cpsr[21]), 
    .mod_reg_addr_cpsr_20_in(mod_reg_addr_cpsr[20]), 
    .mod_reg_addr_cpsr_19_in(mod_reg_addr_cpsr[19]), 
    .mod_reg_addr_cpsr_18_in(mod_reg_addr_cpsr[18]), 
    .mod_reg_addr_cpsr_17_in(mod_reg_addr_cpsr[17]), 
    .mod_reg_addr_cpsr_16_in(mod_reg_addr_cpsr[16]), 
    .mod_reg_addr_cpsr_15_in(mod_reg_addr_cpsr[15]), 
    .mod_reg_addr_cpsr_14_in(mod_reg_addr_cpsr[14]), 
    .mod_reg_addr_cpsr_13_in(mod_reg_addr_cpsr[13]), 
    .mod_reg_addr_cpsr_12_in(mod_reg_addr_cpsr[12]), 
    .mod_reg_addr_cpsr_11_in(mod_reg_addr_cpsr[11]), 
    .mod_reg_addr_cpsr_10_in(mod_reg_addr_cpsr[10]), 
    .mod_reg_addr_cpsr_9_in(mod_reg_addr_cpsr[9]), 
    .mod_reg_addr_cpsr_8_in(mod_reg_addr_cpsr[8]), 
    .mod_reg_addr_cpsr_7_in(mod_reg_addr_cpsr[7]), 
    .mod_reg_addr_cpsr_6_in(mod_reg_addr_cpsr[6]), 
    .mod_reg_addr_cpsr_5_in(mod_reg_addr_cpsr[5]), 
    .mod_reg_addr_cpsr_4_in(mod_reg_addr_cpsr[4]), 
    .mod_reg_addr_cpsr_3_in(mod_reg_addr_cpsr[3]), 
    .mod_reg_addr_cpsr_2_in(mod_reg_addr_cpsr[2]), 
    .mod_reg_addr_cpsr_1_in(mod_reg_addr_cpsr[1]), 
    .mod_reg_addr_cpsr_0_in(mod_reg_addr_cpsr[0]),
    .data_frm_RAT_pipe_rn_in(data_frm_RAT_alu_pipe1_rn_in),
	 .data_frm_RAT_pipe_rm_in(data_frm_RAT_alu_pipe1_rm_in),
	 .data_frm_RAT_pipe_rs_in(data_frm_RAT_alu_pipe1_rs_in),
	 .data_frm_RAT_pipe_cpsr_in(data_frm_RAT_alu_pipe1_cpsr_in),
    .reg_data_to_issue_pipe_rn_out(reg_data_to_issue_alu_pipe1_rn_out),
	 .reg_data_to_issue_pipe_rm_out(reg_data_to_issue_alu_pipe1_rm_out),
	 .reg_data_to_issue_pipe_rs_out(reg_data_to_issue_alu_pipe1_rs_out),
	 .reg_data_to_issue_pipe_cpsr_out(reg_data_to_issue_alu_pipe1_cpsr_out)
    );
/******ALU_PIPE1 ENDS******/


/******ALU_PIPE2 STARTS******/
instr_issue_pipe_reg instr_issue_alu_pipe2_rn (
    .tag_to_issue_pipe_rn_in(tag_to_issue_plus_alu_pipe2_rn_log_address_in), 
	 .tag_to_issue_pipe_rm_in(tag_to_issue_plus_alu_pipe2_rm_log_address_in),
	 .tag_to_issue_pipe_rs_in(tag_to_issue_plus_alu_pipe2_rs_log_address_in),
	 .tag_to_issue_pipe_cpsr_in(tag_to_issue_alu_pipe2_cpsr_in),
    .valid_instructions_RB_in(valid_instructions_in_RB), 
    .load_multiple_instr_RB_in(load_multiple_instr_RB), 
    .ldm_data_reg_15_in(ldm_data_reg[15][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_14_in(ldm_data_reg[14][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_13_in(ldm_data_reg[13][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_12_in(ldm_data_reg[12][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_11_in(ldm_data_reg[11][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_10_in(ldm_data_reg[10][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_9_in(ldm_data_reg[9][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_8_in(ldm_data_reg[8][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_7_in(ldm_data_reg[7][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_6_in(ldm_data_reg[6][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_5_in(ldm_data_reg[5][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_4_in(ldm_data_reg[4][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_3_in(ldm_data_reg[3][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_2_in(ldm_data_reg[2][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_1_in(ldm_data_reg[1][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_0_in(ldm_data_reg[0][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .mod_reg_addr_rd_23_in(mod_reg_addr_rd[23]), 
    .mod_reg_addr_rd_22_in(mod_reg_addr_rd[22]), 
    .mod_reg_addr_rd_21_in(mod_reg_addr_rd[21]), 
    .mod_reg_addr_rd_20_in(mod_reg_addr_rd[20]), 
    .mod_reg_addr_rd_19_in(mod_reg_addr_rd[19]), 
    .mod_reg_addr_rd_18_in(mod_reg_addr_rd[18]), 
    .mod_reg_addr_rd_17_in(mod_reg_addr_rd[17]), 
    .mod_reg_addr_rd_16_in(mod_reg_addr_rd[16]), 
    .mod_reg_addr_rd_15_in(mod_reg_addr_rd[15]), 
    .mod_reg_addr_rd_14_in(mod_reg_addr_rd[14]), 
    .mod_reg_addr_rd_13_in(mod_reg_addr_rd[13]), 
    .mod_reg_addr_rd_12_in(mod_reg_addr_rd[12]), 
    .mod_reg_addr_rd_11_in(mod_reg_addr_rd[11]), 
    .mod_reg_addr_rd_10_in(mod_reg_addr_rd[10]), 
    .mod_reg_addr_rd_9_in(mod_reg_addr_rd[9]), 
    .mod_reg_addr_rd_8_in(mod_reg_addr_rd[8]), 
    .mod_reg_addr_rd_7_in(mod_reg_addr_rd[7]), 
    .mod_reg_addr_rd_6_in(mod_reg_addr_rd[6]), 
    .mod_reg_addr_rd_5_in(mod_reg_addr_rd[5]), 
    .mod_reg_addr_rd_4_in(mod_reg_addr_rd[4]), 
    .mod_reg_addr_rd_3_in(mod_reg_addr_rd[3]), 
    .mod_reg_addr_rd_2_in(mod_reg_addr_rd[2]), 
    .mod_reg_addr_rd_1_in(mod_reg_addr_rd[1]), 
    .mod_reg_addr_rd_0_in(mod_reg_addr_rd[0]), 
    .mod_reg_addr_rn_23_in(mod_reg_addr_rn[23]), 
    .mod_reg_addr_rn_22_in(mod_reg_addr_rn[22]), 
    .mod_reg_addr_rn_21_in(mod_reg_addr_rn[21]), 
    .mod_reg_addr_rn_20_in(mod_reg_addr_rn[20]), 
    .mod_reg_addr_rn_19_in(mod_reg_addr_rn[19]), 
    .mod_reg_addr_rn_18_in(mod_reg_addr_rn[18]), 
    .mod_reg_addr_rn_17_in(mod_reg_addr_rn[17]), 
    .mod_reg_addr_rn_16_in(mod_reg_addr_rn[16]), 
    .mod_reg_addr_rn_15_in(mod_reg_addr_rn[15]), 
    .mod_reg_addr_rn_14_in(mod_reg_addr_rn[14]), 
    .mod_reg_addr_rn_13_in(mod_reg_addr_rn[13]), 
    .mod_reg_addr_rn_12_in(mod_reg_addr_rn[12]), 
    .mod_reg_addr_rn_11_in(mod_reg_addr_rn[11]), 
    .mod_reg_addr_rn_10_in(mod_reg_addr_rn[10]), 
    .mod_reg_addr_rn_9_in(mod_reg_addr_rn[9]), 
    .mod_reg_addr_rn_8_in(mod_reg_addr_rn[8]), 
    .mod_reg_addr_rn_7_in(mod_reg_addr_rn[7]), 
    .mod_reg_addr_rn_6_in(mod_reg_addr_rn[6]), 
    .mod_reg_addr_rn_5_in(mod_reg_addr_rn[5]), 
    .mod_reg_addr_rn_4_in(mod_reg_addr_rn[4]), 
    .mod_reg_addr_rn_3_in(mod_reg_addr_rn[3]), 
    .mod_reg_addr_rn_2_in(mod_reg_addr_rn[2]), 
    .mod_reg_addr_rn_1_in(mod_reg_addr_rn[1]), 
    .mod_reg_addr_rn_0_in(mod_reg_addr_rn[0]), 
	 .mod_reg_addr_cpsr_23_in(mod_reg_addr_cpsr[23]), 
    .mod_reg_addr_cpsr_22_in(mod_reg_addr_cpsr[22]), 
    .mod_reg_addr_cpsr_21_in(mod_reg_addr_cpsr[21]), 
    .mod_reg_addr_cpsr_20_in(mod_reg_addr_cpsr[20]), 
    .mod_reg_addr_cpsr_19_in(mod_reg_addr_cpsr[19]), 
    .mod_reg_addr_cpsr_18_in(mod_reg_addr_cpsr[18]), 
    .mod_reg_addr_cpsr_17_in(mod_reg_addr_cpsr[17]), 
    .mod_reg_addr_cpsr_16_in(mod_reg_addr_cpsr[16]), 
    .mod_reg_addr_cpsr_15_in(mod_reg_addr_cpsr[15]), 
    .mod_reg_addr_cpsr_14_in(mod_reg_addr_cpsr[14]), 
    .mod_reg_addr_cpsr_13_in(mod_reg_addr_cpsr[13]), 
    .mod_reg_addr_cpsr_12_in(mod_reg_addr_cpsr[12]), 
    .mod_reg_addr_cpsr_11_in(mod_reg_addr_cpsr[11]), 
    .mod_reg_addr_cpsr_10_in(mod_reg_addr_cpsr[10]), 
    .mod_reg_addr_cpsr_9_in(mod_reg_addr_cpsr[9]), 
    .mod_reg_addr_cpsr_8_in(mod_reg_addr_cpsr[8]), 
    .mod_reg_addr_cpsr_7_in(mod_reg_addr_cpsr[7]), 
    .mod_reg_addr_cpsr_6_in(mod_reg_addr_cpsr[6]), 
    .mod_reg_addr_cpsr_5_in(mod_reg_addr_cpsr[5]), 
    .mod_reg_addr_cpsr_4_in(mod_reg_addr_cpsr[4]), 
    .mod_reg_addr_cpsr_3_in(mod_reg_addr_cpsr[3]), 
    .mod_reg_addr_cpsr_2_in(mod_reg_addr_cpsr[2]), 
    .mod_reg_addr_cpsr_1_in(mod_reg_addr_cpsr[1]), 
    .mod_reg_addr_cpsr_0_in(mod_reg_addr_cpsr[0]),
    .data_frm_RAT_pipe_rn_in(data_frm_RAT_alu_pipe2_rn_in),
	 .data_frm_RAT_pipe_rm_in(data_frm_RAT_alu_pipe2_rm_in),
	 .data_frm_RAT_pipe_rs_in(data_frm_RAT_alu_pipe2_rs_in),
	 .data_frm_RAT_pipe_cpsr_in(data_frm_RAT_alu_pipe2_cpsr_in),
    .reg_data_to_issue_pipe_rn_out(reg_data_to_issue_alu_pipe2_rn_out),
	 .reg_data_to_issue_pipe_rm_out(reg_data_to_issue_alu_pipe2_rm_out),
	 .reg_data_to_issue_pipe_rs_out(reg_data_to_issue_alu_pipe2_rs_out),
	 .reg_data_to_issue_pipe_cpsr_out(reg_data_to_issue_alu_pipe2_cpsr_out)
    );
/******ALU_PIPE2 ENDS******/


/******LS_PIPE3 STARTS******/
instr_issue_pipe_reg instr_issue_ls_pipe3_rn (
    .tag_to_issue_pipe_rn_in(tag_to_issue_plus_ls_pipe3_rn_log_address_in), 
	 .tag_to_issue_pipe_rm_in(tag_to_issue_plus_ls_pipe3_rm_log_address_in),
	 .tag_to_issue_pipe_rs_in(tag_to_issue_plus_ls_pipe3_rs_log_address_in),
	 .tag_to_issue_pipe_cpsr_in(tag_to_issue_ls_pipe3_cpsr_in),
    .valid_instructions_RB_in(valid_instructions_in_RB), 
    .load_multiple_instr_RB_in(load_multiple_instr_RB), 
    .ldm_data_reg_15_in(ldm_data_reg[15][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_14_in(ldm_data_reg[14][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_13_in(ldm_data_reg[13][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_12_in(ldm_data_reg[12][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_11_in(ldm_data_reg[11][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_10_in(ldm_data_reg[10][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_9_in(ldm_data_reg[9][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_8_in(ldm_data_reg[8][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_7_in(ldm_data_reg[7][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_6_in(ldm_data_reg[6][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_5_in(ldm_data_reg[5][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_4_in(ldm_data_reg[4][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_3_in(ldm_data_reg[3][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_2_in(ldm_data_reg[2][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_1_in(ldm_data_reg[1][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_0_in(ldm_data_reg[0][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .mod_reg_addr_rd_23_in(mod_reg_addr_rd[23]), 
    .mod_reg_addr_rd_22_in(mod_reg_addr_rd[22]), 
    .mod_reg_addr_rd_21_in(mod_reg_addr_rd[21]), 
    .mod_reg_addr_rd_20_in(mod_reg_addr_rd[20]), 
    .mod_reg_addr_rd_19_in(mod_reg_addr_rd[19]), 
    .mod_reg_addr_rd_18_in(mod_reg_addr_rd[18]), 
    .mod_reg_addr_rd_17_in(mod_reg_addr_rd[17]), 
    .mod_reg_addr_rd_16_in(mod_reg_addr_rd[16]), 
    .mod_reg_addr_rd_15_in(mod_reg_addr_rd[15]), 
    .mod_reg_addr_rd_14_in(mod_reg_addr_rd[14]), 
    .mod_reg_addr_rd_13_in(mod_reg_addr_rd[13]), 
    .mod_reg_addr_rd_12_in(mod_reg_addr_rd[12]), 
    .mod_reg_addr_rd_11_in(mod_reg_addr_rd[11]), 
    .mod_reg_addr_rd_10_in(mod_reg_addr_rd[10]), 
    .mod_reg_addr_rd_9_in(mod_reg_addr_rd[9]), 
    .mod_reg_addr_rd_8_in(mod_reg_addr_rd[8]), 
    .mod_reg_addr_rd_7_in(mod_reg_addr_rd[7]), 
    .mod_reg_addr_rd_6_in(mod_reg_addr_rd[6]), 
    .mod_reg_addr_rd_5_in(mod_reg_addr_rd[5]), 
    .mod_reg_addr_rd_4_in(mod_reg_addr_rd[4]), 
    .mod_reg_addr_rd_3_in(mod_reg_addr_rd[3]), 
    .mod_reg_addr_rd_2_in(mod_reg_addr_rd[2]), 
    .mod_reg_addr_rd_1_in(mod_reg_addr_rd[1]), 
    .mod_reg_addr_rd_0_in(mod_reg_addr_rd[0]), 
    .mod_reg_addr_rn_23_in(mod_reg_addr_rn[23]), 
    .mod_reg_addr_rn_22_in(mod_reg_addr_rn[22]), 
    .mod_reg_addr_rn_21_in(mod_reg_addr_rn[21]), 
    .mod_reg_addr_rn_20_in(mod_reg_addr_rn[20]), 
    .mod_reg_addr_rn_19_in(mod_reg_addr_rn[19]), 
    .mod_reg_addr_rn_18_in(mod_reg_addr_rn[18]), 
    .mod_reg_addr_rn_17_in(mod_reg_addr_rn[17]), 
    .mod_reg_addr_rn_16_in(mod_reg_addr_rn[16]), 
    .mod_reg_addr_rn_15_in(mod_reg_addr_rn[15]), 
    .mod_reg_addr_rn_14_in(mod_reg_addr_rn[14]), 
    .mod_reg_addr_rn_13_in(mod_reg_addr_rn[13]), 
    .mod_reg_addr_rn_12_in(mod_reg_addr_rn[12]), 
    .mod_reg_addr_rn_11_in(mod_reg_addr_rn[11]), 
    .mod_reg_addr_rn_10_in(mod_reg_addr_rn[10]), 
    .mod_reg_addr_rn_9_in(mod_reg_addr_rn[9]), 
    .mod_reg_addr_rn_8_in(mod_reg_addr_rn[8]), 
    .mod_reg_addr_rn_7_in(mod_reg_addr_rn[7]), 
    .mod_reg_addr_rn_6_in(mod_reg_addr_rn[6]), 
    .mod_reg_addr_rn_5_in(mod_reg_addr_rn[5]), 
    .mod_reg_addr_rn_4_in(mod_reg_addr_rn[4]), 
    .mod_reg_addr_rn_3_in(mod_reg_addr_rn[3]), 
    .mod_reg_addr_rn_2_in(mod_reg_addr_rn[2]), 
    .mod_reg_addr_rn_1_in(mod_reg_addr_rn[1]), 
    .mod_reg_addr_rn_0_in(mod_reg_addr_rn[0]), 
	 .mod_reg_addr_cpsr_23_in(mod_reg_addr_cpsr[23]), 
    .mod_reg_addr_cpsr_22_in(mod_reg_addr_cpsr[22]), 
    .mod_reg_addr_cpsr_21_in(mod_reg_addr_cpsr[21]), 
    .mod_reg_addr_cpsr_20_in(mod_reg_addr_cpsr[20]), 
    .mod_reg_addr_cpsr_19_in(mod_reg_addr_cpsr[19]), 
    .mod_reg_addr_cpsr_18_in(mod_reg_addr_cpsr[18]), 
    .mod_reg_addr_cpsr_17_in(mod_reg_addr_cpsr[17]), 
    .mod_reg_addr_cpsr_16_in(mod_reg_addr_cpsr[16]), 
    .mod_reg_addr_cpsr_15_in(mod_reg_addr_cpsr[15]), 
    .mod_reg_addr_cpsr_14_in(mod_reg_addr_cpsr[14]), 
    .mod_reg_addr_cpsr_13_in(mod_reg_addr_cpsr[13]), 
    .mod_reg_addr_cpsr_12_in(mod_reg_addr_cpsr[12]), 
    .mod_reg_addr_cpsr_11_in(mod_reg_addr_cpsr[11]), 
    .mod_reg_addr_cpsr_10_in(mod_reg_addr_cpsr[10]), 
    .mod_reg_addr_cpsr_9_in(mod_reg_addr_cpsr[9]), 
    .mod_reg_addr_cpsr_8_in(mod_reg_addr_cpsr[8]), 
    .mod_reg_addr_cpsr_7_in(mod_reg_addr_cpsr[7]), 
    .mod_reg_addr_cpsr_6_in(mod_reg_addr_cpsr[6]), 
    .mod_reg_addr_cpsr_5_in(mod_reg_addr_cpsr[5]), 
    .mod_reg_addr_cpsr_4_in(mod_reg_addr_cpsr[4]), 
    .mod_reg_addr_cpsr_3_in(mod_reg_addr_cpsr[3]), 
    .mod_reg_addr_cpsr_2_in(mod_reg_addr_cpsr[2]), 
    .mod_reg_addr_cpsr_1_in(mod_reg_addr_cpsr[1]), 
    .mod_reg_addr_cpsr_0_in(mod_reg_addr_cpsr[0]),
    .data_frm_RAT_pipe_rn_in(data_frm_RAT_ls_pipe3_rn_in),
	 .data_frm_RAT_pipe_rm_in(data_frm_RAT_ls_pipe3_rm_in),
	 .data_frm_RAT_pipe_rs_in(data_frm_RAT_ls_pipe3_rs_in),
	 .data_frm_RAT_pipe_cpsr_in(data_frm_RAT_ls_pipe3_cpsr_in),
    .reg_data_to_issue_pipe_rn_out(reg_data_to_issue_ls_pipe3_rn_out),
	 .reg_data_to_issue_pipe_rm_out(reg_data_to_issue_ls_pipe3_rm_out),
	 .reg_data_to_issue_pipe_rs_out(reg_data_to_issue_ls_pipe3_rs_out),
	 .reg_data_to_issue_pipe_cpsr_out(reg_data_to_issue_ls_pipe3_cpsr_out)
    );
/******LS_PIPE3 ENDS******/


/******BRANCH_PIPE4 STARTS******/
instr_issue_pipe_reg instr_issue_branch_pipe4_rn (
    .tag_to_issue_pipe_rn_in(tag_to_issue_plus_branch_pipe4_rn_log_address_in), 
	 .tag_to_issue_pipe_rm_in(tag_to_issue_plus_branch_pipe4_rm_log_address_in),
	 .tag_to_issue_pipe_rs_in(tag_to_issue_plus_branch_pipe4_rs_log_address_in),
	 .tag_to_issue_pipe_cpsr_in(tag_to_issue_branch_pipe4_cpsr_in),
    .valid_instructions_RB_in(valid_instructions_in_RB), 
    .load_multiple_instr_RB_in(load_multiple_instr_RB), 
    .ldm_data_reg_15_in(ldm_data_reg[15][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_14_in(ldm_data_reg[14][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_13_in(ldm_data_reg[13][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_12_in(ldm_data_reg[12][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_11_in(ldm_data_reg[11][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_10_in(ldm_data_reg[10][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_9_in(ldm_data_reg[9][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_8_in(ldm_data_reg[8][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_7_in(ldm_data_reg[7][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_6_in(ldm_data_reg[6][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_5_in(ldm_data_reg[5][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_4_in(ldm_data_reg[4][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_3_in(ldm_data_reg[3][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_2_in(ldm_data_reg[2][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_1_in(ldm_data_reg[1][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .ldm_data_reg_0_in(ldm_data_reg[0][`LOG_MOD_REG_ADDR_SIZE-1:0]), 
    .mod_reg_addr_rd_23_in(mod_reg_addr_rd[23]), 
    .mod_reg_addr_rd_22_in(mod_reg_addr_rd[22]), 
    .mod_reg_addr_rd_21_in(mod_reg_addr_rd[21]), 
    .mod_reg_addr_rd_20_in(mod_reg_addr_rd[20]), 
    .mod_reg_addr_rd_19_in(mod_reg_addr_rd[19]), 
    .mod_reg_addr_rd_18_in(mod_reg_addr_rd[18]), 
    .mod_reg_addr_rd_17_in(mod_reg_addr_rd[17]), 
    .mod_reg_addr_rd_16_in(mod_reg_addr_rd[16]), 
    .mod_reg_addr_rd_15_in(mod_reg_addr_rd[15]), 
    .mod_reg_addr_rd_14_in(mod_reg_addr_rd[14]), 
    .mod_reg_addr_rd_13_in(mod_reg_addr_rd[13]), 
    .mod_reg_addr_rd_12_in(mod_reg_addr_rd[12]), 
    .mod_reg_addr_rd_11_in(mod_reg_addr_rd[11]), 
    .mod_reg_addr_rd_10_in(mod_reg_addr_rd[10]), 
    .mod_reg_addr_rd_9_in(mod_reg_addr_rd[9]), 
    .mod_reg_addr_rd_8_in(mod_reg_addr_rd[8]), 
    .mod_reg_addr_rd_7_in(mod_reg_addr_rd[7]), 
    .mod_reg_addr_rd_6_in(mod_reg_addr_rd[6]), 
    .mod_reg_addr_rd_5_in(mod_reg_addr_rd[5]), 
    .mod_reg_addr_rd_4_in(mod_reg_addr_rd[4]), 
    .mod_reg_addr_rd_3_in(mod_reg_addr_rd[3]), 
    .mod_reg_addr_rd_2_in(mod_reg_addr_rd[2]), 
    .mod_reg_addr_rd_1_in(mod_reg_addr_rd[1]), 
    .mod_reg_addr_rd_0_in(mod_reg_addr_rd[0]), 
    .mod_reg_addr_rn_23_in(mod_reg_addr_rn[23]), 
    .mod_reg_addr_rn_22_in(mod_reg_addr_rn[22]), 
    .mod_reg_addr_rn_21_in(mod_reg_addr_rn[21]), 
    .mod_reg_addr_rn_20_in(mod_reg_addr_rn[20]), 
    .mod_reg_addr_rn_19_in(mod_reg_addr_rn[19]), 
    .mod_reg_addr_rn_18_in(mod_reg_addr_rn[18]), 
    .mod_reg_addr_rn_17_in(mod_reg_addr_rn[17]), 
    .mod_reg_addr_rn_16_in(mod_reg_addr_rn[16]), 
    .mod_reg_addr_rn_15_in(mod_reg_addr_rn[15]), 
    .mod_reg_addr_rn_14_in(mod_reg_addr_rn[14]), 
    .mod_reg_addr_rn_13_in(mod_reg_addr_rn[13]), 
    .mod_reg_addr_rn_12_in(mod_reg_addr_rn[12]), 
    .mod_reg_addr_rn_11_in(mod_reg_addr_rn[11]), 
    .mod_reg_addr_rn_10_in(mod_reg_addr_rn[10]), 
    .mod_reg_addr_rn_9_in(mod_reg_addr_rn[9]), 
    .mod_reg_addr_rn_8_in(mod_reg_addr_rn[8]), 
    .mod_reg_addr_rn_7_in(mod_reg_addr_rn[7]), 
    .mod_reg_addr_rn_6_in(mod_reg_addr_rn[6]), 
    .mod_reg_addr_rn_5_in(mod_reg_addr_rn[5]), 
    .mod_reg_addr_rn_4_in(mod_reg_addr_rn[4]), 
    .mod_reg_addr_rn_3_in(mod_reg_addr_rn[3]), 
    .mod_reg_addr_rn_2_in(mod_reg_addr_rn[2]), 
    .mod_reg_addr_rn_1_in(mod_reg_addr_rn[1]), 
    .mod_reg_addr_rn_0_in(mod_reg_addr_rn[0]), 
	 .mod_reg_addr_cpsr_23_in(mod_reg_addr_cpsr[23]), 
    .mod_reg_addr_cpsr_22_in(mod_reg_addr_cpsr[22]), 
    .mod_reg_addr_cpsr_21_in(mod_reg_addr_cpsr[21]), 
    .mod_reg_addr_cpsr_20_in(mod_reg_addr_cpsr[20]), 
    .mod_reg_addr_cpsr_19_in(mod_reg_addr_cpsr[19]), 
    .mod_reg_addr_cpsr_18_in(mod_reg_addr_cpsr[18]), 
    .mod_reg_addr_cpsr_17_in(mod_reg_addr_cpsr[17]), 
    .mod_reg_addr_cpsr_16_in(mod_reg_addr_cpsr[16]), 
    .mod_reg_addr_cpsr_15_in(mod_reg_addr_cpsr[15]), 
    .mod_reg_addr_cpsr_14_in(mod_reg_addr_cpsr[14]), 
    .mod_reg_addr_cpsr_13_in(mod_reg_addr_cpsr[13]), 
    .mod_reg_addr_cpsr_12_in(mod_reg_addr_cpsr[12]), 
    .mod_reg_addr_cpsr_11_in(mod_reg_addr_cpsr[11]), 
    .mod_reg_addr_cpsr_10_in(mod_reg_addr_cpsr[10]), 
    .mod_reg_addr_cpsr_9_in(mod_reg_addr_cpsr[9]), 
    .mod_reg_addr_cpsr_8_in(mod_reg_addr_cpsr[8]), 
    .mod_reg_addr_cpsr_7_in(mod_reg_addr_cpsr[7]), 
    .mod_reg_addr_cpsr_6_in(mod_reg_addr_cpsr[6]), 
    .mod_reg_addr_cpsr_5_in(mod_reg_addr_cpsr[5]), 
    .mod_reg_addr_cpsr_4_in(mod_reg_addr_cpsr[4]), 
    .mod_reg_addr_cpsr_3_in(mod_reg_addr_cpsr[3]), 
    .mod_reg_addr_cpsr_2_in(mod_reg_addr_cpsr[2]), 
    .mod_reg_addr_cpsr_1_in(mod_reg_addr_cpsr[1]), 
    .mod_reg_addr_cpsr_0_in(mod_reg_addr_cpsr[0]),
    .data_frm_RAT_pipe_rn_in(data_frm_RAT_branch_pipe4_rn_in),
	 .data_frm_RAT_pipe_rm_in(data_frm_RAT_branch_pipe4_rm_in),
	 .data_frm_RAT_pipe_rs_in(data_frm_RAT_branch_pipe4_rs_in),
	 .data_frm_RAT_pipe_cpsr_in(data_frm_RAT_branch_pipe4_cpsr_in),
    .reg_data_to_issue_pipe_rn_out(reg_data_to_issue_branch_pipe4_rn_out),
	 .reg_data_to_issue_pipe_rm_out(reg_data_to_issue_branch_pipe4_rm_out),
	 .reg_data_to_issue_pipe_rs_out(reg_data_to_issue_branch_pipe4_rs_out),
	 .reg_data_to_issue_pipe_cpsr_out(reg_data_to_issue_branch_pipe4_cpsr_out)
    );
/******BRANCH_PIPE4 ENDS******/


/******RETIRE_STARTS******/
retire_controller_sub_module controlling_retire (
	 .clk_in(clk_in),
	 .reset_in(reset_in),
    .head_pointer_instr1_in(head_pointer_instr1), 
    .head_pointer_instr2_in(head_pointer_instr2), 
    .valid_retire_instr_in(valid_retire_reg), 
    .load_multiple_instr_RB_in(load_multiple_instr_RB),
	 .ldm_data_reg_15_in(ldm_data_reg[15]),
	 .ldm_data_reg_14_in(ldm_data_reg[14]),
	 .ldm_data_reg_13_in(ldm_data_reg[13]),
	 .ldm_data_reg_12_in(ldm_data_reg[12]),
	 .ldm_data_reg_11_in(ldm_data_reg[11]),
	 .ldm_data_reg_10_in(ldm_data_reg[10]),
	 .ldm_data_reg_9_in(ldm_data_reg[9]),
	 .ldm_data_reg_8_in(ldm_data_reg[8]),
	 .ldm_data_reg_7_in(ldm_data_reg[7]),
	 .ldm_data_reg_6_in(ldm_data_reg[6]),
	 .ldm_data_reg_5_in(ldm_data_reg[5]),
	 .ldm_data_reg_4_in(ldm_data_reg[4]),
	 .ldm_data_reg_3_in(ldm_data_reg[3]),
	 .ldm_data_reg_2_in(ldm_data_reg[2]),
	 .ldm_data_reg_1_in(ldm_data_reg[1]),
	 .ldm_data_reg_0_in(ldm_data_reg[0]),
	 .retire_one_instr_out(retire_1_instr),
	 .retire_both_instr_out(retire_2_instr),
	 .ldm_retire_complete_instr1_out(ldm_retire_complete_instr1),
	 .ldm_retire_complete_instr2_out(ldm_retire_complete_instr2)
    );


endmodule
