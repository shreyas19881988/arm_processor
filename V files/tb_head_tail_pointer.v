`include "timescale.v"
`include "RB_info.v"



module tb_head_tail_pointer;

	// Inputs
	reg clk_in;
	reg reset_in;
	reg new_entry_enable_in;
	reg retire_1_instr_in;
	reg retire_2_instr_in;

	// Outputs
	wire [`RB_SIZE-1:0] head_pointer_out;
	wire [`RB_SIZE-1:0] tail_pointer_out;
	wire RB_full_out;
	wire RB_empty_out;

	// Instantiate the Unit Under Test (UUT)
	head_tail_pointer_RB uut (
		.clk_in(clk_in), 
		.reset_in(reset_in), 
		.new_entry_enable_in(new_entry_enable_in), 
		.retire_1_instr_in(retire_1_instr_in), 
		.retire_2_instr_in(retire_2_instr_in), 
		.head_pointer_out(head_pointer_out), 
		.tail_pointer_out(tail_pointer_out), 
		.RB_full_out(RB_full_out),
		.RB_empty_out(RB_empty_out)
	);

	initial begin
		// Initialize Inputs
		clk_in = 0;
		reset_in = 1;
		new_entry_enable_in = 0;
		retire_1_instr_in = 0;
		retire_2_instr_in = 0;

		// Wait 100 ns for global reset to finish
		#105;
      reset_in <= 0;
		#10
		new_entry_enable_in <= 1;
		#10
		new_entry_enable_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 1;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 1;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 1;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 1;
		#10
		new_entry_enable_in <= 0;
		retire_1_instr_in <= 0;
		retire_2_instr_in <= 0;

		// Add stimulus here

	end
      
	always #5 clk_in = ~clk_in;
	
endmodule

