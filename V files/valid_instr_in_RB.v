`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module valid_instr_in_RB(
									input [`RB_SIZE-1:0] head_pointer1_in,
									input [`RB_SIZE-1:0] tail_pointer1_in,
									output [`RB_SIZE-1:0] valid_instr_out,
									output head_greater_than_tail_out,
									output [`RB_SIZE-1:0] all_above_tail_out,
									output [`RB_SIZE-1:0] all_below_head_out
								);

wire [`RB_SIZE-1:0] head_greater_than_tail;
wire [`RB_SIZE-1:0] head_greater_than_tag_position;
wire [`RB_SIZE-1:0] all_below_head;
wire [`RB_SIZE-1:0] all_below_tail;
wire [`RB_SIZE-1:0] all_above_tag_position;
wire [`RB_SIZE-1:0] valid_instr_head_greater_than_tail;
wire [`RB_SIZE-1:0] valid_instr_tail_greater_than_head;
wire [`RB_SIZE-1:0] valid_instr_above_tag_for_head_greater_than_tag;
wire [`RB_SIZE-1:0] valid_instr_above_tag_for_tag_greater_than_head;
wire head_greater_than_tag_position_single;

genvar i;
generate
for(i=`RB_SIZE-1;i>0;i=i-1)
begin	:	grp_tail_greater
	assign all_above_tail_out[i] = |(tail_pointer1_in[i-1:0]);
end
endgenerate

assign all_above_tail_out[0] = 1'b0;


assign head_greater_than_tail = head_pointer1_in & all_above_tail_out;


assign head_greater_than_tail_out = |(head_greater_than_tail);


genvar j;
generate
for(j=0;j<=`RB_SIZE-2;j=j+1)
begin	:	grp_head_greater
	assign all_below_tail[j] = |(tail_pointer1_in[`RB_SIZE-1:j+1]);
	assign all_below_head[j] = |(head_pointer1_in[`RB_SIZE-1:j+1]);
end
endgenerate


assign all_below_tail[`RB_SIZE-1] = 1'b0;


assign all_below_head[`RB_SIZE-1] = 1'b0;


assign all_below_head_out = all_below_head | head_pointer1_in;


assign valid_instr_head_greater_than_tail = all_below_head_out & all_above_tail_out;


assign valid_instr_tail_greater_than_head = all_below_head_out | all_above_tail_out;


assign valid_instr_out = head_greater_than_tail_out ? valid_instr_head_greater_than_tail : 

valid_instr_tail_greater_than_head;


endmodule
