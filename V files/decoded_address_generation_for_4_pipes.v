`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module decoded_address_generation_for_4_pipes(
					input [`PIPES_INSTR_TAG_WORD-1:0] tag_instr_pipes_in,
					input [`RB_SIZE-1:0] valid_instructions_in_RB_in,
					output [`RB_SIZE-1:0] decoded_valid_addr_alu_pipe1_out,
					output [`RB_SIZE-1:0] decoded_valid_addr_alu_pipe2_out,
					output [`RB_SIZE-1:0] decoded_valid_addr_ls_pipe3_out,
					output [`RB_SIZE-1:0] decoded_valid_addr_branch_pipe4_out
    );

wire [`RB_SIZE-1:0] decoded_addr_alu_pipe1;
wire [`RB_SIZE-1:0] decoded_addr_alu_pipe2;
wire [`RB_SIZE-1:0] decoded_addr_ls_pipe3;
wire [`RB_SIZE-1:0] decoded_addr_branch_pipe4;


decoder_24 decoded_address_alu_pipe1 (
				.tag_in(tag_instr_pipes_in[`ALU_PIPE1_INSTR_TAG_START:`ALU_PIPE1_INSTR_TAG_END]),
				.decoded_addr_out(decoded_addr_alu_pipe1)
				);

decoder_24 decoded_address_alu_pipe2 (
				.tag_in(tag_instr_pipes_in[`ALU_PIPE2_INSTR_TAG_START:`ALU_PIPE2_INSTR_TAG_END]),
				.decoded_addr_out(decoded_addr_alu_pipe2)
				);

decoder_24 decoded_address_ls_pipe3 (
				.tag_in(tag_instr_pipes_in[`LS_PIPE3_INSTR_TAG_START:`LS_PIPE3_INSTR_TAG_END]),
				.decoded_addr_out(decoded_addr_ls_pipe3)
				);

decoder_24 decoded_address_branch_pipe4 (
				.tag_in(tag_instr_pipes_in[`BRANCH_PIPE4_INSTR_TAG_START:`BRANCH_PIPE4_INSTR_TAG_END]),
				.decoded_addr_out(decoded_addr_branch_pipe4)
				);


assign decoded_valid_addr_alu_pipe1_out = decoded_addr_alu_pipe1 & valid_instructions_in_RB_in;


assign decoded_valid_addr_alu_pipe2_out = decoded_addr_alu_pipe2 & valid_instructions_in_RB_in;


assign decoded_valid_addr_ls_pipe3_out = decoded_addr_ls_pipe3 & valid_instructions_in_RB_in;


assign decoded_valid_addr_branch_pipe4_out = decoded_addr_branch_pipe4 & valid_instructions_in_RB_in;

endmodule
