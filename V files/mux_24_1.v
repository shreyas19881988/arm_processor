`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module mux_24_1(y_out,i0_in,i1_in,i2_in,i3_in,i4_in,i5_in,i6_in,i7_in,i8_in,i9_in,i10_in,i11_in,
i12_in,i13_in,i14_in,i15_in,i16_in,i17_in,i18_in,i19_in,i20_in,i21_in,i22_in,i23_in,sel_in);

parameter BUS_WIDTH = 5;

output [BUS_WIDTH-1 : 0] y_out;
input [BUS_WIDTH-1 : 0] i0_in;
input [BUS_WIDTH-1 : 0] i1_in;
input [BUS_WIDTH-1 : 0] i2_in;
input [BUS_WIDTH-1 : 0] i3_in;
input [BUS_WIDTH-1 : 0] i4_in;
input [BUS_WIDTH-1 : 0] i5_in;
input [BUS_WIDTH-1 : 0] i6_in;
input [BUS_WIDTH-1 : 0] i7_in;
input [BUS_WIDTH-1 : 0] i8_in;
input [BUS_WIDTH-1 : 0] i9_in;
input [BUS_WIDTH-1 : 0] i10_in;
input [BUS_WIDTH-1 : 0] i11_in;
input [BUS_WIDTH-1 : 0] i12_in;
input [BUS_WIDTH-1 : 0] i13_in;
input [BUS_WIDTH-1 : 0] i14_in;
input [BUS_WIDTH-1 : 0] i15_in;
input [BUS_WIDTH-1 : 0] i16_in;
input [BUS_WIDTH-1 : 0] i17_in;
input [BUS_WIDTH-1 : 0] i18_in;
input [BUS_WIDTH-1 : 0] i19_in;
input [BUS_WIDTH-1 : 0] i20_in;
input [BUS_WIDTH-1 : 0] i21_in;
input [BUS_WIDTH-1 : 0] i22_in;
input [BUS_WIDTH-1 : 0] i23_in;
	
input [`ENCODED_RB_ADDR-1:0] sel_in;

reg [BUS_WIDTH-1 : 0] y_out;
wire [BUS_WIDTH-1 : 0] i0_in;
wire [BUS_WIDTH-1 : 0] i1_in;
wire [BUS_WIDTH-1 : 0] i2_in;
wire [BUS_WIDTH-1 : 0] i3_in;
wire [BUS_WIDTH-1 : 0] i4_in;
wire [BUS_WIDTH-1 : 0] i5_in;
wire [BUS_WIDTH-1 : 0] i6_in;
wire [BUS_WIDTH-1 : 0] i7_in;
wire [BUS_WIDTH-1 : 0] i8_in;
wire [BUS_WIDTH-1 : 0] i9_in;
wire [BUS_WIDTH-1 : 0] i10_in;
wire [BUS_WIDTH-1 : 0] i11_in;
wire [BUS_WIDTH-1 : 0] i12_in;
wire [BUS_WIDTH-1 : 0] i13_in;
wire [BUS_WIDTH-1 : 0] i14_in;
wire [BUS_WIDTH-1 : 0] i15_in;
wire [BUS_WIDTH-1 : 0] i16_in;
wire [BUS_WIDTH-1 : 0] i17_in;
wire [BUS_WIDTH-1 : 0] i18_in;
wire [BUS_WIDTH-1 : 0] i19_in;
wire [BUS_WIDTH-1 : 0] i20_in;
wire [BUS_WIDTH-1 : 0] i21_in;
wire [BUS_WIDTH-1 : 0] i22_in;
wire [BUS_WIDTH-1 : 0] i23_in;
wire [`ENCODED_RB_ADDR-1:0] sel_in;

always @(*)
begin
	case (sel_in)
		`ENCODED_RB_ADDR'b00000 : y_out = i0_in;
		`ENCODED_RB_ADDR'b00001 : y_out = i1_in;
		`ENCODED_RB_ADDR'b00010 : y_out = i2_in;
		`ENCODED_RB_ADDR'b00011 : y_out = i3_in;
		`ENCODED_RB_ADDR'b00100 : y_out = i4_in;
		`ENCODED_RB_ADDR'b00101 : y_out = i5_in;
		`ENCODED_RB_ADDR'b00110 : y_out = i6_in;
		`ENCODED_RB_ADDR'b00111 : y_out = i7_in;
		`ENCODED_RB_ADDR'b01000 : y_out = i8_in;
		`ENCODED_RB_ADDR'b01001 : y_out = i9_in;
		`ENCODED_RB_ADDR'b01010 : y_out = i10_in;
		`ENCODED_RB_ADDR'b01011 : y_out = i11_in;
		`ENCODED_RB_ADDR'b01100 : y_out = i12_in;
		`ENCODED_RB_ADDR'b01101 : y_out = i13_in;
		`ENCODED_RB_ADDR'b01110 : y_out = i14_in;
		`ENCODED_RB_ADDR'b01111 : y_out = i15_in;
		`ENCODED_RB_ADDR'b10000 : y_out = i16_in;
		`ENCODED_RB_ADDR'b10001 : y_out = i17_in;
		`ENCODED_RB_ADDR'b10010 : y_out = i18_in;
		`ENCODED_RB_ADDR'b10011 : y_out = i19_in;
		`ENCODED_RB_ADDR'b10100 : y_out = i20_in;
		`ENCODED_RB_ADDR'b10101 : y_out = i21_in;
		`ENCODED_RB_ADDR'b10110 : y_out = i22_in;
		`ENCODED_RB_ADDR'b10111 : y_out = i23_in;
		default : y_out = 0;
	endcase
end

endmodule
