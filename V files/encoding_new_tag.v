`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module encoding_new_tag(
								input [`RB_SIZE-1:0] all_instr_updating_rd_in,
								input [`RB_SIZE-1:0] all_instr_updating_rn_in,
								input [`RB_SIZE-1:0] all_instr_updating_cpsr_in,
								input head_greater_than_tail_in,
								input [`RB_SIZE-1:0] all_above_tail_in,
								input [`RB_SIZE-1:0] all_below_head_in,
								input [`RB_SIZE-1:0] invalid_instr_in,
								input [`RB_SIZE-1:0] instr_other_pipe1_valid_retire_in,
								input [`RB_SIZE-1:0] instr_other_pipe2_valid_retire_in,
								input [`RB_SIZE-1:0] instr_pipe3_other_valid_retire_in,
								input [`THREE_INSTR_TAG_WORD-1:0] tag_in_other_pipes_in,
								output [`TAG_TO_CHANGE_RD_RN_CPSR-1:0] new_tag_to_change_rd_rn_cpsr_out,
								output rd_invalid_but_retiring_in_other_pipe_out,
								output rn_invalid_but_retiring_in_other_pipe_out,
								output cpsr_invalid_but_retiring_in_other_pipe_out
								
    );

wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_rd;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_rn;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_cpsr;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_rd;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_rn;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_cpsr;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_rd;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_rn;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_cpsr;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_rd;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_rn;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_cpsr;
wire [`RB_SIZE-1:0] instr_updating_rd_above_tail;
wire [`RB_SIZE-1:0] instr_updating_rn_above_tail;
wire [`RB_SIZE-1:0] instr_updating_cpsr_above_tail;
wire [`RB_SIZE-1:0] instr_updating_rd_below_head;
wire [`RB_SIZE-1:0] instr_updating_rn_below_head;
wire [`RB_SIZE-1:0] instr_updating_cpsr_below_head;
wire [`RB_SIZE-1:0] all_below_invalid_instr;
wire [`RB_SIZE-1:0] invalid_instr_above_tail;
wire or_invalid_instr_above_tail;
wire [`NO_OF_REG-1:0] decoded_rd_addr;
wire [`NO_OF_REG-1:0] decoded_rn_addr;
wire [`NO_OF_REG-1:0] decoded_cpsr_addr;
wire [`TAG_SIZE-1:0] tag_rd;
wire [`TAG_SIZE-1:0] tag_rn;
wire [`TAG_SIZE-1:0] tag_cpsr;



genvar i;
generate
for(i=0;i<=`RB_SIZE-2;i=i+1)
begin	: grp_generate_all_below_invalid_instr
	assign all_below_invalid_instr[i] = |(invalid_instr_in[`RB_SIZE-1:i+1]);
end
endgenerate

assign all_below_invalid_instr[`RB_SIZE-1] = 1'b0;


assign invalid_instr_above_tail = all_above_tail_in & all_below_invalid_instr;

/******RD STARTS******/

/******Generating New Tag Starts******/
priority_encoder_24_1 priority_encoder_for_head_greater_than_tail (
    .decoded_input_in(all_instr_updating_rd_in), 
    .priority_encoded_out(new_tag_head_greater_than_tail_rd)
    );


assign instr_updating_rd_above_tail = all_above_tail_in & all_instr_updating_rd_in;


priority_encoder_24_1 priority_encoder_for_tail_greater_than_head_and_instr_above_tail (
    .decoded_input_in(instr_updating_rd_above_tail), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_rd)
    );


assign instr_updating_rd_below_head = all_below_head_in & all_instr_updating_rd_in;


priority_encoder_24_1 priority_encoder_for_tail_greater_than_head_and_instr_below_head (
    .decoded_input_in(instr_updating_rd_below_head), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_rd)
    );


assign or_invalid_instr_above_tail = |(invalid_instr_above_tail);


assign new_tag_tail_greater_than_head_final_rd = or_invalid_instr_above_tail ? 

new_tag_tail_greater_than_head_instr_above_tail_rd : 

new_tag_tail_greater_than_head_instr_below_head_rd;


assign new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RD_STARTS:`TAG_TO_CHANGE_RD_ENDS] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_rd : 

new_tag_tail_greater_than_head_final_rd;


assign rd_invalid_but_retiring_in_other_pipe_out = 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RD_STARTS:`TAG_TO_CHANGE_RD_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RD_STARTS:`TAG_TO_CHANGE_RD_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RD_STARTS:`TAG_TO_CHANGE_RD_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_pipe3_other_valid_retire_in));
/******RD ENDS******/

/******RN STARTS******/

/******Generating New Tag Starts******/
priority_encoder_24_1 priority_encoder_for_head_greater_than_tail (
    .decoded_input_in(all_instr_updating_rn_in), 
    .priority_encoded_out(new_tag_head_greater_than_tail_rn)
    );


assign instr_updating_rn_above_tail = all_above_tail_in & all_instr_updating_rn_in;


priority_encoder_24_1 priority_encoder_for_tail_greater_than_head_and_instr_above_tail (
    .decoded_input_in(instr_updating_rn_above_tail), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_rn)
    );


assign instr_updating_rn_below_head = all_below_head_in & all_instr_updating_rn_in;


priority_encoder_24_1 priority_encoder_for_tail_greater_than_head_and_instr_below_head (
    .decoded_input_in(instr_updating_rn_below_head), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_rn)
    );


assign or_invalid_instr_above_tail = |(invalid_instr_above_tail);


assign new_tag_tail_greater_than_head_final_rn = or_invalid_instr_above_tail ? 

new_tag_tail_greater_than_head_instr_above_tail_rn : 

new_tag_tail_greater_than_head_instr_below_head_rn;


assign new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RN_STARTS:`TAG_TO_CHANGE_RN_ENDS] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_rn : 

new_tag_tail_greater_than_head_final_rn;


assign rn_invalid_but_retiring_in_other_pipe_out =  

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RN_STARTS:`TAG_TO_CHANGE_RN_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RN_STARTS:`TAG_TO_CHANGE_RN_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_RN_STARTS:`TAG_TO_CHANGE_RN_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_pipe3_other_valid_retire_in));
/******RN ENDS******/

/******CPSR STARTS******/

/******New tag Starts******/
priority_encoder_24_1 priority_encoder_for_head_greater_than_tail (
    .decoded_input_in(all_instr_updating_cpsr_in), 
    .priority_encoded_out(new_tag_head_greater_than_tail_cpsr)
    );


assign instr_updating_cpsr_above_tail = all_above_tail_in & all_instr_updating_cpsr_in;


priority_encoder_24_1 priority_encoder_for_tail_greater_than_head_and_instr_above_tail (
    .decoded_input_in(instr_updating_cpsr_above_tail), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_cpsr)
    );


assign instr_updating_cpsr_below_head = all_below_head_in & all_instr_updating_cpsr_in;


priority_encoder_24_1 priority_encoder_for_tail_greater_than_head_and_instr_below_head (
    .decoded_input_in(instr_updating_cpsr_below_head), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_cpsr)
    );


assign or_invalid_instr_above_tail = |(invalid_instr_above_tail);


assign new_tag_tail_greater_than_head_final_cpsr = or_invalid_instr_above_tail ? 

new_tag_tail_greater_than_head_instr_above_tail_cpsr : 

new_tag_tail_greater_than_head_instr_below_head_cpsr;


assign new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_CPSR_STARTS:`TAG_TO_CHANGE_CPSR_ENDS] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_cpsr : 

new_tag_tail_greater_than_head_final_cpsr;


assign cpsr_invalid_but_retiring_in_other_pipe_out =  

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_CPSR_STARTS:`TAG_TO_CHANGE_CPSR_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_CPSR_STARTS:`TAG_TO_CHANGE_CPSR_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_rd_rn_cpsr_out[`TAG_TO_CHANGE_CPSR_STARTS:`TAG_TO_CHANGE_CPSR_ENDS] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_pipe3_other_valid_retire_in));
/******CPSR Ends******/



endmodule
