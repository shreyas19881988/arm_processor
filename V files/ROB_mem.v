`include "timescale.v"
`include "RB_info.v"

module ROB_mem(
						input clk_in,
						input reset_in,
						
						/******Two new instructions are to be put in the ROB******/
						input new_entry_enable_in,
						
						/******REORDER_BUFFER_ENTRY_WORD******/
						input [`RB_ENTRY_WORD-1:0] RB_entry1_info_in,
						input [`RB_ENTRY_WORD-1:0] RB_entry2_info_in
						
    );

wire [`RB_ENTRY_WORD-1:0] reorder_buffer_entry [`RB_SIZE-1:0];
wire [`RB_SIZE-1:0] decoded_address1;
wire [`RB_SIZE-1:0] decoded_address2;







endmodule
