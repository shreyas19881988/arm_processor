`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"


module instr_issue_for_cpsr(
								input [`TAG_SIZE-1:0] tag_to_issue_pipe_cpsr_in,
								input [`RB_SIZE-1:0] valid_instructions_RB_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_23_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_22_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_21_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_20_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_19_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_18_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_17_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_16_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_15_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_14_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_13_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_12_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_11_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_10_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_9_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_8_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_7_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_6_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_5_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_4_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_3_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_2_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_1_in,
								input [`MOD_REG_ADDR_SIZE-1:0] mod_reg_addr_cpsr_0_in,
								input [`MOD_REG_ADDR_SIZE-1:0] data_frm_RAT_pipe_cpsr_in,
								output [`MOD_REG_ADDR_SIZE-1:0] reg_data_to_issue_pipe_cpsr_out
    );


wire [`RB_SIZE-1:0] decoded_addr_for_instr_issue_pipe_cpsr;
wire [`MOD_REG_ADDR_SIZE-1:0] reg_data_frm_cpsr_reg_for_instr_issue_pipe_cpsr;
wire [`RB_SIZE-1:0] instr_to_issue_pipe_cspr_valid;


decoder_24 decoded_address_tag_to_issue_alu_pipe1_rn_addr (
				.tag_in(tag_to_issue_pipe_cpsr_in),
				.decoded_addr_out(decoded_addr_for_instr_issue_pipe_cpsr)
				);


assign instr_to_issue_pipe_cspr_valid = decoded_addr_for_instr_issue_pipe_cpsr & 

valid_instructions_RB_in;


mux_24_1 #`MOD_REG_ADDR_SIZE mux_for_selecting_cpsr_data (
    .y_out(reg_data_frm_cpsr_reg_for_instr_issue_pipe_cpsr), 
    .i0_in(mod_reg_addr_cpsr_23_in), 
    .i1_in(mod_reg_addr_cpsr_22_in), 
    .i2_in(mod_reg_addr_cpsr_21_in), 
    .i3_in(mod_reg_addr_cpsr_20_in), 
    .i4_in(mod_reg_addr_cpsr_19_in), 
    .i5_in(mod_reg_addr_cpsr_18_in), 
    .i6_in(mod_reg_addr_cpsr_17_in), 
    .i7_in(mod_reg_addr_cpsr_16_in), 
    .i8_in(mod_reg_addr_cpsr_15_in), 
    .i9_in(mod_reg_addr_cpsr_14_in), 
    .i10_in(mod_reg_addr_cpsr_13_in), 
    .i11_in(mod_reg_addr_cpsr_12_in), 
    .i12_in(mod_reg_addr_cpsr_11_in), 
    .i13_in(mod_reg_addr_cpsr_10_in), 
    .i14_in(mod_reg_addr_cpsr_9_in), 
    .i15_in(mod_reg_addr_cpsr_8_in), 
    .i16_in(mod_reg_addr_cpsr_7_in), 
    .i17_in(mod_reg_addr_cpsr_6_in), 
    .i18_in(mod_reg_addr_cpsr_5_in), 
    .i19_in(mod_reg_addr_cpsr_4_in), 
    .i20_in(mod_reg_addr_cpsr_3_in), 
    .i21_in(mod_reg_addr_cpsr_2_in), 
    .i22_in(mod_reg_addr_cpsr_1_in), 
    .i23_in(mod_reg_addr_cpsr_0_in), 
    .sel_in(tag_to_issue_pipe_cpsr_in)
    );


assign reg_data_to_issue_pipe_cpsr_out = |(instr_to_issue_pipe_cspr_valid) ? 

reg_data_frm_cpsr_reg_for_instr_issue_pipe_cpsr : data_frm_RAT_pipe_cpsr_in;


endmodule
