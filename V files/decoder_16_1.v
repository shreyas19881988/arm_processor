`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module decoder_16_1(
							input [`LOG_ADDRESS_SIZE-1:0] reg_addr_in,
							output [`decoded_output_16_1-1:0] decoded_out
    );




always@(*)
begin
	case(reg_addr_in)
		`LOG_ADDRESS_SIZE'b0000 : 
		begin
			decoded_out = 1;
		end
		`LOG_ADDRESS_SIZE'b0001 : 
		begin
			decoded_out = 2;
		end
		`LOG_ADDRESS_SIZE'b0010 : 
		begin
			decoded_out = 4;
		end
		`LOG_ADDRESS_SIZE'b0011 : 
		begin
			decoded_out = 8;
		end
		`LOG_ADDRESS_SIZE'b0100 : 
		begin
			decoded_out = 16;
		end
		`LOG_ADDRESS_SIZE'b0101 : 
		begin
			decoded_out = 32;
		end
		`LOG_ADDRESS_SIZE'b0110 : 
		begin
			decoded_out = 64;
		end
		`LOG_ADDRESS_SIZE'b0111 : 
		begin
			decoded_out = 128;
		end
		`LOG_ADDRESS_SIZE'b1000 : 
		begin
			decoded_out = 256;
		end
		`LOG_ADDRESS_SIZE'b1001 : 
		begin
			decoded_out = 512;
		end
		`LOG_ADDRESS_SIZE'b1010 : 
		begin
			decoded_out = 1024;
		end
		`LOG_ADDRESS_SIZE'b1011 : 
		begin
			decoded_out = 2048;
		end
		`LOG_ADDRESS_SIZE'b1100 : 
		begin
			decoded_out = 4096;
		end
		`LOG_ADDRESS_SIZE'b1101 : 
		begin
			decoded_out = 8192;
		end
		`LOG_ADDRESS_SIZE'b1110 : 
		begin
			decoded_out = 16384;
		end
		`LOG_ADDRESS_SIZE'b1111 : 
		begin
			decoded_out = 32768;
		end
	endcase
end



endmodule
