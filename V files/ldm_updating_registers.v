`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module ldm_updating_registers(
		input [`LOG_ADDRESS_SIZE-1:0] r0_in,
		input [`LOG_ADDRESS_SIZE-1:0] r1_in,
		input [`LOG_ADDRESS_SIZE-1:0] r2_in,
		input [`LOG_ADDRESS_SIZE-1:0] r3_in,
		input [`LOG_ADDRESS_SIZE-1:0] r4_in,
		input [`LOG_ADDRESS_SIZE-1:0] r5_in,
		input [`LOG_ADDRESS_SIZE-1:0] r6_in,
		input [`LOG_ADDRESS_SIZE-1:0] r7_in,
		input [`LOG_ADDRESS_SIZE-1:0] r8_in,
		input [`LOG_ADDRESS_SIZE-1:0] r9_in,
		input [`LOG_ADDRESS_SIZE-1:0] r10_in,
		input [`LOG_ADDRESS_SIZE-1:0] r11_in,
		input [`LOG_ADDRESS_SIZE-1:0] r12_in,
		input [`LOG_ADDRESS_SIZE-1:0] r13_in,
		input [`LOG_ADDRESS_SIZE-1:0] r14_in,
		input [`LOG_ADDRESS_SIZE-1:0] r15_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_23_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_22_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_21_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_20_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_19_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_18_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_17_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_16_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_15_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_14_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_13_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_12_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_11_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_10_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_9_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_8_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_7_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_6_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_5_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_4_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_3_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_2_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_1_in,
		input [`LDM_WORD-1:0] ldm_word_frm_RB_0_in,
		input [`TAG_SIZE-1:0] tag_instr_in,
		input [`RB_SIZE-1:0] invalid_ldm_instruction_in,
		input [`RB_SIZE-1:0] valid_instr_above_tag_pipe_in,
		input or_invalid_instr_above_tail_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r0_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r1_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r2_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r3_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r4_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r5_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r6_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r7_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r8_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r9_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r10_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r11_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r12_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r13_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r14_pipe_in,
		input [`RB_SIZE-1:0] instr_updating_r15_pipe_in,
		input [`RB_SIZE-1:0] all_above_tail_in,
		input [`RB_SIZE-1:0] all_below_head_in,
		input head_greater_than_tail_in,
		input [`THREE_INSTR_TAG_WORD-1:0] tag_in_other_pipes_in,
		input [`RB_SIZE-1:0] instr_other_pipe1_valid_retire_in,
		input [`RB_SIZE-1:0] instr_other_pipe2_valid_retire_in,
		input [`RB_SIZE-1:0] instr_other_pipe3_valid_retire_in,
		output [`TAG_ALL_REG-1:0] new_tag_to_change_all_reg_for_ldm_invalid_out,
		output [`NO_OF_REG-1:0] new_tag_change_en_for_ldm_invalid_out,
		output [`NO_OF_REG-1:0] tag_retire_en_for_ldm_invalid_out

		
    );


wire [`LDM_WORD-1:0] ldm_word_frm_pipe;
wire or_invalid_ldm_instruction;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r0;
wire or_valid_instr_above_ldm_invalid_tag_updating_r0;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r1;
wire or_valid_instr_above_ldm_invalid_tag_updating_r1;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r2;
wire or_valid_instr_above_ldm_invalid_tag_updating_r2;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r3;
wire or_valid_instr_above_ldm_invalid_tag_updating_r3;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r4;
wire or_valid_instr_above_ldm_invalid_tag_updating_r4;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r5;
wire or_valid_instr_above_ldm_invalid_tag_updating_r5;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r6;
wire or_valid_instr_above_ldm_invalid_tag_updating_r6;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r7;
wire or_valid_instr_above_ldm_invalid_tag_updating_r7;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r8;
wire or_valid_instr_above_ldm_invalid_tag_updating_r8;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r9;
wire or_valid_instr_above_ldm_invalid_tag_updating_r9;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r10;
wire or_valid_instr_above_ldm_invalid_tag_updating_r10;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r11;
wire or_valid_instr_above_ldm_invalid_tag_updating_r11;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r12;
wire or_valid_instr_above_ldm_invalid_tag_updating_r12;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r13;
wire or_valid_instr_above_ldm_invalid_tag_updating_r13;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r14;
wire or_valid_instr_above_ldm_invalid_tag_updating_r14;
wire [`RB_SIZE-1:0] valid_instr_above_ldm_invalid_tag_updating_r15;
wire or_valid_instr_above_ldm_invalid_tag_updating_r15;
wire ldm_invalid_but_updating_r0;
wire ldm_invalid_but_updating_r1;
wire ldm_invalid_but_updating_r2;
wire ldm_invalid_but_updating_r3;
wire ldm_invalid_but_updating_r4;
wire ldm_invalid_but_updating_r5;
wire ldm_invalid_but_updating_r6;
wire ldm_invalid_but_updating_r7;
wire ldm_invalid_but_updating_r8;
wire ldm_invalid_but_updating_r9;
wire ldm_invalid_but_updating_r10;
wire ldm_invalid_but_updating_r11;
wire ldm_invalid_but_updating_r12;
wire ldm_invalid_but_updating_r13;
wire ldm_invalid_but_updating_r14;
wire ldm_invalid_but_updating_r15;
wire [`RB_SIZE-1:0] instr_updating_r0_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r1_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r2_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r3_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r4_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r5_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r6_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r7_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r8_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r9_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r10_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r11_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r12_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r13_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r14_but_current_ldm_invalid;
wire [`RB_SIZE-1:0] instr_updating_r15_but_current_ldm_invalid;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r0;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r1;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r2;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r3;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r4;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r5;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r6;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r7;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r8;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r9;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r10;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r11;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r12;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r13;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r14;
wire [`TAG_SIZE-1:0] new_tag_head_greater_than_tail_r15;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r0;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r1;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r2;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r3;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r4;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r5;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r6;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r7;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r8;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r9;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r10;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r11;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r12;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r13;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r14;
wire [`RB_SIZE-1:0] instr_above_tail_updating_r15;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r0;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r1;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r2;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r3;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r4;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r5;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r6;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r7;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r8;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r9;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r10;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r11;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r12;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r13;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r14;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_above_tail_r15;
wire [`RB_SIZE-1:0] instr_below_head_updating_r0;
wire [`RB_SIZE-1:0] instr_below_head_updating_r1;
wire [`RB_SIZE-1:0] instr_below_head_updating_r2;
wire [`RB_SIZE-1:0] instr_below_head_updating_r3;
wire [`RB_SIZE-1:0] instr_below_head_updating_r4;
wire [`RB_SIZE-1:0] instr_below_head_updating_r5;
wire [`RB_SIZE-1:0] instr_below_head_updating_r6;
wire [`RB_SIZE-1:0] instr_below_head_updating_r7;
wire [`RB_SIZE-1:0] instr_below_head_updating_r8;
wire [`RB_SIZE-1:0] instr_below_head_updating_r9;
wire [`RB_SIZE-1:0] instr_below_head_updating_r10;
wire [`RB_SIZE-1:0] instr_below_head_updating_r11;
wire [`RB_SIZE-1:0] instr_below_head_updating_r12;
wire [`RB_SIZE-1:0] instr_below_head_updating_r13;
wire [`RB_SIZE-1:0] instr_below_head_updating_r14;
wire [`RB_SIZE-1:0] instr_below_head_updating_r15;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r0;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r1;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r2;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r3;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r4;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r5;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r6;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r7;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r8;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r9;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r10;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r11;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r12;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r13;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r14;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_instr_below_head_r15;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r0;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r1;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r2;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r3;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r4;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r5;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r6;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r7;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r8;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r9;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r10;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r11;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r12;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r13;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r14;
wire [`TAG_SIZE-1:0] new_tag_tail_greater_than_head_final_updating_r15;
wire new_tag_for_r0_retiring_other_pipe;
wire new_tag_for_r1_retiring_other_pipe;
wire new_tag_for_r2_retiring_other_pipe;
wire new_tag_for_r3_retiring_other_pipe;
wire new_tag_for_r4_retiring_other_pipe;
wire new_tag_for_r5_retiring_other_pipe;
wire new_tag_for_r6_retiring_other_pipe;
wire new_tag_for_r7_retiring_other_pipe;
wire new_tag_for_r8_retiring_other_pipe;
wire new_tag_for_r9_retiring_other_pipe;
wire new_tag_for_r10_retiring_other_pipe;
wire new_tag_for_r11_retiring_other_pipe;
wire new_tag_for_r12_retiring_other_pipe;
wire new_tag_for_r13_retiring_other_pipe;
wire new_tag_for_r14_retiring_other_pipe;
wire new_tag_for_r15_retiring_other_pipe;


mux_24_1 #`LDM_WORD ldm_word_pipe (
    .y_out(ldm_word_frm_pipe), 
    .i0_in(ldm_word_frm_RB_23_in), 
    .i1_in(ldm_word_frm_RB_22_in), 
    .i2_in(ldm_word_frm_RB_21_in), 
    .i3_in(ldm_word_frm_RB_20_in), 
    .i4_in(ldm_word_frm_RB_19_in), 
    .i5_in(ldm_word_frm_RB_18_in), 
    .i6_in(ldm_word_frm_RB_17_in), 
    .i7_in(ldm_word_frm_RB_16_in), 
    .i8_in(ldm_word_frm_RB_15_in), 
    .i9_in(ldm_word_frm_RB_14_in), 
    .i10_in(ldm_word_frm_RB_13_in), 
    .i11_in(ldm_word_frm_RB_12_in), 
    .i12_in(ldm_word_frm_RB_11_in), 
    .i13_in(ldm_word_frm_RB_10_in), 
    .i14_in(ldm_word_frm_RB_9_in), 
    .i15_in(ldm_word_frm_RB_8_in), 
    .i16_in(ldm_word_frm_RB_7_in), 
    .i17_in(ldm_word_frm_RB_6_in), 
    .i18_in(ldm_word_frm_RB_5_in), 
    .i19_in(ldm_word_frm_RB_4_in), 
    .i20_in(ldm_word_frm_RB_3_in), 
    .i21_in(ldm_word_frm_RB_2_in), 
    .i22_in(ldm_word_frm_RB_1_in), 
    .i23_in(ldm_word_frm_RB_0_in), 
    .sel_in(tag_instr_in)
    );


assign or_invalid_ldm_instruction = |(invalid_ldm_instruction_in);

/******Tag change for R0******/
assign ldm_invalid_but_updating_r0 = ldm_word_frm_pipe[`LDM_REG_R0] & 

or_invalid_ldm_instruction;


assign instr_updating_r0_but_current_ldm_invalid = instr_updating_r0_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r0}};


assign valid_instr_above_ldm_invalid_tag_updating_r0 = instr_updating_r0_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r0 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r0), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r0)
    );


assign instr_above_tail_updating_r0 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r0;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r0 (
    .decoded_input_in(instr_above_tail_updating_r0), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r0)
    );


assign instr_below_head_updating_r0 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r0;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r0 (
    .decoded_input_in(instr_below_head_updating_r0), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r0)
    );


assign new_tag_tail_greater_than_head_final_updating_r0 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r0 : 

new_tag_tail_greater_than_head_instr_below_head_r0;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R0_START:`TAG_FOR_R0_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r0 : 

new_tag_tail_greater_than_head_final_updating_r0;


assign new_tag_for_r0_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R0_START:`TAG_FOR_R0_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R0_START:`TAG_FOR_R0_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R0_START:`TAG_FOR_R0_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r0 = 

|(valid_instr_above_ldm_invalid_tag_updating_r0);


assign new_tag_change_en_for_ldm_invalid_out[`R0] = ldm_invalid_but_updating_r0 & 

or_valid_instr_above_ldm_invalid_tag_updating_r0 & ~new_tag_for_r0_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R0] = ldm_invalid_but_updating_r0 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r0 | new_tag_for_r0_retiring_other_pipe);
/******Tag change for R0******/


/******Tag change for R1******/
assign ldm_invalid_but_updating_r1 = ldm_word_frm_pipe[`LDM_REG_R1] & 

or_invalid_ldm_instruction;


assign instr_updating_r1_but_current_ldm_invalid = instr_updating_r1_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r1}};


assign valid_instr_above_ldm_invalid_tag_updating_r1 = instr_updating_r1_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r1 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r1), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r1)
    );


assign instr_above_tail_updating_r1 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r1;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r1 (
    .decoded_input_in(instr_above_tail_updating_r1), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r1)
    );


assign instr_below_head_updating_r1 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r1;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r1 (
    .decoded_input_in(instr_below_head_updating_r1), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r1)
    );


assign new_tag_tail_greater_than_head_final_updating_r1 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r1 : 

new_tag_tail_greater_than_head_instr_below_head_r1;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R1_START:`TAG_FOR_R1_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r1 : 

new_tag_tail_greater_than_head_final_updating_r1;


assign new_tag_for_r1_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R1_START:`TAG_FOR_R1_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R1_START:`TAG_FOR_R1_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R1_START:`TAG_FOR_R1_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r1 = 

|(valid_instr_above_ldm_invalid_tag_updating_r1);


assign new_tag_change_en_for_ldm_invalid_out[`R1] = ldm_invalid_but_updating_r1 & 

or_valid_instr_above_ldm_invalid_tag_updating_r1 & ~new_tag_for_r1_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R1] = ldm_invalid_but_updating_r1 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r1 | new_tag_for_r1_retiring_other_pipe);
/******Tag change for R1******/


/******Tag change for R2******/
assign ldm_invalid_but_updating_r2 = ldm_word_frm_pipe[`LDM_REG_R2] & 

or_invalid_ldm_instruction;


assign instr_updating_r2_but_current_ldm_invalid = instr_updating_r2_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r2}};


assign valid_instr_above_ldm_invalid_tag_updating_r2 = instr_updating_r2_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r2 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r2), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r2)
    );


assign instr_above_tail_updating_r2 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r2;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r2 (
    .decoded_input_in(instr_above_tail_updating_r2), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r2)
    );


assign instr_below_head_updating_r2 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r2;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r2 (
    .decoded_input_in(instr_below_head_updating_r2), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r2)
    );


assign new_tag_tail_greater_than_head_final_updating_r2 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r2 : 

new_tag_tail_greater_than_head_instr_below_head_r2;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R2_START:`TAG_FOR_R2_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r2 : 

new_tag_tail_greater_than_head_final_updating_r2;


assign new_tag_for_r2_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R2_START:`TAG_FOR_R2_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R2_START:`TAG_FOR_R2_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R2_START:`TAG_FOR_R2_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r2 = 

|(valid_instr_above_ldm_invalid_tag_updating_r2);


assign new_tag_change_en_for_ldm_invalid_out[`R2] = ldm_invalid_but_updating_r2 & 

or_valid_instr_above_ldm_invalid_tag_updating_r2 & ~new_tag_for_r2_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R2] = ldm_invalid_but_updating_r2 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r2 | new_tag_for_r2_retiring_other_pipe);
/******Tag change for R2******/


/******Tag change for R3******/
assign ldm_invalid_but_updating_r3 = ldm_word_frm_pipe[`LDM_REG_R3] & 

or_invalid_ldm_instruction;


assign instr_updating_r3_but_current_ldm_invalid = instr_updating_r3_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r3}};


assign valid_instr_above_ldm_invalid_tag_updating_r3 = instr_updating_r3_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r3 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r3), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r3)
    );


assign instr_above_tail_updating_r3 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r3;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r3 (
    .decoded_input_in(instr_above_tail_updating_r3), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r3)
    );


assign instr_below_head_updating_r3 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r3;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r3 (
    .decoded_input_in(instr_below_head_updating_r3), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r3)
    );


assign new_tag_tail_greater_than_head_final_updating_r3 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r3 : 

new_tag_tail_greater_than_head_instr_below_head_r3;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R3_START:`TAG_FOR_R3_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r3 : 

new_tag_tail_greater_than_head_final_updating_r3;


assign new_tag_for_r3_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R3_START:`TAG_FOR_R3_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R3_START:`TAG_FOR_R3_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R3_START:`TAG_FOR_R3_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r3 = 

|(valid_instr_above_ldm_invalid_tag_updating_r3);


assign new_tag_change_en_for_ldm_invalid_out[`R3] = ldm_invalid_but_updating_r3 & 

or_valid_instr_above_ldm_invalid_tag_updating_r3 & ~new_tag_for_r3_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R3] = ldm_invalid_but_updating_r3 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r3 | new_tag_for_r3_retiring_other_pipe);
/******Tag change for R3******/


/******Tag change for R4******/
assign ldm_invalid_but_updating_r4 = ldm_word_frm_pipe[`LDM_REG_R4] & 

or_invalid_ldm_instruction;


assign instr_updating_r4_but_current_ldm_invalid = instr_updating_r4_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r4}};


assign valid_instr_above_ldm_invalid_tag_updating_r4 = instr_updating_r4_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r4 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r4), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r4)
    );


assign instr_above_tail_updating_r4 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r4;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r4 (
    .decoded_input_in(instr_above_tail_updating_r4), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r4)
    );


assign instr_below_head_updating_r4 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r4;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r4 (
    .decoded_input_in(instr_below_head_updating_r4), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r4)
    );


assign new_tag_tail_greater_than_head_final_updating_r4 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r4 : 

new_tag_tail_greater_than_head_instr_below_head_r4;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R4_START:`TAG_FOR_R4_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r4 : 

new_tag_tail_greater_than_head_final_updating_r4;


assign new_tag_for_r4_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R4_START:`TAG_FOR_R4_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R4_START:`TAG_FOR_R4_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R4_START:`TAG_FOR_R4_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r4 = 

|(valid_instr_above_ldm_invalid_tag_updating_r4);


assign new_tag_change_en_for_ldm_invalid_out[`R4] = ldm_invalid_but_updating_r4 & 

or_valid_instr_above_ldm_invalid_tag_updating_r4 & ~new_tag_for_r4_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R4] = ldm_invalid_but_updating_r4 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r4 | new_tag_for_r4_retiring_other_pipe);
/******Tag change for R4******/


/******Tag change for R5******/
assign ldm_invalid_but_updating_r5 = ldm_word_frm_pipe[`LDM_REG_R5] & 

or_invalid_ldm_instruction;


assign instr_updating_r5_but_current_ldm_invalid = instr_updating_r5_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r5}};


assign valid_instr_above_ldm_invalid_tag_updating_r5 = instr_updating_r5_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r5 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r5), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r5)
    );


assign instr_above_tail_updating_r5 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r5;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r5 (
    .decoded_input_in(instr_above_tail_updating_r5), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r5)
    );


assign instr_below_head_updating_r5 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r5;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r5 (
    .decoded_input_in(instr_below_head_updating_r5), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r5)
    );


assign new_tag_tail_greater_than_head_final_updating_r5 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r5 : 

new_tag_tail_greater_than_head_instr_below_head_r5;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R5_START:`TAG_FOR_R5_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r5 : 

new_tag_tail_greater_than_head_final_updating_r5;


assign new_tag_for_r5_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R5_START:`TAG_FOR_R5_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R5_START:`TAG_FOR_R5_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R5_START:`TAG_FOR_R5_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r5 = 

|(valid_instr_above_ldm_invalid_tag_updating_r5);


assign new_tag_change_en_for_ldm_invalid_out[`R5] = ldm_invalid_but_updating_r5 & 

or_valid_instr_above_ldm_invalid_tag_updating_r5 & ~new_tag_for_r5_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R5] = ldm_invalid_but_updating_r5 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r5 | new_tag_for_r5_retiring_other_pipe);
/******Tag change for R5******/


/******Tag change for R6******/
assign ldm_invalid_but_updating_r6 = ldm_word_frm_pipe[`LDM_REG_R6] & 

or_invalid_ldm_instruction;


assign instr_updating_r6_but_current_ldm_invalid = instr_updating_r6_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r6}};


assign valid_instr_above_ldm_invalid_tag_updating_r6 = instr_updating_r6_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r6 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r6), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r6)
    );


assign instr_above_tail_updating_r6 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r6;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r6 (
    .decoded_input_in(instr_above_tail_updating_r6), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r6)
    );


assign instr_below_head_updating_r6 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r6;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r6 (
    .decoded_input_in(instr_below_head_updating_r6), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r6)
    );


assign new_tag_tail_greater_than_head_final_updating_r6 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r6 : 

new_tag_tail_greater_than_head_instr_below_head_r6;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R6_START:`TAG_FOR_R6_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r6 : 

new_tag_tail_greater_than_head_final_updating_r6;


assign new_tag_for_r6_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R6_START:`TAG_FOR_R6_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R6_START:`TAG_FOR_R6_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R6_START:`TAG_FOR_R6_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r6 = 

|(valid_instr_above_ldm_invalid_tag_updating_r6);


assign new_tag_change_en_for_ldm_invalid_out[`R6] = ldm_invalid_but_updating_r6 & 

or_valid_instr_above_ldm_invalid_tag_updating_r6 & ~new_tag_for_r6_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R6] = ldm_invalid_but_updating_r6 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r6 | new_tag_for_r6_retiring_other_pipe);
/******Tag change for R6******/


/******Tag change for R7******/
assign ldm_invalid_but_updating_r7 = ldm_word_frm_pipe[`LDM_REG_R7] & 

or_invalid_ldm_instruction;


assign instr_updating_r7_but_current_ldm_invalid = instr_updating_r7_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r7}};


assign valid_instr_above_ldm_invalid_tag_updating_r7 = instr_updating_r7_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r7 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r7), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r7)
    );


assign instr_above_tail_updating_r7 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r7;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r7 (
    .decoded_input_in(instr_above_tail_updating_r7), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r7)
    );


assign instr_below_head_updating_r7 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r7;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r7 (
    .decoded_input_in(instr_below_head_updating_r7), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r7)
    );


assign new_tag_tail_greater_than_head_final_updating_r7 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r7 : 

new_tag_tail_greater_than_head_instr_below_head_r7;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R7_START:`TAG_FOR_R7_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r7 : 

new_tag_tail_greater_than_head_final_updating_r7;


assign new_tag_for_r7_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R7_START:`TAG_FOR_R7_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R7_START:`TAG_FOR_R7_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R7_START:`TAG_FOR_R7_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r7 = 

|(valid_instr_above_ldm_invalid_tag_updating_r7);


assign new_tag_change_en_for_ldm_invalid_out[`R7] = ldm_invalid_but_updating_r7 & 

or_valid_instr_above_ldm_invalid_tag_updating_r7 & ~new_tag_for_r7_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R7] = ldm_invalid_but_updating_r7 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r7 | new_tag_for_r7_retiring_other_pipe);
/******Tag change for R7******/


/******Tag change for R8******/
assign ldm_invalid_but_updating_r8 = ldm_word_frm_pipe[`LDM_REG_R8] & 

or_invalid_ldm_instruction;


assign instr_updating_r8_but_current_ldm_invalid = instr_updating_r8_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r8}};


assign valid_instr_above_ldm_invalid_tag_updating_r8 = instr_updating_r8_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r8 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r8), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r8)
    );


assign instr_above_tail_updating_r8 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r8;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r8 (
    .decoded_input_in(instr_above_tail_updating_r8), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r8)
    );


assign instr_below_head_updating_r8 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r8;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r8 (
    .decoded_input_in(instr_below_head_updating_r8), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r8)
    );


assign new_tag_tail_greater_than_head_final_updating_r8 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r8 : 

new_tag_tail_greater_than_head_instr_below_head_r8;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R8_START:`TAG_FOR_R8_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r8 : 

new_tag_tail_greater_than_head_final_updating_r8;


assign new_tag_for_r8_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R8_START:`TAG_FOR_R8_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R8_START:`TAG_FOR_R8_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R8_START:`TAG_FOR_R8_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r8 = 

|(valid_instr_above_ldm_invalid_tag_updating_r8);


assign new_tag_change_en_for_ldm_invalid_out[`R8] = ldm_invalid_but_updating_r8 & 

or_valid_instr_above_ldm_invalid_tag_updating_r8 & ~new_tag_for_r8_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R8] = ldm_invalid_but_updating_r8 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r8 | new_tag_for_r8_retiring_other_pipe);
/******Tag change for R8******/


/******Tag change for R9******/
assign ldm_invalid_but_updating_r9 = ldm_word_frm_pipe[`LDM_REG_R9] & 

or_invalid_ldm_instruction;


assign instr_updating_r9_but_current_ldm_invalid = instr_updating_r9_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r9}};


assign valid_instr_above_ldm_invalid_tag_updating_r9 = instr_updating_r9_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r9 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r9), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r9)
    );


assign instr_above_tail_updating_r9 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r9;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r9 (
    .decoded_input_in(instr_above_tail_updating_r9), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r9)
    );


assign instr_below_head_updating_r9 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r9;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r9 (
    .decoded_input_in(instr_below_head_updating_r9), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r9)
    );


assign new_tag_tail_greater_than_head_final_updating_r9 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r9 : 

new_tag_tail_greater_than_head_instr_below_head_r9;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R9_START:`TAG_FOR_R9_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r9 : 

new_tag_tail_greater_than_head_final_updating_r9;


assign new_tag_for_r9_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R9_START:`TAG_FOR_R9_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R9_START:`TAG_FOR_R9_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R9_START:`TAG_FOR_R9_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r9 = 

|(valid_instr_above_ldm_invalid_tag_updating_r9);


assign new_tag_change_en_for_ldm_invalid_out[`R9] = ldm_invalid_but_updating_r9 & 

or_valid_instr_above_ldm_invalid_tag_updating_r9 & ~new_tag_for_r9_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R9] = ldm_invalid_but_updating_r9 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r9 | new_tag_for_r9_retiring_other_pipe);
/******Tag change for R9******/


/******Tag change for R10******/
assign ldm_invalid_but_updating_r10 = ldm_word_frm_pipe[`LDM_REG_R10] & 

or_invalid_ldm_instruction;


assign instr_updating_r10_but_current_ldm_invalid = instr_updating_r10_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r10}};


assign valid_instr_above_ldm_invalid_tag_updating_r10 = instr_updating_r10_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r10 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r10), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r10)
    );


assign instr_above_tail_updating_r10 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r10;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r10 (
    .decoded_input_in(instr_above_tail_updating_r10), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r10)
    );


assign instr_below_head_updating_r10 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r10;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r10 (
    .decoded_input_in(instr_below_head_updating_r10), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r10)
    );


assign new_tag_tail_greater_than_head_final_updating_r10 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r10 : 

new_tag_tail_greater_than_head_instr_below_head_r10;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R10_START:`TAG_FOR_R10_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r10 : 

new_tag_tail_greater_than_head_final_updating_r10;


assign new_tag_for_r10_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R10_START:`TAG_FOR_R10_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R10_START:`TAG_FOR_R10_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R10_START:`TAG_FOR_R10_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r10 = 

|(valid_instr_above_ldm_invalid_tag_updating_r10);


assign new_tag_change_en_for_ldm_invalid_out[`R10] = ldm_invalid_but_updating_r10 & 

or_valid_instr_above_ldm_invalid_tag_updating_r10 & ~new_tag_for_r10_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R10] = ldm_invalid_but_updating_r10 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r10 | new_tag_for_r10_retiring_other_pipe);
/******Tag change for R10******/


/******Tag change for R11******/
assign ldm_invalid_but_updating_r11 = ldm_word_frm_pipe[`LDM_REG_R11] & 

or_invalid_ldm_instruction;


assign instr_updating_r11_but_current_ldm_invalid = instr_updating_r11_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r11}};


assign valid_instr_above_ldm_invalid_tag_updating_r11 = instr_updating_r11_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r11 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r11), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r11)
    );


assign instr_above_tail_updating_r11 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r11;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r11 (
    .decoded_input_in(instr_above_tail_updating_r11), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r11)
    );


assign instr_below_head_updating_r11 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r11;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r11 (
    .decoded_input_in(instr_below_head_updating_r11), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r11)
    );


assign new_tag_tail_greater_than_head_final_updating_r11 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r11 : 

new_tag_tail_greater_than_head_instr_below_head_r11;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R11_START:`TAG_FOR_R11_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r11 : 

new_tag_tail_greater_than_head_final_updating_r11;


assign new_tag_for_r11_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R11_START:`TAG_FOR_R11_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R11_START:`TAG_FOR_R11_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R11_START:`TAG_FOR_R11_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r11 = 

|(valid_instr_above_ldm_invalid_tag_updating_r11);


assign new_tag_change_en_for_ldm_invalid_out[`R11] = ldm_invalid_but_updating_r11 & 

or_valid_instr_above_ldm_invalid_tag_updating_r11 & ~new_tag_for_r11_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R11] = ldm_invalid_but_updating_r11 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r11 | new_tag_for_r11_retiring_other_pipe);
/******Tag change for R11******/


/******Tag change for R12******/
assign ldm_invalid_but_updating_r12 = ldm_word_frm_pipe[`LDM_REG_R12] & 

or_invalid_ldm_instruction;


assign instr_updating_r12_but_current_ldm_invalid = instr_updating_r12_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r12}};


assign valid_instr_above_ldm_invalid_tag_updating_r12 = instr_updating_r12_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r12 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r12), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r12)
    );


assign instr_above_tail_updating_r12 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r12;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r12 (
    .decoded_input_in(instr_above_tail_updating_r12), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r12)
    );


assign instr_below_head_updating_r12 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r12;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r12 (
    .decoded_input_in(instr_below_head_updating_r12), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r12)
    );


assign new_tag_tail_greater_than_head_final_updating_r12 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r12 : 

new_tag_tail_greater_than_head_instr_below_head_r12;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R12_START:`TAG_FOR_R12_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r12 : 

new_tag_tail_greater_than_head_final_updating_r12;


assign new_tag_for_r12_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R12_START:`TAG_FOR_R12_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R12_START:`TAG_FOR_R12_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R12_START:`TAG_FOR_R12_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r12 = 

|(valid_instr_above_ldm_invalid_tag_updating_r12);


assign new_tag_change_en_for_ldm_invalid_out[`R12] = ldm_invalid_but_updating_r12 & 

or_valid_instr_above_ldm_invalid_tag_updating_r12 & ~new_tag_for_r12_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R12] = ldm_invalid_but_updating_r12 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r12 | new_tag_for_r12_retiring_other_pipe);
/******Tag change for R12******/


/******Tag change for R13******/
assign ldm_invalid_but_updating_r13 = ldm_word_frm_pipe[`LDM_REG_R13] & 

or_invalid_ldm_instruction;


assign instr_updating_r13_but_current_ldm_invalid = instr_updating_r13_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r13}};


assign valid_instr_above_ldm_invalid_tag_updating_r13 = instr_updating_r13_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r13 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r13), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r13)
    );


assign instr_above_tail_updating_r13 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r13;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r13 (
    .decoded_input_in(instr_above_tail_updating_r13), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r13)
    );


assign instr_below_head_updating_r13 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r13;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r13 (
    .decoded_input_in(instr_below_head_updating_r13), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r13)
    );


assign new_tag_tail_greater_than_head_final_updating_r13 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r13 : 

new_tag_tail_greater_than_head_instr_below_head_r13;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R13_START:`TAG_FOR_R13_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r13 : 

new_tag_tail_greater_than_head_final_updating_r13;


assign new_tag_for_r13_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R13_START:`TAG_FOR_R13_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R13_START:`TAG_FOR_R13_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R13_START:`TAG_FOR_R13_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r13 = 

|(valid_instr_above_ldm_invalid_tag_updating_r13);


assign new_tag_change_en_for_ldm_invalid_out[`R13] = ldm_invalid_but_updating_r13 & 

or_valid_instr_above_ldm_invalid_tag_updating_r13 & ~new_tag_for_r13_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R13] = ldm_invalid_but_updating_r13 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r13 | new_tag_for_r13_retiring_other_pipe);
/******Tag change for R13******/


/******Tag change for R14******/
assign ldm_invalid_but_updating_r14 = ldm_word_frm_pipe[`LDM_REG_R14] & 

or_invalid_ldm_instruction;


assign instr_updating_r14_but_current_ldm_invalid = instr_updating_r14_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r14}};


assign valid_instr_above_ldm_invalid_tag_updating_r14 = instr_updating_r14_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r14 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r14), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r14)
    );


assign instr_above_tail_updating_r14 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r14;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r14 (
    .decoded_input_in(instr_above_tail_updating_r14), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r14)
    );


assign instr_below_head_updating_r14 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r14;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r14 (
    .decoded_input_in(instr_below_head_updating_r14), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r14)
    );


assign new_tag_tail_greater_than_head_final_updating_r14 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r14 : 

new_tag_tail_greater_than_head_instr_below_head_r14;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R14_START:`TAG_FOR_R14_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r14 : 

new_tag_tail_greater_than_head_final_updating_r14;


assign new_tag_for_r14_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R14_START:`TAG_FOR_R14_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R14_START:`TAG_FOR_R14_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R14_START:`TAG_FOR_R14_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r14 = 

|(valid_instr_above_ldm_invalid_tag_updating_r14);


assign new_tag_change_en_for_ldm_invalid_out[`R14] = ldm_invalid_but_updating_r14 & 

or_valid_instr_above_ldm_invalid_tag_updating_r14 & ~new_tag_for_r14_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R14] = ldm_invalid_but_updating_r14 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r14 | new_tag_for_r14_retiring_other_pipe);
/******Tag change for R14******/


/******Tag change for R15******/
assign ldm_invalid_but_updating_r15 = ldm_word_frm_pipe[`LDM_REG_R15] & 

or_invalid_ldm_instruction;


assign instr_updating_r15_but_current_ldm_invalid = instr_updating_r15_pipe_in & 

{`RB_SIZE{ldm_invalid_but_updating_r15}};


assign valid_instr_above_ldm_invalid_tag_updating_r15 = instr_updating_r15_but_current_ldm_invalid & 

valid_instr_above_tag_pipe_in;


priority_encoder_24_1 priority_encoder_for_new_tag_head_greater_than_tail_r15 (
    .decoded_input_in(valid_instr_above_ldm_invalid_tag_updating_r15), 
    .priority_encoded_out(new_tag_head_greater_than_tail_r15)
    );


assign instr_above_tail_updating_r15 = all_above_tail_in & 

valid_instr_above_ldm_invalid_tag_updating_r15;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_above_tail_r15 (
    .decoded_input_in(instr_above_tail_updating_r15), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_above_tail_r15)
    );


assign instr_below_head_updating_r15 = all_below_head_in & 

valid_instr_above_ldm_invalid_tag_updating_r15;


priority_encoder_24_1 priority_encoder_for_new_tag_tail_greater_than_head_instr_below_head_r15 (
    .decoded_input_in(instr_below_head_updating_r15), 
    .priority_encoded_out(new_tag_tail_greater_than_head_instr_below_head_r15)
    );


assign new_tag_tail_greater_than_head_final_updating_r15 = or_invalid_instr_above_tail_pipe_in ? 

new_tag_tail_greater_than_head_instr_above_tail_r15 : 

new_tag_tail_greater_than_head_instr_below_head_r15;


assign new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R15_START:`TAG_FOR_R15_END] = 

head_greater_than_tail_in ? new_tag_head_greater_than_tail_r15 : 

new_tag_tail_greater_than_head_final_updating_r15;


assign new_tag_for_r15_retiring_other_pipe = 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R15_START:`TAG_FOR_R15_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE1_STARTS:`TAG_PIPE1_ENDS])) & |(instr_other_pipe1_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R15_START:`TAG_FOR_R15_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE2_STARTS:`TAG_PIPE2_ENDS])) & |(instr_other_pipe2_valid_retire_in)) | 

(~(|(new_tag_to_change_all_reg_for_ldm_invalid_out[`TAG_FOR_R15_START:`TAG_FOR_R15_END] ^ 

tag_in_other_pipes_in[`TAG_PIPE3_STARTS:`TAG_PIPE3_ENDS])) & |(instr_other_pipe3_valid_retire_in));


assign or_valid_instr_above_ldm_invalid_tag_updating_r15 = 

|(valid_instr_above_ldm_invalid_tag_updating_r15);


assign new_tag_change_en_for_ldm_invalid_out[`R15] = ldm_invalid_but_updating_r15 & 

or_valid_instr_above_ldm_invalid_tag_updating_r15 & ~new_tag_for_r15_retiring_other_pipe;


assign tag_retire_en_for_ldm_invalid_out[`R15] = ldm_invalid_but_updating_r15 & 

(~or_valid_instr_above_ldm_invalid_tag_updating_r15 | new_tag_for_r15_retiring_other_pipe);
/******Tag change for R15******/


endmodule
