`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module decoding_all_below_vector_64(
						input [`decoder_output_64-1:0] input_vector_in,
						output [`decoder_output_64-1:0] all_below_input_vector_out
    );


genvar i;
generate
for(i=`decoder_output_64-2;i>=0;i=i-1)
begin	:	grp_all_below_input_vector
	assign all_below_input_vector_out[i] = |(input_vector_in[`decoder_output_64-1:i]);
end
endgenerate


assign all_below_input_vector_out[`decoder_output_64-1] = 1'b0;


endmodule
