`include "timescale.v"
`include "RB_info.v"

module head_tail_pointer_RB(
										input clk_in,
										input reset_in,
										
										/******Two new instructions are to be put in the ROB******/
										input new_entry_enable_in,
										
										/******Only one instruction can be committed******/
										input retire_1_instr_in, 
										
										/******Only one instruction can be committed******/
										input retire_2_instr_in, 
										
										/******Head Pointer data******/
										output [`RB_SIZE-1:0] head_pointer_out,
										
										/******Tail Pointer data******/
										output [`RB_SIZE-1:0] tail_pointer_out,
										
										/******RB_Full******/
										output RB_full_out,
										
										/******RB_Empty******/
										output RB_empty_out
    );

wire [`RB_SIZE-1:0] data_to_tail_pointer_reg;
wire [`RB_SIZE-1:0] data_to_head_pointer_reg;
wire [`RB_SIZE-1:0] data_to_head_pointer_retire2_1;
wire new_entry_enable_reg;

/************************TAIL_POINTER************************/
genvar i;
generate
for(i = 0;i<`RB_SIZE-2;i=i+1)
begin : grp_data_to_tail_pointer_reg
	assign data_to_tail_pointer_reg[i] = new_entry_enable_in ? 	tail_pointer_out[i+2] : 
	tail_pointer_out[i];
end
endgenerate

assign data_to_tail_pointer_reg[`RB_SIZE-1] = new_entry_enable_in ? tail_pointer_out[1] : 
tail_pointer_out[`RB_SIZE-1];
assign data_to_tail_pointer_reg[`RB_SIZE-2] = new_entry_enable_in ? tail_pointer_out[0] : 
tail_pointer_out[`RB_SIZE-2];
/******/

/******Tail pointer register******/
genvar j;
generate
for(j=`RB_SIZE-3;j>=0;j=j-1)
begin : grp_tail_pointer_out
	register_with_reset #1 reg_tail_pointer_out (
		 .data_in(data_to_tail_pointer_reg[j]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(new_entry_enable_in), 
		 .data_out(tail_pointer_out[j])
		 );
end
endgenerate

register_with_preset #1 reg_tail_pointer_2 (
		 .data_in(data_to_tail_pointer_reg[`RB_SIZE-2]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(new_entry_enable_in), 
		 .data_out(tail_pointer_out[`RB_SIZE-2])
		 );

register_with_preset #1 reg_tail_pointer_1 (
		 .data_in(data_to_tail_pointer_reg[`RB_SIZE-1]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(new_entry_enable_in), 
		 .data_out(tail_pointer_out[`RB_SIZE-1])
		 );

/******Tail pointer register******/

/************************TAIL_POINTER************************/

/************************HEAD_POINTER************************/
genvar k;
generate
for(k = 0;k<`RB_SIZE-2;k=k+1)
begin : grp_data_to_head_pointer_reg
	assign data_to_head_pointer_retire2_1[k] = retire_2_instr_in ? head_pointer_out[k+2] : 
	head_pointer_out[k+1];
	
	assign data_to_head_pointer_reg[k] = (retire_2_instr_in | retire_1_instr_in) ? 
	data_to_head_pointer_retire2_1[k] : head_pointer_out[k]; 
end
endgenerate

assign data_to_head_pointer_retire2_1[`RB_SIZE-1] = retire_2_instr_in ? head_pointer_out[1] : 
head_pointer_out[0];
assign data_to_head_pointer_retire2_1[`RB_SIZE-2] = retire_2_instr_in ? head_pointer_out[0] : 
head_pointer_out[`RB_SIZE-1];

assign data_to_head_pointer_reg[`RB_SIZE-1] = (retire_2_instr_in | retire_1_instr_in) ? 
data_to_head_pointer_retire2_1[`RB_SIZE-1] : head_pointer_out[`RB_SIZE-1];
assign data_to_head_pointer_reg[`RB_SIZE-2] = (retire_2_instr_in | retire_1_instr_in) ? 
data_to_head_pointer_retire2_1[`RB_SIZE-2] : head_pointer_out[`RB_SIZE-2];
/******/

/******Head pointer register******/
genvar l;
generate
for(l=`RB_SIZE-3;l>=0;l=l-1)
begin : grp_head_pointer_out
	register_with_reset #1 reg_head_pointer_out (
		 .data_in(data_to_head_pointer_reg[l]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(retire_2_instr_in | retire_1_instr_in), 
		 .data_out(head_pointer_out[l])
		 );
end
endgenerate

register_with_preset #1 reg_head_pointer_2 (
		 .data_in(data_to_head_pointer_reg[`RB_SIZE-2]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(retire_2_instr_in | retire_1_instr_in), 
		 .data_out(head_pointer_out[`RB_SIZE-2])
		 );

register_with_preset #1 reg_head_pointer_1 (
		 .data_in(data_to_head_pointer_reg[`RB_SIZE-1]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(retire_2_instr_in | retire_1_instr_in), 
		 .data_out(head_pointer_out[`RB_SIZE-1])
		 );

/******Head pointer register******/

/************************HEAD_POINTER************************/

/******New entry enable register******/	
register_with_reset #1 reg_new_entry_enable (
		 .data_in(new_entry_enable_in), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(1'b1), 
		 .data_out(new_entry_enable_reg)
		 );	
/******New entry enable register******/

/******Retire 1 instr register******/	
register_with_reset #1 reg_retire_1_instr (
		 .data_in(retire_1_instr_in), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(1'b1), 
		 .data_out(reg_retire_1_instr_reg)
		 );	
/******Retire 1 instr register******/

/******Retire 2 instrs register******/	
register_with_reset #1 reg_retire_2_instr (
		 .data_in(retire_2_instr_in), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(1'b1), 
		 .data_out(reg_retire_2_instr_reg)
		 );	
/******Retire 2 instrs register******/

assign temp = |(head_pointer_out & tail_pointer_out);

assign RB_full_out = temp & new_entry_enable_reg;
assign RB_empty_out = temp & (reg_retire_1_instr_reg | reg_retire_2_instr_reg);

endmodule
