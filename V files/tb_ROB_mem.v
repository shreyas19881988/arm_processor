`include "timescale.v"
`include "RB_info.v"

module tb_ROB_mem;

	// Inputs
	reg clk_in;
	reg reset_in;
	reg new_entry_enable_in;
	reg [`RB_ENTRY_WORD-1:0] RB_entry1_info_in;
	reg [`RB_ENTRY_WORD-1:0] RB_entry2_info_in;
	
	wire x;

	// Instantiate the Unit Under Test (UUT)
	ROB_mem uut (
		.clk_in(clk_in), 
		.reset_in(reset_in), 
		.new_entry_enable_in(new_entry_enable_in), 
		.RB_entry1_info_in(RB_entry1_info_in), 
		.RB_entry2_info_in(RB_entry2_info_in)
		//.x(x)
	);

	initial begin
		// Initialize Inputs
		clk_in = 0;
		reset_in = 1;
		new_entry_enable_in = 0;
		RB_entry1_info_in = 0;
		RB_entry2_info_in = 0;

		// Wait 100 ns for global reset to finish
		#105;
      reset_in <= 0;
      #10
		new_entry_enable_in <= 1;
		RB_entry1_info_in <= 23'b1_00000_0000_0000_0000_0011_1;
		RB_entry2_info_in <= 23'b1_00001_0000_0000_0000_0010_1;;
		// Add stimulus here

	end
      
	always #5 clk_in = ~clk_in;	
		
endmodule

