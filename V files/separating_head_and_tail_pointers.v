`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module separating_head_and_tail_pointers(
														input [`RB_SIZE-1:0] head_pointer_in,
														input [`RB_SIZE-1:0] tail_pointer_in,
														output [`RB_SIZE-1:0] head_pointer1_out,
														output [`RB_SIZE-1:0] head_pointer2_out,
														output [`RB_SIZE-1:0] tail_pointer1_out,
														output [`RB_SIZE-1:0] tail_pointer2_out
    );

wire [`RB_SIZE-1:0] tail_pointer_instr1;
wire [`RB_SIZE-1:0] tail_pointer_instr2;
wire [`RB_SIZE-1:0] head_pointer_instr1;
wire [`RB_SIZE-1:0] head_pointer_instr2;

genvar i;
generate
for(i=`RB_SIZE-1;i>0;i=i-1)
begin : grp_tail_pointer_1
	assign tail_pointer1_out[i-1] = (~tail_pointer_in[i] & tail_pointer_in[i-1]);
	
	
	assign head_pointer1_out[i-1] = (~head_pointer_in[i] & head_pointer_in[i-1]);
end
endgenerate


assign tail_pointer1_out[`RB_SIZE-1] = (~tail_pointer_in[0] & tail_pointer_in[`RB_SIZE-1]);


assign head_pointer1_out[`RB_SIZE-1] = (~head_pointer_in[0] & head_pointer_in[`RB_SIZE-1]);


genvar j;
generate
for(j=0;j<`RB_SIZE-1;j=j+1)
begin : grp_tail_pointer_2
	assign tail_pointer2_out[j+1] = (~tail_pointer_in[j] & tail_pointer_in[j+1]);
	
	
	assign head_pointer2_out[j+1] = (~head_pointer_in[j] & head_pointer_in[j+1]);
end
endgenerate

assign tail_pointer2_out[0] = (~tail_pointer_in[`RB_SIZE-1] & tail_pointer_in[0]);


assign head_pointer2_out[0] = (~head_pointer_in[`RB_SIZE-1] & head_pointer_in[0]);

endmodule
