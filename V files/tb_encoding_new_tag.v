`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module tb_encoding_new_tag;

	// Inputs
	reg [`RB_SIZE-1:0] all_instr_updating_reg_in;
	reg head_greater_than_tail_in;
	reg [`RB_SIZE-1:0] all_above_tail_in;
	reg [`RB_SIZE-1:0] all_below_head_in;
	reg [`RB_SIZE-1:0] invalid_instr_in;

	// Outputs
	wire [`TAG_SIZE-1:0] new_tag_out;

	// Instantiate the Unit Under Test (UUT)
	encoding_new_tag uut (
		.all_instr_updating_reg_in(all_instr_updating_reg_in), 
		.head_greater_than_tail_in(head_greater_than_tail_in), 
		.all_above_tail_in(all_above_tail_in), 
		.all_below_head_in(all_below_head_in), 
		.invalid_instr_in(invalid_instr_in),
		.new_tag_out(new_tag_out)
	);

	initial begin
		// Initialize Inputs
		all_instr_updating_reg_in = 0;
		head_greater_than_tail_in = 0;
		all_above_tail_in = 0;
		all_below_head_in = 0;
		invalid_instr_in = 0;

		// Wait 100 ns for global reset to finish
		#100;
      all_instr_updating_reg_in <= `RB_SIZE'b0000_1001_0010_0100_1000_0000;
		head_greater_than_tail_in = 1'b1;
		#100;
      all_instr_updating_reg_in <= `RB_SIZE'b0000_1001_1000_0000_0001_0000;
		head_greater_than_tail_in = 1'b0;
		all_below_head_in <= `RB_SIZE'b0000_0000_0000_0111_1111_1111;
		all_above_tail_in <= `RB_SIZE'b1111_1111_1111_0000_0000_0000;
		invalid_instr_in <= `RB_SIZE'b0000_0000_0000_0000_0000_0010;
		// Add stimulus here

	end
      
endmodule

