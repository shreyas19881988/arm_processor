`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module free_list_controller_alu_pipes(
			input clk_in,
			input reset_in,
			input reg_issue_alu_pipe1_rd_in,
			input reg_issue_alu_pipe1_rn_in,
			input reg_issue_alu_pipe2_rd_in,
			input reg_issue_alu_pipe2_rn_in,
			input [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] retired_instr1_rd_addr_in,
			input retire_en_instr1_rd_in,
			input [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] retired_instr1_rn_addr_in,
			input retire_en_instr1_rn_in,
			input [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] retired_instr2_rd_addr_in,
			input retire_en_instr2_rd_in,
			input [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] retired_instr2_rn_addr_in,
			input retire_en_instr2_rn_in,
			output [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] reg_addr_issued_to_alu_pipe1_rd_out,
			output [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] reg_addr_issued_to_alu_pipe1_rn_out,
			output [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] reg_addr_issued_to_alu_pipe2_rd_out,
			output [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] reg_addr_issued_to_alu_pipe2_rn_out
										
    );


wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] register_file_addr [`ALU_PIPES_REG_FILE_SIZE-1:0];
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] free_bits;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] data_for_free_bit;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] issue_pointer_for_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] issue_pointer_for_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] issue_pointer_for_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] issue_pointer_for_alu_pipe2_rn;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] anded_file_addr_alu_pipe1_rd [`ALU_PIPES_REG_FILE_SIZE-1:0];
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] anded_file_addr_alu_pipe1_rn [`ALU_PIPES_REG_FILE_SIZE-1:0];
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] anded_file_addr_alu_pipe2_rd [`ALU_PIPES_REG_FILE_SIZE-1:0];
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] anded_file_addr_alu_pipe2_rn [`ALU_PIPES_REG_FILE_SIZE-1:0];
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_0_anded_file_addr_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_1_anded_file_addr_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_2_anded_file_addr_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_3_anded_file_addr_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_4_anded_file_addr_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_5_anded_file_addr_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_0_anded_file_addr_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_1_anded_file_addr_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_2_anded_file_addr_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_3_anded_file_addr_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_4_anded_file_addr_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_5_anded_file_addr_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_0_anded_file_addr_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_1_anded_file_addr_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_2_anded_file_addr_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_3_anded_file_addr_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_4_anded_file_addr_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_5_anded_file_addr_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_0_anded_file_addr_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_1_anded_file_addr_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_2_anded_file_addr_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_3_anded_file_addr_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_4_anded_file_addr_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] orred_5_anded_file_addr_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] reset_for_free_bits_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] reset_for_free_bits_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] reset_for_free_bits_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] reset_for_free_bits_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rd_0;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rd_1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rd_2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rd_3;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rd_4;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rd_5;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rn_0;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rn_1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rn_2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rn_3;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rn_4;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe1_rn_5;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rd_0;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rd_1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rd_2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rd_3;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rd_4;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rd_5;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rn_0;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rn_1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rn_2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rn_3;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rn_4;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_alu_pipe2_rn_5;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rd_0;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rd_1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rd_2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rd_3;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rd_4;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rd_5;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rn_0;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rn_1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rn_2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rn_3;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rn_4;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe1_rn_5;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rd_0;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rd_1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rd_2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rd_3;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rd_4;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rd_5;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rn_0;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rn_1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rn_2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rn_3;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rn_4;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] next_hamming_reg_free_alu_pipe2_rn_5;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rd_0;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rd_1;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rd_2;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rd_3;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rd_4;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rd_5;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rn_0;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rn_1;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rn_2;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rn_3;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rn_4;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe1_rn_5;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rd_0;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rd_1;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rd_2;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rd_3;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rd_4;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rd_5;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rn_0;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rn_1;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rn_2;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rn_3;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rn_4;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_hamming_reg_alu_pipe2_rn_5;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] all_below_issue_pointer_for_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] all_below_issue_pointer_for_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] all_below_issue_pointer_for_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] all_below_issue_pointer_for_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] all_reg_below_issue_pointer_free_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] all_reg_below_issue_pointer_free_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] all_reg_below_issue_pointer_free_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] all_reg_below_issue_pointer_free_alu_pipe2_rn;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_reg_addr_to_point_alu_pipe1_rd;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_reg_addr_to_point_alu_pipe1_rn;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_reg_addr_to_point_alu_pipe2_rd;
wire [`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:0] next_reg_addr_to_point_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_not_free_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_not_free_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_not_free_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] new_issue_position_hamming_not_free_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] data_to_issue_pointer_alu_pipe1_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] data_to_issue_pointer_alu_pipe1_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] data_to_issue_pointer_alu_pipe2_rd;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] data_to_issue_pointer_alu_pipe2_rn;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] decoded_rd_addr_instr1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] decoded_rn_addr_instr1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] decoded_rd_addr_instr2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] decoded_rn_addr_instr2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] retire_decoded_rd_addr_instr1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] retire_decoded_rn_addr_instr1;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] retire_decoded_rd_addr_instr2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] retire_decoded_rn_addr_instr2;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] combined_reset_for_free_bits;
wire [`ALU_PIPES_REG_FILE_SIZE-1:0] en_free_bits;


assign register_file_addr[63] = 0;
assign register_file_addr[62] = 1;
assign register_file_addr[61] = 2;
assign register_file_addr[60] = 3;
assign register_file_addr[59] = 4;
assign register_file_addr[58] = 5;
assign register_file_addr[57] = 6;
assign register_file_addr[56] = 7;
assign register_file_addr[55] = 8;
assign register_file_addr[54] = 9;
assign register_file_addr[53] = 10;
assign register_file_addr[52] = 11;
assign register_file_addr[51] = 12;
assign register_file_addr[50] = 13;
assign register_file_addr[49] = 14;
assign register_file_addr[48] = 15;
assign register_file_addr[47] = 16;
assign register_file_addr[46] = 17;
assign register_file_addr[45] = 18;
assign register_file_addr[44] = 19;
assign register_file_addr[43] = 20;
assign register_file_addr[42] = 21;
assign register_file_addr[41] = 22;
assign register_file_addr[40] = 23;
assign register_file_addr[39] = 24;
assign register_file_addr[38] = 25;
assign register_file_addr[37] = 26;
assign register_file_addr[36] = 27;
assign register_file_addr[35] = 28;
assign register_file_addr[34] = 29;
assign register_file_addr[33] = 30;
assign register_file_addr[32] = 31;
assign register_file_addr[31] = 32;
assign register_file_addr[30] = 33;
assign register_file_addr[29] = 34;
assign register_file_addr[28] = 35;
assign register_file_addr[27] = 36;
assign register_file_addr[26] = 37;
assign register_file_addr[25] = 38;
assign register_file_addr[24] = 39;
assign register_file_addr[23] = 40;
assign register_file_addr[22] = 41;
assign register_file_addr[21] = 42;
assign register_file_addr[20] = 43;
assign register_file_addr[19] = 44;
assign register_file_addr[18] = 45;
assign register_file_addr[17] = 46;
assign register_file_addr[16] = 47;
assign register_file_addr[15] = 48;
assign register_file_addr[14] = 49;
assign register_file_addr[13] = 50;
assign register_file_addr[12] = 51;
assign register_file_addr[11] = 52;
assign register_file_addr[10] = 53;
assign register_file_addr[9] = 54;
assign register_file_addr[8] = 55;
assign register_file_addr[7] = 56;
assign register_file_addr[6] = 57;
assign register_file_addr[5] = 58;
assign register_file_addr[4] = 59;
assign register_file_addr[3] = 60;
assign register_file_addr[2] = 61;
assign register_file_addr[1] = 62;
assign register_file_addr[0] = 63;

/******ALU_PIPE1_BEGINS******/

/******RD_BEGINS******/

genvar i;
generate
for(i=`ALU_PIPES_REG_FILE_SIZE-1;i>=0;i=i-1)
begin 
	assign anded_file_addr_alu_pipe1_rd[i] = register_file_addr[i] & 
	
	{`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE{issue_pointer_for_alu_pipe1_rd[i]}};
end
endgenerate


genvar j;
generate
for(j=`ALU_PIPES_REG_FILE_SIZE-1;j>=0;j=j-1)
begin 
	assign orred_0_anded_file_addr_alu_pipe1_rd[j] = anded_file_addr_alu_pipe1_rd[j][0];
	assign orred_1_anded_file_addr_alu_pipe1_rd[j] = anded_file_addr_alu_pipe1_rd[j][1];
	assign orred_2_anded_file_addr_alu_pipe1_rd[j] = anded_file_addr_alu_pipe1_rd[j][2];
	assign orred_3_anded_file_addr_alu_pipe1_rd[j] = anded_file_addr_alu_pipe1_rd[j][3];
	assign orred_4_anded_file_addr_alu_pipe1_rd[j] = anded_file_addr_alu_pipe1_rd[j][4];
	assign orred_5_anded_file_addr_alu_pipe1_rd[j] = anded_file_addr_alu_pipe1_rd[j][5];
end
endgenerate


assign reg_addr_issued_to_alu_pipe1_rd_out = {|(orred_5_anded_file_addr_alu_pipe1_rd),

|(orred_4_anded_file_addr_alu_pipe1_rd),|(orred_3_anded_file_addr_alu_pipe1_rd),

|(orred_2_anded_file_addr_alu_pipe1_rd),|(orred_1_anded_file_addr_alu_pipe1_rd),

|(orred_0_anded_file_addr_alu_pipe1_rd)};


decoder_64 decoder_for_free_bit_reset_alu_pipe1_rd (
    .addr_in(reg_addr_issued_to_alu_pipe1_rd_out), 
    .decoded_addr_out(reset_for_free_bits_alu_pipe1_rd)
    );


assign next_hamming_reg_alu_pipe1_rd_0[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:1] = 

reg_addr_issued_to_alu_pipe1_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:1];


assign next_hamming_reg_alu_pipe1_rd_0[0] = ~reg_addr_issued_to_alu_pipe1_rd_out[0];


assign next_hamming_reg_alu_pipe1_rd_1[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:2] = 

reg_addr_issued_to_alu_pipe1_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:2];


assign next_hamming_reg_alu_pipe1_rd_1[1] = ~reg_addr_issued_to_alu_pipe1_rd_out[1];


assign next_hamming_reg_alu_pipe1_rd_1[0] = reg_addr_issued_to_alu_pipe1_rd_out[0];


assign next_hamming_reg_alu_pipe1_rd_2[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:3] = 

reg_addr_issued_to_alu_pipe1_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:3];


assign next_hamming_reg_alu_pipe1_rd_2[2] = ~reg_addr_issued_to_alu_pipe1_rd_out[2];


assign next_hamming_reg_alu_pipe1_rd_2[1:0] = reg_addr_issued_to_alu_pipe1_rd_out[1:0];


assign next_hamming_reg_alu_pipe1_rd_3[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:4] = 

reg_addr_issued_to_alu_pipe1_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:4];


assign next_hamming_reg_alu_pipe1_rd_3[3] = ~reg_addr_issued_to_alu_pipe1_rd_out[3];


assign next_hamming_reg_alu_pipe1_rd_3[2:0] = reg_addr_issued_to_alu_pipe1_rd_out[2:0];


assign next_hamming_reg_alu_pipe1_rd_4[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1] = 

reg_addr_issued_to_alu_pipe1_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1];


assign next_hamming_reg_alu_pipe1_rd_4[4] = ~reg_addr_issued_to_alu_pipe1_rd_out[4];


assign next_hamming_reg_alu_pipe1_rd_4[3:0] = reg_addr_issued_to_alu_pipe1_rd_out[3:0];


assign next_hamming_reg_alu_pipe1_rd_5[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1] = 

~reg_addr_issued_to_alu_pipe1_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1];


assign next_hamming_reg_alu_pipe1_rd_5[4:0] = reg_addr_issued_to_alu_pipe1_rd_out[4:0];


decoder_64 decoder_for_next_hamming_alu_pipe1_rd_0 (
    .addr_in(next_hamming_reg_alu_pipe1_rd_0), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rd_0)
    );
	 

decoder_64 decoder_for_next_hamming_alu_pipe1_rd_1 (
    .addr_in(next_hamming_reg_alu_pipe1_rd_1), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rd_1)
    );
	 

decoder_64 decoder_for_next_hamming_alu_pipe1_rd_2 (
    .addr_in(next_hamming_reg_alu_pipe1_rd_2), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rd_2)
    );
	 

decoder_64 decoder_for_next_hamming_alu_pipe1_rd_3 (
    .addr_in(next_hamming_reg_alu_pipe1_rd_3), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rd_3)
    );


decoder_64 decoder_for_next_hamming_alu_pipe1_rd_4 (
    .addr_in(next_hamming_reg_alu_pipe1_rd_4), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rd_4)
    );


decoder_64 decoder_for_next_hamming_alu_pipe1_rd_5 (
    .addr_in(next_hamming_reg_alu_pipe1_rd_5), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rd_5)
    );



assign next_hamming_reg_free_alu_pipe1_rd_0 = new_issue_position_hamming_alu_pipe1_rd_0 & free_bits;


assign next_hamming_reg_free_alu_pipe1_rd_1 = new_issue_position_hamming_alu_pipe1_rd_1 & free_bits;


assign next_hamming_reg_free_alu_pipe1_rd_2 = new_issue_position_hamming_alu_pipe1_rd_2 & free_bits;


assign next_hamming_reg_free_alu_pipe1_rd_3 = new_issue_position_hamming_alu_pipe1_rd_3 & free_bits;


assign next_hamming_reg_free_alu_pipe1_rd_4 = new_issue_position_hamming_alu_pipe1_rd_4 & free_bits;


assign next_hamming_reg_free_alu_pipe1_rd_5 = new_issue_position_hamming_alu_pipe1_rd_5 & free_bits;


decoding_all_below_vector_64 all_below_issue_pointer_alu_pipe1_rd (
    .input_vector_in(issue_pointer_for_alu_pipe1_rd), 
    .all_below_input_vector_out(all_below_issue_pointer_for_alu_pipe1_rd)
    );


assign all_reg_below_issue_pointer_free_alu_pipe1_rd = all_below_issue_pointer_for_alu_pipe1_rd & 

free_bits;


priority_encoder_64 priority_encoder_for_next_reg_addr_to_point_issue_pointer_alu_pipe1_rd (
    .decoded_input_in(all_reg_below_issue_pointer_free_alu_pipe1_rd), 
    .priority_encoded_out(next_reg_addr_to_point_alu_pipe1_rd)
    );


decoder_64 decoder_for_next_non_hamming_alu_pipe1_rd (
    .addr_in(next_reg_addr_to_point_alu_pipe1_rd), 
    .decoded_addr_out(new_issue_position_hamming_not_free_alu_pipe1_rd)
    );


assign data_to_issue_pointer_alu_pipe1_rd = (|(next_hamming_reg_free_alu_pipe1_rd_0) ? 

new_issue_position_hamming_alu_pipe1_rd_0 : (|(next_hamming_reg_free_alu_pipe1_rd_1) ? 

new_issue_position_hamming_alu_pipe1_rd_1 : (|(next_hamming_reg_free_alu_pipe1_rd_2) ? 

new_issue_position_hamming_alu_pipe1_rd_2 : (|(next_hamming_reg_free_alu_pipe1_rd_3) ? 

new_issue_position_hamming_alu_pipe1_rd_3 : (|(next_hamming_reg_free_alu_pipe1_rd_4) ? 

new_issue_position_hamming_alu_pipe1_rd_4 : (|(next_hamming_reg_free_alu_pipe1_rd_5) ? 

new_issue_position_hamming_alu_pipe1_rd_5 : new_issue_position_hamming_not_free_alu_pipe1_rd))))));

/******RD_ENDS******/


/******RN_BEGINS******/

genvar k;
generate
for(k=`ALU_PIPES_REG_FILE_SIZE-1;k>=0;k=k-1)
begin 
	assign anded_file_addr_alu_pipe1_rn[k] = register_file_addr[k] & 
	
	{`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE{issue_pointer_for_alu_pipe1_rn[k]}};
end
endgenerate


genvar l;
generate
for(l=`ALU_PIPES_REG_FILE_SIZE-1;l>=0;l=l-1)
begin 
	assign orred_0_anded_file_addr_alu_pipe1_rn[l] = anded_file_addr_alu_pipe1_rn[l][0];
	assign orred_1_anded_file_addr_alu_pipe1_rn[l] = anded_file_addr_alu_pipe1_rn[l][1];
	assign orred_2_anded_file_addr_alu_pipe1_rn[l] = anded_file_addr_alu_pipe1_rn[l][2];
	assign orred_3_anded_file_addr_alu_pipe1_rn[l] = anded_file_addr_alu_pipe1_rn[l][3];
	assign orred_4_anded_file_addr_alu_pipe1_rn[l] = anded_file_addr_alu_pipe1_rn[l][4];
	assign orred_5_anded_file_addr_alu_pipe1_rn[l] = anded_file_addr_alu_pipe1_rn[l][5];
end
endgenerate


assign reg_addr_issued_to_alu_pipe1_rn_out = {|(orred_5_anded_file_addr_alu_pipe1_rn),

|(orred_4_anded_file_addr_alu_pipe1_rn),|(orred_3_anded_file_addr_alu_pipe1_rn),

|(orred_2_anded_file_addr_alu_pipe1_rn),|(orred_1_anded_file_addr_alu_pipe1_rn),

|(orred_0_anded_file_addr_alu_pipe1_rn)};


decoder_64 decoder_for_free_bit_reset_alu_pipe1_rn (
    .addr_in(reg_addr_issued_to_alu_pipe1_rn_out), 
    .decoded_addr_out(reset_for_free_bits_alu_pipe1_rn)
    );


assign next_hamming_reg_alu_pipe1_rn_0[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:1] = 

reg_addr_issued_to_alu_pipe1_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:1];


assign next_hamming_reg_alu_pipe1_rn_0[0] = ~reg_addr_issued_to_alu_pipe1_rn_out[0];


assign next_hamming_reg_alu_pipe1_rn_1[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:2] = 

reg_addr_issued_to_alu_pipe1_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:2];


assign next_hamming_reg_alu_pipe1_rn_1[1] = ~reg_addr_issued_to_alu_pipe1_rn_out[1];


assign next_hamming_reg_alu_pipe1_rn_1[0] = reg_addr_issued_to_alu_pipe1_rn_out[0];


assign next_hamming_reg_alu_pipe1_rn_2[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:3] = 

reg_addr_issued_to_alu_pipe1_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:3];


assign next_hamming_reg_alu_pipe1_rn_2[2] = ~reg_addr_issued_to_alu_pipe1_rn_out[2];


assign next_hamming_reg_alu_pipe1_rn_2[1:0] = reg_addr_issued_to_alu_pipe1_rn_out[1:0];


assign next_hamming_reg_alu_pipe1_rn_3[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:4] = 

reg_addr_issued_to_alu_pipe1_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:4];


assign next_hamming_reg_alu_pipe1_rn_3[3] = ~reg_addr_issued_to_alu_pipe1_rn_out[3];


assign next_hamming_reg_alu_pipe1_rn_3[2:0] = reg_addr_issued_to_alu_pipe1_rn_out[2:0];


assign next_hamming_reg_alu_pipe1_rn_4[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1] = 

reg_addr_issued_to_alu_pipe1_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1];


assign next_hamming_reg_alu_pipe1_rn_4[4] = ~reg_addr_issued_to_alu_pipe1_rn_out[4];


assign next_hamming_reg_alu_pipe1_rn_4[3:0] = reg_addr_issued_to_alu_pipe1_rn_out[3:0];


assign next_hamming_reg_alu_pipe1_rn_5[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1] = 

~reg_addr_issued_to_alu_pipe1_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1];


assign next_hamming_reg_alu_pipe1_rn_5[4:0] = reg_addr_issued_to_alu_pipe1_rn_out[4:0];


decoder_64 decoder_for_next_hamming_alu_pipe1_rn_0 (
    .addr_in(next_hamming_reg_alu_pipe1_rn_0), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rn_0)
    );


decoder_64 decoder_for_next_hamming_alu_pipe1_rn_1 (
    .addr_in(next_hamming_reg_alu_pipe1_rn_1), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rn_1)
    );


decoder_64 decoder_for_next_hamming_alu_pipe1_rn_2 (
    .addr_in(next_hamming_reg_alu_pipe1_rn_2), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rn_2)
    );


decoder_64 decoder_for_next_hamming_alu_pipe1_rn_3 (
    .addr_in(next_hamming_reg_alu_pipe1_rn_3), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rn_3)
    );


decoder_64 decoder_for_next_hamming_alu_pipe1_rn_4 (
    .addr_in(next_hamming_reg_alu_pipe1_rn_4), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rn_4)
    );


decoder_64 decoder_for_next_hamming_alu_pipe1_rn_5 (
    .addr_in(next_hamming_reg_alu_pipe1_rn_5), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe1_rn_5)
    );



assign next_hamming_reg_free_alu_pipe1_rn_0 = new_issue_position_hamming_alu_pipe1_rn_0 & free_bits;


assign next_hamming_reg_free_alu_pipe1_rn_1 = new_issue_position_hamming_alu_pipe1_rn_1 & free_bits;


assign next_hamming_reg_free_alu_pipe1_rn_2 = new_issue_position_hamming_alu_pipe1_rn_2 & free_bits;


assign next_hamming_reg_free_alu_pipe1_rn_3 = new_issue_position_hamming_alu_pipe1_rn_3 & free_bits;


assign next_hamming_reg_free_alu_pipe1_rn_4 = new_issue_position_hamming_alu_pipe1_rn_4 & free_bits;


assign next_hamming_reg_free_alu_pipe1_rn_5 = new_issue_position_hamming_alu_pipe1_rn_5 & free_bits;


decoding_all_below_vector_64 all_below_issue_pointer_alu_pipe1_rn (
    .input_vector_in(issue_pointer_for_alu_pipe1_rn), 
    .all_below_input_vector_out(all_below_issue_pointer_for_alu_pipe1_rn)
    );


assign all_reg_below_issue_pointer_free_alu_pipe1_rn = all_below_issue_pointer_for_alu_pipe1_rn & 

free_bits;


priority_encoder_64 priority_encoder_for_next_reg_addr_to_point_issue_pointer_alu_pipe1_rn (
    .decoded_input_in(all_reg_below_issue_pointer_free_alu_pipe1_rn), 
    .priority_encoded_out(next_reg_addr_to_point_alu_pipe1_rn)
    );


decoder_64 decoder_for_next_non_hamming_alu_pipe1_rn (
    .addr_in(next_reg_addr_to_point_alu_pipe1_rn), 
    .decoded_addr_out(new_issue_position_hamming_not_free_alu_pipe1_rn)
    );


assign data_to_issue_pointer_alu_pipe1_rn = (|(next_hamming_reg_free_alu_pipe1_rn_0) ? 

new_issue_position_hamming_alu_pipe1_rn_0 : (|(next_hamming_reg_free_alu_pipe1_rn_1) ? 

new_issue_position_hamming_alu_pipe1_rn_1 : (|(next_hamming_reg_free_alu_pipe1_rn_2) ? 

new_issue_position_hamming_alu_pipe1_rn_2 : (|(next_hamming_reg_free_alu_pipe1_rn_3) ? 

new_issue_position_hamming_alu_pipe1_rn_3 : (|(next_hamming_reg_free_alu_pipe1_rn_4) ? 

new_issue_position_hamming_alu_pipe1_rn_4 : (|(next_hamming_reg_free_alu_pipe1_rn_5) ? 

new_issue_position_hamming_alu_pipe1_rn_5 : new_issue_position_hamming_not_free_alu_pipe1_rn))))));

/******RN_ENDS******/

/******ALU_PIPE1_ENDS******/


/******ALU_PIPE2_BEGINS******/

/******RD_BEGINS******/

genvar m;
generate
for(m=`ALU_PIPES_REG_FILE_SIZE-1;m>=0;m=m-1)
begin 
	assign anded_file_addr_alu_pipe2_rd[m] = register_file_addr[m] & 
	
	{`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE{issue_pointer_for_alu_pipe2_rd[m]}};
end
endgenerate


genvar n;
generate
for(n=`ALU_PIPES_REG_FILE_SIZE-1;n>=0;n=n-1)
begin 
	assign orred_0_anded_file_addr_alu_pipe2_rd[n] = anded_file_addr_alu_pipe2_rd[n][0];
	assign orred_1_anded_file_addr_alu_pipe2_rd[n] = anded_file_addr_alu_pipe2_rd[n][1];
	assign orred_2_anded_file_addr_alu_pipe2_rd[n] = anded_file_addr_alu_pipe2_rd[n][2];
	assign orred_3_anded_file_addr_alu_pipe2_rd[n] = anded_file_addr_alu_pipe2_rd[n][3];
	assign orred_4_anded_file_addr_alu_pipe2_rd[n] = anded_file_addr_alu_pipe2_rd[n][4];
	assign orred_5_anded_file_addr_alu_pipe2_rd[n] = anded_file_addr_alu_pipe2_rd[n][5];
end
endgenerate


assign reg_addr_issued_to_alu_pipe2_rd_out = {|(orred_5_anded_file_addr_alu_pipe2_rd),

|(orred_4_anded_file_addr_alu_pipe2_rd),|(orred_3_anded_file_addr_alu_pipe2_rd),

|(orred_2_anded_file_addr_alu_pipe2_rd),|(orred_1_anded_file_addr_alu_pipe2_rd),

|(orred_0_anded_file_addr_alu_pipe2_rd)};


decoder_64 decoder_for_free_bit_reset_alu_pipe2_rd (
    .addr_in(reg_addr_issued_to_alu_pipe2_rd_out), 
    .decoded_addr_out(reset_for_free_bits_alu_pipe2_rd)
    );


assign next_hamming_reg_alu_pipe2_rd_0[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:1] = 

reg_addr_issued_to_alu_pipe2_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:1];


assign next_hamming_reg_alu_pipe2_rd_0[0] = ~reg_addr_issued_to_alu_pipe2_rd_out[0];


assign next_hamming_reg_alu_pipe2_rd_1[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:2] = 

reg_addr_issued_to_alu_pipe2_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:2];


assign next_hamming_reg_alu_pipe2_rd_1[1] = ~reg_addr_issued_to_alu_pipe2_rd_out[1];


assign next_hamming_reg_alu_pipe2_rd_1[0] = reg_addr_issued_to_alu_pipe2_rd_out[0];


assign next_hamming_reg_alu_pipe2_rd_2[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:3] = 

reg_addr_issued_to_alu_pipe2_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:3];


assign next_hamming_reg_alu_pipe2_rd_2[2] = ~reg_addr_issued_to_alu_pipe2_rd_out[2];


assign next_hamming_reg_alu_pipe2_rd_2[1:0] = reg_addr_issued_to_alu_pipe2_rd_out[1:0];


assign next_hamming_reg_alu_pipe2_rd_3[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:4] = 

reg_addr_issued_to_alu_pipe2_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:4];


assign next_hamming_reg_alu_pipe2_rd_3[3] = ~reg_addr_issued_to_alu_pipe2_rd_out[3];


assign next_hamming_reg_alu_pipe2_rd_3[2:0] = reg_addr_issued_to_alu_pipe2_rd_out[2:0];


assign next_hamming_reg_alu_pipe2_rd_4[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1] = 

reg_addr_issued_to_alu_pipe2_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1];


assign next_hamming_reg_alu_pipe2_rd_4[4] = ~reg_addr_issued_to_alu_pipe2_rd_out[4];


assign next_hamming_reg_alu_pipe2_rd_4[3:0] = reg_addr_issued_to_alu_pipe2_rd_out[3:0];


assign next_hamming_reg_alu_pipe2_rd_5[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1] = 

~reg_addr_issued_to_alu_pipe2_rd_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1];


assign next_hamming_reg_alu_pipe2_rd_5[4:0] = reg_addr_issued_to_alu_pipe2_rd_out[4:0];


decoder_64 decoder_for_next_hamming_alu_pipe2_rd_0 (
    .addr_in(next_hamming_reg_alu_pipe2_rd_0), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rd_0)
    );


decoder_64 decoder_for_next_hamming_alu_pipe2_rd_1 (
    .addr_in(next_hamming_reg_alu_pipe2_rd_1), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rd_1)
    );


decoder_64 decoder_for_next_hamming_alu_pipe2_rd_2 (
    .addr_in(next_hamming_reg_alu_pipe2_rd_2), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rd_2)
    );


decoder_64 decoder_for_next_hamming_alu_pipe2_rd_3 (
    .addr_in(next_hamming_reg_alu_pipe2_rd_3), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rd_3)
    );


decoder_64 decoder_for_next_hamming_alu_pipe2_rd_4 (
    .addr_in(next_hamming_reg_alu_pipe2_rd_4), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rd_4)
    );


decoder_64 decoder_for_next_hamming_alu_pipe2_rd_5 (
    .addr_in(next_hamming_reg_alu_pipe2_rd_5), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rd_5)
    );


assign next_hamming_reg_free_alu_pipe2_rd_0 = new_issue_position_hamming_alu_pipe2_rd_0 & free_bits;


assign next_hamming_reg_free_alu_pipe2_rd_1 = new_issue_position_hamming_alu_pipe2_rd_1 & free_bits;


assign next_hamming_reg_free_alu_pipe2_rd_2 = new_issue_position_hamming_alu_pipe2_rd_2 & free_bits;


assign next_hamming_reg_free_alu_pipe2_rd_3 = new_issue_position_hamming_alu_pipe2_rd_3 & free_bits;


assign next_hamming_reg_free_alu_pipe2_rd_4 = new_issue_position_hamming_alu_pipe2_rd_4 & free_bits;


assign next_hamming_reg_free_alu_pipe2_rd_5 = new_issue_position_hamming_alu_pipe2_rd_5 & free_bits;


decoding_all_below_vector_64 all_below_issue_pointer_alu_pipe2_rd (
    .input_vector_in(issue_pointer_for_alu_pipe2_rd), 
    .all_below_input_vector_out(all_below_issue_pointer_for_alu_pipe2_rd)
    );


assign all_reg_below_issue_pointer_free_alu_pipe2_rd = all_below_issue_pointer_for_alu_pipe2_rd & 

free_bits;


priority_encoder_64 priority_encoder_for_next_reg_addr_to_point_issue_pointer_alu_pipe2_rd (
    .decoded_input_in(all_reg_below_issue_pointer_free_alu_pipe2_rd), 
    .priority_encoded_out(next_reg_addr_to_point_alu_pipe2_rd)
    );


decoder_64 decoder_for_next_non_hamming_alu_pipe2_rd (
    .addr_in(next_reg_addr_to_point_alu_pipe2_rd), 
    .decoded_addr_out(new_issue_position_hamming_not_free_alu_pipe2_rd)
    );


assign data_to_issue_pointer_alu_pipe2_rd = (|(next_hamming_reg_free_alu_pipe2_rd_0) ? 

new_issue_position_hamming_alu_pipe2_rd_0 : (|(next_hamming_reg_free_alu_pipe2_rd_1) ? 

new_issue_position_hamming_alu_pipe2_rd_1 : (|(next_hamming_reg_free_alu_pipe2_rd_2) ? 

new_issue_position_hamming_alu_pipe2_rd_2 : (|(next_hamming_reg_free_alu_pipe2_rd_3) ? 

new_issue_position_hamming_alu_pipe2_rd_3 : (|(next_hamming_reg_free_alu_pipe2_rd_4) ? 

new_issue_position_hamming_alu_pipe2_rd_4 : (|(next_hamming_reg_free_alu_pipe2_rd_5) ? 

new_issue_position_hamming_alu_pipe2_rd_5 : new_issue_position_hamming_not_free_alu_pipe2_rd))))));

/******RD_ENDS******/


/******RN_BEGINS******/

genvar o;
generate
for(o=`ALU_PIPES_REG_FILE_SIZE-1;o>=0;o=o-1)
begin 
	assign anded_file_addr_alu_pipe2_rn[o] = register_file_addr[o] & 
	
	{`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE{issue_pointer_for_alu_pipe2_rn[o]}};
end
endgenerate


genvar p;
generate
for(p=`ALU_PIPES_REG_FILE_SIZE-1;p>=0;p=p-1)
begin 
	assign orred_0_anded_file_addr_alu_pipe2_rn[p] = anded_file_addr_alu_pipe2_rn[p][0];
	assign orred_1_anded_file_addr_alu_pipe2_rn[p] = anded_file_addr_alu_pipe2_rn[p][1];
	assign orred_2_anded_file_addr_alu_pipe2_rn[p] = anded_file_addr_alu_pipe2_rn[p][2];
	assign orred_3_anded_file_addr_alu_pipe2_rn[p] = anded_file_addr_alu_pipe2_rn[p][3];
	assign orred_4_anded_file_addr_alu_pipe2_rn[p] = anded_file_addr_alu_pipe2_rn[p][4];
	assign orred_5_anded_file_addr_alu_pipe2_rn[p] = anded_file_addr_alu_pipe2_rn[p][5];
end
endgenerate


assign reg_addr_issued_to_alu_pipe2_rn_out = {|(orred_5_anded_file_addr_alu_pipe2_rn),

|(orred_4_anded_file_addr_alu_pipe2_rn),|(orred_3_anded_file_addr_alu_pipe2_rn),

|(orred_2_anded_file_addr_alu_pipe2_rn),|(orred_1_anded_file_addr_alu_pipe2_rn),

|(orred_0_anded_file_addr_alu_pipe2_rn)};


decoder_64 decoder_for_free_bit_reset_alu_pipe2_rn (
    .addr_in(reg_addr_issued_to_alu_pipe2_rn_out), 
    .decoded_addr_out(reset_for_free_bits_alu_pipe2_rn)
    );


assign next_hamming_reg_alu_pipe2_rn_0[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:1] = 

reg_addr_issued_to_alu_pipe2_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:1];


assign next_hamming_reg_alu_pipe2_rn_0[0] = ~reg_addr_issued_to_alu_pipe2_rn_out[0];


assign next_hamming_reg_alu_pipe2_rn_1[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:2] = 

reg_addr_issued_to_alu_pipe2_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:2];


assign next_hamming_reg_alu_pipe2_rn_1[1] = ~reg_addr_issued_to_alu_pipe2_rn_out[1];


assign next_hamming_reg_alu_pipe2_rn_1[0] = reg_addr_issued_to_alu_pipe2_rn_out[0];


assign next_hamming_reg_alu_pipe2_rn_2[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:3] = 

reg_addr_issued_to_alu_pipe2_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:3];


assign next_hamming_reg_alu_pipe2_rn_2[2] = ~reg_addr_issued_to_alu_pipe2_rn_out[2];


assign next_hamming_reg_alu_pipe2_rn_2[1:0] = reg_addr_issued_to_alu_pipe2_rn_out[1:0];


assign next_hamming_reg_alu_pipe2_rn_3[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:4] = 

reg_addr_issued_to_alu_pipe2_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1:4];


assign next_hamming_reg_alu_pipe2_rn_3[3] = ~reg_addr_issued_to_alu_pipe2_rn_out[3];


assign next_hamming_reg_alu_pipe2_rn_3[2:0] = reg_addr_issued_to_alu_pipe2_rn_out[2:0];


assign next_hamming_reg_alu_pipe2_rn_4[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1] = 

reg_addr_issued_to_alu_pipe2_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1];


assign next_hamming_reg_alu_pipe2_rn_4[4] = ~reg_addr_issued_to_alu_pipe2_rn_out[4];


assign next_hamming_reg_alu_pipe2_rn_4[3:0] = reg_addr_issued_to_alu_pipe2_rn_out[3:0];


assign next_hamming_reg_alu_pipe2_rn_5[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1] = 

~reg_addr_issued_to_alu_pipe2_rn_out[`DISTRIBUTED_REG_FILE_ALU_PIPES_ADDR_SIZE-1];


assign next_hamming_reg_alu_pipe2_rn_5[4:0] = reg_addr_issued_to_alu_pipe2_rn_out[4:0];


decoder_64 decoder_for_next_hamming_alu_pipe2_rn_0 (
    .addr_in(next_hamming_reg_alu_pipe2_rn_0), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rn_0)
    );


decoder_64 decoder_for_next_hamming_alu_pipe2_rn_1 (
    .addr_in(next_hamming_reg_alu_pipe2_rn_1), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rn_1)
    );


decoder_64 decoder_for_next_hamming_alu_pipe2_rn_2 (
    .addr_in(next_hamming_reg_alu_pipe2_rn_2), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rn_2)
    );


decoder_64 decoder_for_next_hamming_alu_pipe2_rn_3 (
    .addr_in(next_hamming_reg_alu_pipe2_rn_3), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rn_3)
    );


decoder_64 decoder_for_next_hamming_alu_pipe2_rn_4 (
    .addr_in(next_hamming_reg_alu_pipe2_rn_4), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rn_4)
    );


decoder_64 decoder_for_next_hamming_alu_pipe2_rn_5 (
    .addr_in(next_hamming_reg_alu_pipe2_rn_5), 
    .decoded_addr_out(new_issue_position_hamming_alu_pipe2_rn_5)
    );


assign next_hamming_reg_free_alu_pipe2_rn_0 = new_issue_position_hamming_alu_pipe2_rn_0 & free_bits;


assign next_hamming_reg_free_alu_pipe2_rn_1 = new_issue_position_hamming_alu_pipe2_rn_1 & free_bits;


assign next_hamming_reg_free_alu_pipe2_rn_2 = new_issue_position_hamming_alu_pipe2_rn_2 & free_bits;


assign next_hamming_reg_free_alu_pipe2_rn_3 = new_issue_position_hamming_alu_pipe2_rn_3 & free_bits;


assign next_hamming_reg_free_alu_pipe2_rn_4 = new_issue_position_hamming_alu_pipe2_rn_4 & free_bits;


assign next_hamming_reg_free_alu_pipe2_rn_5 = new_issue_position_hamming_alu_pipe2_rn_5 & free_bits;


decoding_all_below_vector_64 all_below_issue_pointer_alu_pipe2_rn (
    .input_vector_in(issue_pointer_for_alu_pipe2_rn), 
    .all_below_input_vector_out(all_below_issue_pointer_for_alu_pipe2_rn)
    );


assign all_reg_below_issue_pointer_free_alu_pipe2_rn = all_below_issue_pointer_for_alu_pipe2_rn & 

free_bits;


priority_encoder_64 priority_encoder_for_next_reg_addr_to_point_issue_pointer_alu_pipe2_rn (
    .decoded_input_in(all_reg_below_issue_pointer_free_alu_pipe2_rn), 
    .priority_encoded_out(next_reg_addr_to_point_alu_pipe2_rn)
    );


decoder_64 decoder_for_next_non_hamming_alu_pipe2_rn (
    .addr_in(next_reg_addr_to_point_alu_pipe2_rn), 
    .decoded_addr_out(new_issue_position_hamming_not_free_alu_pipe2_rn)
    );


assign data_to_issue_pointer_alu_pipe2_rn = (|(next_hamming_reg_free_alu_pipe2_rn_0) ? 

new_issue_position_hamming_alu_pipe2_rn_0 : (|(next_hamming_reg_free_alu_pipe2_rn_1) ? 

new_issue_position_hamming_alu_pipe2_rn_1 : (|(next_hamming_reg_free_alu_pipe2_rn_2) ? 

new_issue_position_hamming_alu_pipe2_rn_2 : (|(next_hamming_reg_free_alu_pipe2_rn_3) ? 

new_issue_position_hamming_alu_pipe2_rn_3 : (|(next_hamming_reg_free_alu_pipe2_rn_4) ? 

new_issue_position_hamming_alu_pipe2_rn_4 : (|(next_hamming_reg_free_alu_pipe2_rn_5) ? 

new_issue_position_hamming_alu_pipe2_rn_5 : new_issue_position_hamming_not_free_alu_pipe2_rn))))));

/******RN_ENDS******/

/******ALU_PIPE2_ENDS******/


/******FREE BITS CONTROL STARTS******/

assign combined_reset_for_free_bits = (reset_for_free_bits_alu_pipe1_rd & 

{`ALU_PIPES_REG_FILE_SIZE{reg_issue_alu_pipe1_rd_in}}) | (reset_for_free_bits_alu_pipe1_rn & 

{`ALU_PIPES_REG_FILE_SIZE{reg_issue_alu_pipe1_rn_in}}) | (reset_for_free_bits_alu_pipe2_rd & 

{`ALU_PIPES_REG_FILE_SIZE{reg_issue_alu_pipe2_rd_in}}) | (reset_for_free_bits_alu_pipe2_rn & 

{`ALU_PIPES_REG_FILE_SIZE{reg_issue_alu_pipe2_rn_in}});


decoder_64 decoder_for_instr1_retire_rd (
    .addr_in(retired_instr1_rd_addr_in), 
    .decoded_addr_out(decoded_rd_addr_instr1)
    );


decoder_64 decoder_for_instr1_retire_rn (
    .addr_in(retired_instr1_rn_addr_in), 
    .decoded_addr_out(decoded_rn_addr_instr1)
    );


decoder_64 decoder_for_instr2_retire_rd (
    .addr_in(retired_instr2_rd_addr_in), 
    .decoded_addr_out(decoded_rd_addr_instr2)
    );


decoder_64 decoder_for_instr2_retire_rn (
    .addr_in(retired_instr2_rn_addr_in), 
    .decoded_addr_out(decoded_rn_addr_instr2)
    );


assign retire_decoded_rd_addr_instr1 = decoded_rd_addr_instr1 & 

{`ALU_PIPES_REG_FILE_SIZE{retire_en_instr1_rd_in}};


assign retire_decoded_rn_addr_instr1 = decoded_rn_addr_instr1 & 

{`ALU_PIPES_REG_FILE_SIZE{retire_en_instr1_rn_in}};


assign retire_decoded_rd_addr_instr2 = decoded_rd_addr_instr2 & 

{`ALU_PIPES_REG_FILE_SIZE{retire_en_instr2_rd_in}};


assign retire_decoded_rn_addr_instr2 = decoded_rn_addr_instr2 & 

{`ALU_PIPES_REG_FILE_SIZE{retire_en_instr2_rn_in}};


assign data_for_free_bit = retire_decoded_rd_addr_instr1 | retire_decoded_rn_addr_instr1 | 

retire_decoded_rd_addr_instr2 | retire_decoded_rn_addr_instr2;


genvar q;
generate
for(q=`ALU_PIPES_REG_FILE_SIZE-1;q>=0;q=q-1)
begin	: grp_reg_free_bits
	
	
	assign en_free_bits[q] = data_for_free_bit[q] | reg_issue_alu_pipe1_rd_in | 
	
	reg_issue_alu_pipe1_rn_in | reg_issue_alu_pipe2_rd_in | reg_issue_alu_pipe2_rn_in;
	
	
	register_with_preset_reset #1 reg_free_bits (
		 .data_in(data_for_free_bit[q]), 
		 .clk_in(clk_in), 
		 .preset_in(reset_in),
		 .reset_in(combined_reset_for_free_bits[q]), 
		 .en_in(data_for_free_bit[q]), 
		 .data_out(free_bits[q])
		 );
end
endgenerate

/******FREE BITS CONTROL ENDS******/


/******ISSUE POINTER CONTROL STARTS******/

register_with_preset #1 reg_issue_pointer_alu_pipe1_rd_first_bit (
		 .data_in(data_to_issue_pointer_alu_pipe1_rd[`ALU_PIPES_REG_FILE_SIZE-1]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(reg_issue_alu_pipe1_rd_in), 
		 .data_out(issue_pointer_for_alu_pipe1_rd[`ALU_PIPES_REG_FILE_SIZE-1])
		 );


register_with_preset #1 reg_issue_pointer_alu_pipe1_rn_first_bit (
		 .data_in(data_to_issue_pointer_alu_pipe1_rn[`ALU_PIPES_REG_FILE_SIZE-1]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(reg_issue_alu_pipe1_rn_in), 
		 .data_out(issue_pointer_for_alu_pipe1_rn[`ALU_PIPES_REG_FILE_SIZE-1])
		 );


register_with_preset #1 reg_issue_pointer_alu_pipe2_rd_first_bit (
		 .data_in(data_to_issue_pointer_alu_pipe2_rd[`ALU_PIPES_REG_FILE_SIZE-1]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(reg_issue_alu_pipe2_rd_in), 
		 .data_out(issue_pointer_for_alu_pipe2_rd[`ALU_PIPES_REG_FILE_SIZE-1])
		 );


register_with_reset #1 reg_issue_pointer_alu_pipe2_rn_first_bit (
		 .data_in(data_to_issue_pointer_alu_pipe2_rn[`ALU_PIPES_REG_FILE_SIZE-1]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(reg_issue_alu_pipe2_rn_in), 
		 .data_out(issue_pointer_for_alu_pipe2_rn[`ALU_PIPES_REG_FILE_SIZE-1])
		 );


genvar r;
generate
for(r=`ALU_PIPES_REG_FILE_SIZE-2;r>=0;r=r-1)
begin	: grp_reg_issue_pointer
	register_with_reset #1 reg_issue_pointer_alu_pipe1_rd (
		 .data_in(data_to_issue_pointer_alu_pipe1_rd[r]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(reg_issue_alu_pipe1_rd_in), 
		 .data_out(issue_pointer_for_alu_pipe1_rd[r])
		 );
	
	
	register_with_reset #1 reg_issue_pointer_alu_pipe1_rn (
		 .data_in(data_to_issue_pointer_alu_pipe1_rn[r]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(reg_issue_alu_pipe1_rn_in), 
		 .data_out(issue_pointer_for_alu_pipe1_rn[r])
		 );
	
	
	register_with_reset #1 reg_issue_pointer_alu_pipe2_rd (
		 .data_in(data_to_issue_pointer_alu_pipe2_rd[r]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(reg_issue_alu_pipe2_rd_in), 
		 .data_out(issue_pointer_for_alu_pipe2_rd[r])
		 );
	
	
	register_with_reset #1 reg_issue_pointer_alu_pipe2_rn (
		 .data_in(data_to_issue_pointer_alu_pipe2_rn[r]), 
		 .clk_in(clk_in), 
		 .reset_in(reset_in), 
		 .en_in(reg_issue_alu_pipe2_rn_in), 
		 .data_out(issue_pointer_for_alu_pipe2_rn[r])
		 );
end
endgenerate

/******ISSUE POINTER CONTROL ENDS******/


endmodule
