`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module retire_controller(
									input valid_retire_instr1_in,
									input ldm_instr1_in,
									input valid_ldm_finished_instr1_in,
									input valid_retire_instr2_in,
									input ldm_instr2_in,
									input valid_ldm_finished_instr2_in,
									output reg retire_one_instr_out,
									output reg retire_both_instr_out
    );


always@(*)
begin
	case({valid_retire_instr1_in,ldm_instr1_in,valid_ldm_finished_instr1_in,valid_retire_instr2_in,
	ldm_instr2_in,valid_ldm_finished_instr2_in})
		6'b100000 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b100001 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b100010 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b100011 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b100100 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b1;
		end
		6'b100101 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b1;
		end
		6'b100110 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b100111 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b1;
		end
		6'b101000 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b101001 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b101010 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b101011 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b101100 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b1;
		end
		6'b101101 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b1;
		end
		6'b101110 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b101111 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b1;
		end
		6'b111000 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b111001 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b111010 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b111011 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b0;
		end
		6'b111100 :
		begin
			retire_one_instr_out = 1'b1;
			retire_both_instr_out = 1'b1;
		end
		default : 
		begin
			retire_one_instr_out = 1'b0;
			retire_both_instr_out = 1'b0;
		end
	endcase
end
		
			
			


endmodule
