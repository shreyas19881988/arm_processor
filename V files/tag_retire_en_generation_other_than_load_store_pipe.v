`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"
module tag_retire_en_generation_other_than_load_store(
									input [`RB_SIZE-1:0] valid_instr_retire_updating_rd_pipe_in,
									input instr_invalid_pipe_but_updating_rd_in,
									input instr_invalid_pipe_updating_rd_but_retiring_frm_other_pipe_in,
									input [`RB_SIZE-1:0] valid_instr_retire_updating_rn_pipe_in,
									input instr_invalid_pipe_but_updating_rn_in,
									input instr_invalid_pipe_updating_rn_but_retiring_frm_other_pipe_in,
									input [`RB_SIZE-1:0] valid_instr_retire_updating_cpsr_pipe_in,
									input instr_invalid_pipe_but_updating_cpsr_in,
									input instr_invalid_pipe_updating_cpsr_but_retiring_frm_other_pipe_in,
									input or_valid_instr_above_tag_updating_same_rd_pipe_in,
									input or_valid_instr_above_tag_updating_same_rn_pipe_in,
									input or_valid_instr_above_tag_updating_cpsr_pipe_in,
									output [`RD_RN_CPSR_EN-1:0] tag_retire_en_pipe_out
									
    );


assign tag_retire_en_pipe_out[`RD_EN] = (|(valid_instr_retire_updating_rd_pipe_in)) | 

(instr_invalid_pipe_but_updating_rd_in & (~or_valid_instr_above_tag_updating_same_rd_pipe_in | 

instr_invalid_pipe_updating_rd_but_retiring_frm_other_pipe_in));


assign tag_retire_en_pipe_out[`RN_EN] = |(valid_instr_retire_updating_rn_pipe_in) | 

(instr_invalid_pipe_but_updating_rn_in & (~or_valid_instr_above_tag_updating_same_rn_pipe_in | 

instr_invalid_pipe_updating_rn_but_retiring_frm_other_pipe_in));


assign tag_retire_en_pipe_out[`CPSR_EN] = |(valid_instr_retire_updating_cpsr_pipe_in) | 

(instr_invalid_pipe_but_updating_cpsr_in & (~or_valid_instr_above_tag_updating_cpsr_pipe_in | 

instr_invalid_pipe_updating_cpsr_but_retiring_frm_other_pipe_in));


endmodule
