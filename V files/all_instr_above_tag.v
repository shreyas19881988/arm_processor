`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module all_instr_above_tag(
									input [`RB_SIZE-1:0] tag_position_in_RB_in,
									input [`RB_SIZE-1:0] head_pointer1_in,
									input [`RB_SIZE-1:0] all_below_head_in,
									input [`RB_SIZE-1:0] all_above_tail_in,
									input [`RB_SIZE-1:0] valid_instr_in,
									input [`RB_SIZE-1:0] comparing_rd_pipe_in,
									input [`RB_SIZE-1:0] comparing_rn_pipe_in,
									input [`RB_SIZE-1:0] comparing_cpsr_pipe_in,
									output [`RB_SIZE-1:0] valid_instr_above_tag_same_rd_pipe_out,
									output [`RB_SIZE-1:0] valid_instr_above_tag_same_rn_pipe_out,
									output [`RB_SIZE-1:0] valid_instr_above_tag_same_cpsr_pipe_out
									
    );

wire [`RB_SIZE-1:0] all_above_tag_position;
wire [`RB_SIZE-1:0] head_greater_than_tag_position;
wire head_greater_than_tag_position_single;
wire [`RB_SIZE-1:0] valid_instr_above_tag_for_head_greater_than_tag;
wire [`RB_SIZE-1:0] valid_instr_above_tag_for_tag_greater_than_head;
wire [`RB_SIZE-1:0] valid_instr_above_tag;

genvar i;
generate
for(i=`RB_SIZE-1;i>0;i=i-1)
begin	:	grp_all_above_tag_position
	assign all_above_tag_position[i] = |(tag_position_in_RB_in[i-1:0]);
end
endgenerate


assign all_above_tag_position[0] = 1'b0;


assign head_greater_than_tag_position = head_pointer1_in & all_above_tag_position;


assign head_greater_than_tag_position_single = |(head_greater_than_tag_position);


assign valid_instr_above_tag_for_head_greater_than_tag = all_below_head_in & all_above_tag_position

& valid_instr_in;//last term may not be needed


assign valid_instr_above_tag_for_tag_greater_than_head = (all_above_tail_in & all_above_tag_position) 

| all_below_head_in & valid_instr_in;//last term may not be needed


assign valid_instr_above_tag = head_greater_than_tag_position_single ? 

valid_instr_above_tag_for_head_greater_than_tag : valid_instr_above_tag_for_tag_greater_than_head;


assign valid_instr_above_tag_same_rd_pipe_out = valid_instr_above_tag & comparing_rd_pipe_in;


assign valid_instr_above_tag_same_rn_pipe_out = valid_instr_above_tag & comparing_rn_pipe_in;


assign valid_instr_above_tag_same_cpsr_pipe_out = valid_instr_above_tag & comparing_cpsr_pipe_in;


endmodule
