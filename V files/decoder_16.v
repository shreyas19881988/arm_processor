`include "timescale.v"
`include "RB_info.v"
`include "GEN_info.v"

module decoder_16(
						input [`LOG_ADDRESS_SIZE:0] addr_in,
						output reg [`NO_OF_REG-1:0] decoded_addr_out
    );

always@(*)
begin
	case(addr_in)
		`LOG_ADDRESS_SIZE+1'b10000 : 
		begin
			decoded_addr_out = 32768;
		end
		`LOG_ADDRESS_SIZE+1'b10001 : 
		begin
			decoded_addr_out = 16384;
		end
		`LOG_ADDRESS_SIZE+1'b10010 : 
		begin
			decoded_addr_out = 8192;
		end
		`LOG_ADDRESS_SIZE+1'b10011 : 
		begin
			decoded_addr_out = 4096;
		end
		`LOG_ADDRESS_SIZE+1'b10100 : 
		begin
			decoded_addr_out = 2048;
		end
		`LOG_ADDRESS_SIZE+1'b10101 : 
		begin
			decoded_addr_out = 1024;
		end
		`LOG_ADDRESS_SIZE+1'b10110 : 
		begin
			decoded_addr_out = 512;
		end
		`LOG_ADDRESS_SIZE+1'b10111 : 
		begin
			decoded_addr_out = 256;
		end
		`LOG_ADDRESS_SIZE+1'b11000 : 
		begin
			decoded_addr_out = 128;
		end
		`LOG_ADDRESS_SIZE+1'b11001 : 
		begin
			decoded_addr_out = 64;
		end
		`LOG_ADDRESS_SIZE+1'b11010 : 
		begin
			decoded_addr_out = 32;
		end
		`LOG_ADDRESS_SIZE+1'b11011 : 
		begin
			decoded_addr_out = 16;
		end
		`LOG_ADDRESS_SIZE+1'b11100 : 
		begin
			decoded_addr_out = 8;
		end
		`LOG_ADDRESS_SIZE+1'b11101 : 
		begin
			decoded_addr_out = 4;
		end
		`LOG_ADDRESS_SIZE+1'b11110 : 
		begin
			decoded_addr_out = 2;
		end
		`LOG_ADDRESS_SIZE+1'b11111 : 
		begin
			decoded_addr_out = 1;
		end
		default : 
		begin
			decoded_addr_out = 0;
		end
	endcase
end
endmodule
